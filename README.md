We are a small team of biologists, animators and programmers based at the University of Utah and Harvard Medical School seeking to change the way biologists create and visualize molecular models.

The Molecular FlipBook Project has two main goals:

1. We are developing a comprehensive, free and open-source molecular animation software toolkit, Molecular Flipbook, that has been specifically designed with the needs of biology researchers in mind.

2. We are developing a website that will allow members of the research and education communities to freely share, view, and edit molecular models with one another. 

====================================================================================

Molecular Flipbook Toolkit Open Source Code: https://github.com/MolecularFlipbook/FlipbookApp

Molecular Flipbook Website Open Source Code: https://bitbucket.org/mfbsite/mfb-website

For more information, please visit our website: https://www.molecularflipbook.org/