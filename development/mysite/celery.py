# stdlib imports
from __future__ import absolute_import
import os

# core django imports

# 3rd party imports
from celery import Celery

# project imports
from settings.base import BROKER_URL
from settings.base import get_env_variable

if get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.dev':
	from settings import dev
elif get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.staging':
	from settings import staging
elif get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.production':
	from settings import production

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', get_env_variable('SETTINGS_ENVIRONMENT'))

# instance of the Celery library
# mysite - name of current module (config directory); broker - URL of message broker
# backend - track a task's state, store them in broker; include - a list of modules to import
# when worker starts and can find the tasks.  
app = Celery('mysite', broker=BROKER_URL)#, backend=CELERY_RESULT_BACKEND) #, include=['uploads.tasks'])

if get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.dev':
	# Add Django settings module as a configuration source for Celery.
	app.config_from_object(dev)

	# define all tasks in a separate tasks.py module; Celery will
	# automatically discover tasks in your reusable django apps, e.g. uploads/tasks.py, core/tasks.py
	app.autodiscover_tasks(lambda: dev.INSTALLED_APPS)

elif get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.staging':
	app.config_from_object(staging)
	app.autodiscover_tasks(lambda: staging.INSTALLED_APPS)

elif get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.production':
	app.config_from_object(production)
	app.autodiscover_tasks(lambda: production.INSTALLED_APPS) # for production: settings.base.INSTALLED_APPS

# define a single task
@app.task(bind=True)
def debug_task(self):
	print("Request:%s"%self.request)
	#logging.info("Request:%s"%self.request)
