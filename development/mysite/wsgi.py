"""
WSGI config for mysite project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
# stdlib imports
import os
#import sys
import logging

logger = logging.getLogger(__name__)

# core django imports
#from django.core.wsgi import get_wsgi_application
#import django.core.handlers.wsgi

os.environ.setdefault("DJANGO_SETTINGS_MODULE", os.environ.get('SETTINGS_ENVIRONMENT'))

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
#application = get_wsgi_application()

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)

# newrelic
import newrelic.agent
newrelic.agent.initialize('newrelic.ini')

# add for heroku deployment
from django.core.wsgi import get_wsgi_application
from dj_static import Cling

application = Cling(get_wsgi_application())
