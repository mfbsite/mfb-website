#****************************************************
# urls.py: like a table of contents for the project;
# mapping of URLs to locations of python callback
# functions in views.py
#****************************************************

# stdlib 
import pdb

# django core
from django.conf.urls import patterns
from django.conf.urls import include
from django.conf.urls import url
from django.conf.urls.static import static
#from django.conf import settings

#*********** DEVELOPMENT --  models to use -- DEVELOPMENT ***********************
from settings.base import STATIC_ROOT
from settings.base import MEDIA_URL
from settings.base import MEDIA_ROOT
from settings.base import DEBUG
from settings.base import get_env_variable
# *********************************************************************************

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

# 3rd party apps
from haystack.views import SearchView, search_view_factory
from haystack.query import SearchQuerySet

# forward urls starting with '/pastebook' to the pastebook app. 
#pdb.set_trace()
urlpatterns = patterns('',
					# admin documentation:
					#url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

					# django admin interface: 127.0.0.1:8000/admin; otherwise, view home page: 127.0.0.1:8000
					url(r'^admin/', include(admin.site.urls)), 		# enable the admin
					
					# main navigation page
					url(r'^$', 'core.viewsmainnav.home', name='home'),
					url(r'^about/$', 'core.viewsmainnav.about', name='about'),
					url(r'^downloads/$', 'core.viewsmainnav.downloads', name='downloads'),
					url(r'^library/$', 'core.viewsmainnav.library', name='library'),
					url(r'^tutorial/$', 'core.viewsmainnav.tutorial', name='tutorial'),
					url(r'^news/$', 'core.viewsmainnav.news', name='news'),
					url(r'^help/$', 'core.viewsmainnav.helpfaq', name='helpfaq'),

					# library page
					# explore/browse: most views, most faved, most recent, and staff picks
					url(r'^library/browse/viewed/$', 'core.viewsexplore.browseViewed', name='browse_viewed'),
					url(r'^library/browse/recent/$', 'core.viewsexplore.browseRecent', name='browse_recent'),
					url(r'^library/browse/staff/$','core.viewsexplore.browseStaffPicks',name='browse_staffpicks'),
					url(r'^library/browse/user/uid_(?P<pk>\d+)/$','core.viewsexplore.browseUserSubmissions',name='browse_user'),
					url(r'^library/browse/tag/(?P<pk>\d+)/$', 'core.viewsexplore.browseSubmissionsOfTag', name='browse_submissions_of_tag'),

					# help page
					url(r'^help/mfb_app/$', 'core.viewshelp.mfb_app', name='mfb_app'),
					url(r'^help/mfb_issues/$', 'core.viewshelp.mfb_issues', name='mfb_issues'),
					url(r'^help/faqs/$', 'core.viewshelp.faqs', name='faqs'),
					url(r'^help/fileuploads/$', 'core.viewshelp.fileuploads', name='fileuploads'),
					url(r'^help/tags/$', 'core.viewshelp.tags', name='tags'),
					url(r'^help/guidelines/$', 'core.viewshelp.guidelines', name='guidelines'),
					#url(r'^checkversion/$', 'core.viewsmainnav.checkversion', name='checkversion'),					

					# tutorials page
					url(r'^tutorial/(?P<tutPk>\d+)/$','uploads.viewstutorial.getTutInfo',name="tutinfo" ),

					# search types: autocomplete and general 
					url(r'^search/', include('core.urls')),
					url(r'^search/', include('haystack.urls')),
					url(r'^search/revision/(?P<revPk>\d+)/$','uploads.viewsrevision.getSearchRevInfo',name='searchrevinfo'),

					# user registration, activation
					url(r'^register/$', 'core.viewsregistration.MFBRegistration', name='register'),
					url(r'^accounts/confirm/(?P<activation_key>\w+)/$', 'core.viewsregistration.confirm'),
					url(r'^register/registration/message/(?P<username>\w+)/$', 'core.viewsregistration.message',name='message'),				
					url(r'^register/registration/failed/(?P<username>\w+)/$', 'core.viewsregistration.fail',name='failure'),
					url(r'^register/registration/reactivate/$', 'core.viewsresendactivationemail.resend_activation_email',name='reactivate'),

					# accounts - password managment
					url(r'^user/password/reset/$', 'django.contrib.auth.views.password_reset',name='password_reset'),
					url(r'^user/password/reset/done/$','django.contrib.auth.views.password_reset_done',name='password_reset_done'),
					url(r'^user/password/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm', {'post_reset_redirect' : '/user/password/done/'}),
					url(r'^user/password/done/$', 'django.contrib.auth.views.password_reset_complete'),

					# login, logout, user profile dashboard
					url(r'^login/$', 'core.viewslogin.loginView', name='login'),
					url(r'^logout/$', 'core.viewslogin.logoutView', name='logout'),

					# user settings
					url(r'^my_bookshelf/dashboard/$', 'uploads.viewsdashboard.getDashboard', name='dashboard'),
					url(r'^my_bookshelf/settings/$', 'core.viewssettings.user_settings', name='user_settings'),
					url(r'^my_bookshelf/settings/email/form/$', 'core.viewssettings.email_change', name='email_change'),
					url(r'^my_bookshelf/settings//email/done/$', 'core.viewssettings.email_change_done', name='email_change_done'),
					url(r'^my_bookshelf/settings/password_change/$', 'django.contrib.auth.views.password_change', name='password_change'),
					url(r'^my_bookshelf/settings/password_change/done/$', 'django.contrib.auth.views.password_change_done', name='password_change_done'),
					url(r'^my_bookshelf/settings/address_change/$', 'core.viewssettings.address_change', name='address_change'),
					url(r'^my_bookshelf/settings/address_change/done/$', 'core.viewssettings.address_change_done', name='address_change_done'),					
					url(r'^my_bookshelf/my_submissions/$', 'uploads.viewsuploadsmanager.uploadsManager', name='uploadsmanager'),

					# submission of a scene or animation file
					url(r'^my_bookshelf/upload/$', 'uploads.viewssubmission.MFBSubmission', name='upload'),
					url(r'^my_bookshelf/upload/$', 'uploads.viewssubmission.MFBSubmission', name='mfb_upload'),
					url(r'^my_bookshelf/upload/success/(?P<pk>\d+)/$', 'uploads.viewssubmission.success', name='success'),

					# directly upload user files to AWS S3 asynchronously from Submission form
					url(r'^sign_s3/$', 'uploads.viewssubmission.sign_s3',name='sign_s3'),

					# revision of a submission, pk is of submission object
					url(r'^my_bookshelf/submission/(?P<pkVid>\d+)/revise/$', 'uploads.viewsrevision.MFBRevision', name='revise'), 

					#display latest revision; pk is of Revision object]
					url(r'^my_bookshelf/upload/revision/(?P<pk>\d+)/success/$', 'uploads.viewsrevision.rev_success', name='rev_success'),
					url(r'^my_bookshelf/revision/(?P<revPk>\d+)/$', 'uploads.viewsrevision.getRevInfo', name='revinfo'),

					# specific submission object with latest revision object of a given member (end-user can be logged in or not)
					url(r'^submission/(?P<subPk>\d+)/revision/(?P<revPk>\d+)/$','uploads.viewsrevision.getSubInfo',name="subinfo" ),
					url(r'^submission/tag/success/submission/(?P<subPk>\d+)/revision/(?P<revPk>\d+)/$','uploads.viewsrevision.sub_tag_info_success',name="subtaginfo" ),
					url(r'^submission/tag/success/revision/(?P<revPk>\d+)/$','uploads.viewsrevision.rev_tag_info_success',name="revtaginfo" ),

					# admin settings
					url(r'^my_admin/settings/$', 'uploads.viewstutorial.admin_settings', name='admin_settings'),
					#url(r'^admin/blog/entry/add/$','blog.viewsentry.post_news',name="post_news" ),
					url(r'^my_admin/blog/post_news/$','blog.viewsentry.post_news',name="post_news" ),
					url(r'^admin/blog/entry/$', 'blog.viewsentry.modify_news', name="modify_news"),
					url(r'^my_admin/tutorial/post_tutorial/$','uploads.viewstutorial.post_tutorial',name="post_tutorial" ),
					url(r'^my_admin/tutorial/post_tutorial/(?P<vimeoid>\d+)/done/$','uploads.viewstutorial.post_tutorial_done',name="post_tutorial_done" ),
					url(r'^admin/uploads/tutorial/$','uploads.viewstutorial.feature_tutorial_change',name="feature_tutorial_change" ),
					url(r'^tutorial/admin/feature_tutorial/(?P<vimeoid>\d+)/done/$','uploads.viewstutorial.feature_tutorial_change_done',name="feature_tutorial_change_done" ),
					url(r'^admin/uploads/submission/$','uploads.viewstutorial.feature_animation_change',name="feature_animation_change" ),

)

if get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.dev':
	import debug_toolbar
	# adding static function to the end of the urlpatterns tells development server to map the 
	# MEDIA_URL to the files in the MEDIA_ROOT.
	urlpatterns += patterns('',
		url(r'^__debug__/', include(debug_toolbar.urls)),
	)
	urlpatterns += static(MEDIA_URL, document_root=MEDIA_ROOT)

elif get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.staging' or get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.production':
	# for admin static files on heroku
	urlpatterns += patterns('',
		url(r'^static/(?P<path>.*)$', 'django.contrib.staticfiles.views.serve', {
			'document_root': STATIC_ROOT,
		}),
	 )
