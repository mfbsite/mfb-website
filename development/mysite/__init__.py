from __future__ import absolute_import

# This will make sure the app (Celery instance) is always imported when
# Django starts so that shared_task will use this app. @shared_task will use
# use this celery instance
from .celery import app as celery_app