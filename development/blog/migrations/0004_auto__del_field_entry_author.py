# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Entry.author'
        db.delete_column(u'blog_entry', 'author_id')


    def backwards(self, orm):
        # Adding field 'Entry.author'
        db.add_column(u'blog_entry', 'author',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=datetime.datetime(2014, 7, 16, 0, 0), to=orm['auth.User']),
                      keep_default=False)


    models = {
        u'blog.entry': {
            'Meta': {'ordering': "['event_date', '-title']", 'object_name': 'Entry'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'event_date': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        }
    }

    complete_apps = ['blog']