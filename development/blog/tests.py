import datetime

from django.test import TestCase
#from django.test.client import Client
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.template import Template, Context
from django.utils.timezone import utc
from datetime import date
from django_webtest import WebTest

# Create your tests here.
from .models import Entry
from .formsentry import EntryForm


class EntryModelTest(TestCase):

	def test_unicode_representation(self):
		user = get_user_model().objects.create_user("Mayari")
		now = datetime.datetime.utcnow().replace(tzinfo=utc)
		entry = Entry(event_date=now, title="my entry title test", body="testing body")
		self.assertEqual(unicode(entry), entry.title)

	def test_verbose_name_plural(self):
		self.assertEqual(unicode(Entry._meta.verbose_name_plural), "entries")


class NewsPageTest(TestCase):

	def setUp(self):
		self.user = get_user_model().objects.create(username="Mayari")

	def test_get_event_date(self):
		now = date.today()
		entry = Entry.objects.create(event_date=now, title="My Test Title", body="This is just <a href='www.google.com'>a test</a> for the body")
		self.assertEqual(entry.get_event_date(), now.strftime("%A, %B %d, %Y"))

	def test_no_entry(self):
		response = self.client.get('/news/')
		self.assertContains(response, "There are no upcoming events.")

	def test_one_entry(self):
		now = datetime.datetime.utcnow().replace(tzinfo=utc)
		#print("now = %s"%now)
		entry = Entry.objects.create(event_date=now, title="My Test Title", body="This is just <a href='www.google.com'>a test</a> for the body")
		response = self.client.get('/news/')
		#print(response)
		self.assertContains(response, entry.get_event_date())
		self.assertContains(response, "My Test Title")
		self.assertContains(response, "This is just <a href='www.google.com'>a test</a> for the body")

	def test_many_entries(self):
		datesarray = []
		for n in range(4):
			datesarray.append(date.today())
			Entry.objects.create(event_date=datesarray[n], title="My Test Title #{0}".format(n), body="This is just <a href='www.google.com'>a test</a> for the body #{0}".format(n))
		response = self.client.get('/news/')
		#print(response)
		self.assertContains(response, datesarray[0].strftime("%A, %B %d, %Y"))
		self.assertContains(response, "My Test Title #0")
		self.assertContains(response, "This is just <a href='www.google.com'>a test</a> for the body #0")
		self.assertContains(response, datesarray[2].strftime("%A, %B %d, %Y"))
		self.assertContains(response, "My Test Title #2")
		self.assertContains(response, "This is just <a href='www.google.com'>a test</a> for the body #2")


class ViewsEntryTest(WebTest):

	def setUp(self):
		self.user = get_user_model().objects.create(username="mayari")
		self.entry = Entry.objects.create(event_date=date.today(), title="My Test Title", body="my body test")

	def test_basic_view(self):
		response = self.client.get('/news/')
		self.assertEqual(response.status_code, 200)

	def test_date_in_entry(self):
		response = self.client.get('/news/')
		self.assertContains(response, self.entry.event_date.strftime("%A, %B %d, %Y"))

	def test_title_in_entry(self):
		response = self.client.get('/news/')
		self.assertContains(response, self.entry.title)

	def test_body_in_entry(self):
		response = self.client.get('/news/')
		self.assertContains(response, self.entry.body)

	def test_view_page(self):
		page = self.app.get('/news/')
		self.assertEqual(len(page.forms), 2)


class FormsEntryTest(TestCase):
	def setUp(self):
		self.now = date.today()

	def test_valid_data(self):
		form = EntryForm( {'event_date': self.now, 'title': "My test title", 'body': "my test body"} )
		form.save()
		self.assertTrue(form.is_valid())
		entry = Entry.objects.get(event_date=self.now)
		self.assertEqual(entry.event_date, self.now)
		self.assertEqual(entry.title, "My test title")
		self.assertEqual(entry.body, "my test body")

	def test_blank_data(self):
		form = EntryForm({})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors, {
			'event_date': [u'This field is required.'],
			'title': [u'This field is required.'],
			'body': [u'This field is required.']
			})
