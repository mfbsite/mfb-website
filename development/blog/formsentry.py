# stdlib imports
import os
import logging

# core django imports
from django import forms
from django.forms import ModelForm # ability to pass some model to the form

# 3rd party imports
import magic

# project app imports
from .models import Entry
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')
		
class EntryForm(ModelForm):
	'''create a Tutorial object with form to post to Tutorial'''
	event_date		= forms.CharField(label=(u'Event Date'),widget=forms.TextInput({"placeholder":"Enter event's date"}))
	title			= forms.CharField(label=(u'Title'),widget=forms.TextInput({"placeholder":"Give it a title"}))
	body		 	= forms.CharField(label=(u'Description'),widget=forms.Textarea({"placeholder":"Tell us about your news"}))

	class Meta:
		model = Entry
		fields = ('event_date','title', 'body')