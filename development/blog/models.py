# stdlib imports

# core django imports
from django.db import models
#from django.contrib.auth.models import User

# 3rd party app imports

# project's imports


class Entry(models.Model):
	title				= models.CharField(max_length=500)
	#author				= models.ForeignKey('auth.User')
	body				= models.TextField()
	created_at			= models.DateTimeField(auto_now_add=True, editable=False)
	modified_at			= models.DateTimeField(auto_now=True)
	event_date			= models.DateField(null=True)
	#event_end_date		= models.DateTimeField(null=True)
	#slug			= models.SlugField()


	class Meta:
		verbose_name_plural = "entries"
		ordering = ["-event_date","-title"]

	def __unicode__(self):
		return self.title

	def get_event_date(self):
		return self.event_date.strftime("%A, %B %d, %Y")
