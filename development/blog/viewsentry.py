# stdlib imports
import pdb
import os
import traceback
import sys
import time
import urllib2
import json
import requests
import base64
import hmac
import urllib
from hashlib import sha1
import uuid
import re
import logging

# core django imports
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import user_passes_test
from django.core.files.uploadedfile import UploadedFile
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.template import loader
from django.core.urlresolvers import reverse
from django.db import transaction
from django.views.decorators.csrf import csrf_protect
from django.utils.translation import trans_null
# 3rd party apps

# mfb imports
from .formsentry import EntryForm
from .models import Entry
from core.models import UserProfile
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')


def user_is_staff_admin(user):
	return user.is_authenticated() and user.is_staff

# Create your views here.
@user_passes_test(user_is_staff_admin, login_url="/login/")
@csrf_protect
def post_news(request):
	#'''create and post News event object to News page '''
	#context = {}
	#return render_to_response('admin/blog/entry/add/change_form.html', context, context_instance=RequestContext(request))
	if request.method == "POST":
		logger.info("================= START POST_NEWS request.POST: %s" %(request.POST))
		entryform = EntryForm(request.POST)
		if entryform.is_valid():
			try:
				entryform.save()
			except Exception as e:
				print e
				traceback.print_exc(file=sys.stdout)
			finally:
				logger.info("================= END POST_NEWS ")
				return HttpResponseRedirect(reverse('news'))
		else:
			context = { 'entryform': entryform }
			return render_to_response('accounts/post_news_form.html', context, context_instance=RequestContext(request))
	else:
		entryform = EntryForm()
		context = { 'entryform': entryform }#, 'user': request.user}
		return render_to_response('accounts/post_news_form.html', context, context_instance=RequestContext(request))

@user_passes_test(user_is_staff_admin, login_url="/login/")
@csrf_protect
def modify_news(request):
	'''create and post News event object to News page '''
	context = {}
	return render_to_response('admin/blog/entry/change_list.html', context, context_instance=RequestContext(request))
