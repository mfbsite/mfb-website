'''
Created on Oct 23, 2012

@author: rise
'''
#**********************************************
# settings/dev.py: contains exclusive
# server specific and sensitive settings such
# as db username and password, API keys, or
# secret key, etc. and have settings.py import
# all of these values
#**********************************************
# stdlib imports
import sys

# import django core imports; 


# import 3rd party apps

# import project imports; see settings/base.py 
try:
	from .base import *
except ImportError:
	pass


###################### GENERAL CONFIGURATION
AUTH_PROFILE_MODULE = AUTH_PROFILE_MODULE

ACCOUNT_ACTIVATION_DAYS = ACCOUNT_ACTIVATION_DAYS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = LOCALTZ
################### END GENERAL CONFIGURATION


################### DEBUG CONFIGURATION 
#DEBUG = True #"DEBUG_MODE" in os.environ
#DEBUG = bool(os.environ.get('DEBUG',False))
TEMPLATE_DEBUG = DEBUG
THUMBNAIL_DEBUG = DEBUG
DEBUG_TOOLBAR_PATCH_SETTINGS = False

# tuple that specifies the full python path to the order of displayed panel
DEBUG_TOOLBAR_PANELS = [
	'debug_toolbar.panels.versions.VersionsPanel',
	'debug_toolbar.panels.timer.TimerPanel',
	'debug_toolbar.panels.settings.SettingsPanel',
	'debug_toolbar.panels.headers.HeadersPanel',
	'debug_toolbar.panels.request.RequestPanel',
	'debug_toolbar.panels.sql.SQLPanel',
	'debug_toolbar.panels.staticfiles.StaticFilesPanel',
	'debug_toolbar.panels.templates.TemplatesPanel',
	'debug_toolbar.panels.cache.CachePanel',
	'debug_toolbar.panels.signals.SignalsPanel',
	'debug_toolbar.panels.logging.LoggingPanel',
	'debug_toolbar.panels.redirects.RedirectsPanel',
]

def show_toolbar(request):
	return True # always show toolbar, for example purposes only

DEBUG_TOOLBAR_CONFIG = {
	'SHOW_TOOLBAR_CALLBACK': 'settings.dev.show_toolbar',
}

# django_debug_toolbar: required list or tuple for the built-in show_toolbar method
#INTERNAL_IPS = ('127.0.0.1',)
if DEBUG is True:
	class AllIPS(list):
		def __contains__(self, item):
			return True

	INTERNAL_IPS = AllIPS()

MIDDLEWARE_CLASSES = MIDDLEWARE_CLASSES + (
	'debug_toolbar.middleware.DebugToolbarMiddleware',
)
################### END DEBUG CONFIGURATION


###################### APP CONFIGURATION
INSTALLED_APPS = INSTALLED_APPS + (
		'django_extensions',
		'debug_toolbar',
)
################### END APP CONFIGURATION
