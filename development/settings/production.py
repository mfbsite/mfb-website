'''
Created on Mar 18, 2013

@author: rise
'''
# stdlib imports
import sys

# import django core imports;


# import 3rd party apps

# import project imports; see settings/base.py
try:
	from .base import *
except ImportError:
	pass

###################### GENERAL CONFIGURATION
DEBUG = bool(os.environ.get('DEBUG',False))

AUTH_PROFILE_MODULE = AUTH_PROFILE_MODULE

ACCOUNT_ACTIVATION_DAYS = ACCOUNT_ACTIVATION_DAYS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = LOCALTZ

ALLOWED_HOSTS = ['.molecularflipbook.org','.molecularflipbook.org.','mfbprod.herokuapp.com']
################### END GENERAL CONFIGURATION
