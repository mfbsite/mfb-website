'''
base.py: settings common to all instances of the project
Created on Mar 18, 2013

@author: rise

'''
# stdlib imports
from __future__ import absolute_import
from os.path import join, abspath, dirname
import os
import urlparse
import logging
logger = logging.getLogger(__name__)
from urlparse import urljoin

# django core
# Normally you should not import ANYTHING from Django directly
# into your settings, but ImproperlyConfigured is an exception.
from django.core.exceptions import ImproperlyConfigured

#  project's imports
#from .settings import s3utils

def get_env_variable(var_name):
	"""Get the environment variable or return exception"""
	try:
		return os.environ[var_name]
	except KeyError:
		error_msg = "Set the %s environment variable" % var_name
		raise ImproperlyConfigured(error_msg)


################### SECRET CONFIGURATION
# Make this unique, and don't share it with anybody.
SECRET_KEY = get_env_variable('SECRET_KEY') 

################### END SECRET CONFIGURATION
		

###################### PATH CONFIGURATION
ROOT = os.path.dirname(os.path.dirname(__file__))
logging.info("ROOT= %s"%ROOT)

###################### END PATH CONFIGURATION

###################### DEBUG CONFIGURATION
DEBUG = bool(os.environ.get('DEBUG',False))

###################### END DEBUG CONFIGURATION

###################### MANAGER CONFIGURATION
ADMINS = (
	(get_env_variable('SECRET_ADMIN'), get_env_variable('SECRET_EMAIL_ACCOUNT')),
)

MANAGERS = ADMINS
###################### END MANAGER CONFIGURATION


###################### GENERAL CONFIGURATION
LOCALTZ = 'America/Los_Angeles'

# provide our get_profile()
AUTH_PROFILE_MODULE = 'core.UserProfile'

ACCOUNT_ACTIVATION_DAYS = 2

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True
###################### END GENERAL CONFIGURATION

######################## VIMEO OAUTH CONFIGURATION #########
VIMEO_USERID = get_env_variable('VIMEO_USERID')
VIMEO_API_KEY = get_env_variable('VIMEO_API_KEY')
VIMEO_API_SECRET = get_env_variable('VIMEO_API_SECRET')
VIMEO_ACCESS_TOKEN = get_env_variable('VIMEO_ACCESS_TOKEN')
VIMEO_ACCESS_SECRET = get_env_variable('VIMEO_ACCESS_SECRET')

VIMEO_CALLBACK_URL = get_env_variable('VIMEO_CALLBACK_URL')
VIMEO_REQUEST_TOKEN_URL = "https://vimeo.com/oauth/request_token"
VIMEO_AUTHORIZE_URL = "https://vimeo.com/oauth/authorize"
VIMEO_ACCESS_TOKEN_URL = "https://vimeo.com/oauth/access_token"
VIMEO_METHOD_ENDPOINT = "http://vimeo.com/api/rest/v2"
VIMEO_OEMBED_URL = get_env_variable('VIMEO_OEMBED_URL')

######################## END VIMEO OAUTH CONFIGURATION #########

######################## AWS S3 CONFIGURATION
AWS_ACCESS_KEY_ID = get_env_variable('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = get_env_variable('AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = get_env_variable('AWS_STORAGE_BUCKET_NAME')
AWS_BUCKET_URL = get_env_variable('AWS_BUCKET_URL')

######################## END AWS S3 CONFIGURATION

###################### EMAIL CONFIGURATION
# Email settings for sending account activation mail messages
# Configured mail server use the following; otherwise, add
# Gmail's SMTP server setting: smtp.gmail.com
# ask me for these settings...
EMAIL_USE_TLS = get_env_variable('EMAIL_USE_TLS')
EMAIL_HOST = get_env_variable('SECRET_EMAIL_HOST')
EMAIL_HOST_USER = get_env_variable('SECRET_EMAIL_ACCOUNT')
EMAIL_HOST_PASSWORD = get_env_variable('SECRET_EMAIL_PW') 
EMAIL_PORT = get_env_variable('EMAIL_PORT')
DEFAULT_FROM_EMAIL = get_env_variable('SECRET_EMAIL_ACCOUNT')

###################### END EMAIL CONFIGURATION

####################### DATABASE CONFIGURATION
DATABASES = {
	'default': {
		'ENGINE': get_env_variable('DJANGO_DATABASE_ENGINE'), 					# Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
		'NAME': get_env_variable('DJANGO_DATABASE_NAME'),						# Or path to database file if using sqlite3.
		'USER': get_env_variable('DJANGO_DATABASE_USER'),						# Not used with sqlite3.
		'PASSWORD': get_env_variable('DJANGO_DATABASE_PASSWORD'),  				# Not used with sqlite3.
		'HOST': get_env_variable('DJANGO_DATABASE_HOST'),						# Set to empty string for localhost. Not used with sqlite3.
		'PORT': '',											# Set to empty string for default. Not used with sqlite3.
		#'OPTIONS': {'autocommit':True,},
	}
}

####################### END DATABASE CONFIGURATION

######################## HAYSTACK CONFIGURATION

HAYSTACK_CONNECTIONS = {
	'default': {
		'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
		'URL': get_env_variable('HAYSTACK_URL'),
		'INDEX_NAME': 'mfb_general',
		'TIMEOUT':60,
	},

	'autocomplete': {
		'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
		'URL': get_env_variable('HAYSTACK_URL'),
		'INDEX_NAME': 'mfb_autocomplete',
		'TIMEOUT':60,
	},
}

# enable signal processor that updates the index with additions/deletions
HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
######################## END HAYSTACK CONFIGURATION

######################## CELERY CONFIGURATION
BROKER_URL = get_env_variable('REDISCLOUD_URL') #heroku # local - 'redis://'
BROKER_TRANSPORT_OPTIONS = {'visibility_timeout':3600, 'fanout_prefix': True,'max_connections':10 } # 1 hour
BROKER_HEARTBEAT = 0
CELERY_RESULT_BACKEND = get_env_variable('REDISCLOUD_URL') #heroku # local - 'redis://'
CELERY_TASK_RESULT_EXPIRES = 3600
CELERY_ACCEPT_CONTENT = ['json','pickle']
CELERY_TRACE_APP='1 celery worker -l info' #set the CELERY_TRACE_APP environment variable to raise an exception if the app chain breaks:
#CELERY_TIMEZONE='Europe/Oslo',
CELERY_ENABLE_UTC=False # timezone issue when using retry()
CELERY_ALWAYS_EAGER=False # run tasks asynchronously
CELERY_ACKS_LATE=True
BROKER_POOL_LIMIT=1 # RedisCloud .
CELERY_REDIS_MAX_CONNECTIONS=1

######################## END CELERY CONFIGURATION

###################### DJANGO-CELERY CONFIGURATION
import djcelery
djcelery.setup_loader()

######################## END DJANGO-CELERY CONFIGURATION


###################### DJANGO-ANALYTICS / GOOGLE ANALYTICS CONFIGURATION
# GA unique id
GOOGLE_ANALYTICS_PROPERTY_ID = get_env_variable('GOOGLE_ANALYTICS_PROPERTY_ID')
# track page load times in GA
GOOGLE_ANALYTICS_SITE_SPEED = True
# track different enviroments
#GOOGLE_ANALYTICS_TRACKING_STYLE = TRACK_MULTIPLE_SUBDOMAINS
# pass the domain name to GA
#GOOGLE_ANALYTICS_DOMAIN = get_env_variable('CURRENT_SUBDOMAIN')
# view demographics and interest reports
GOOGLE_ANALYTICS_DISPLAY_ADVERTISING = True

######################## END DJANGO-ANALYTICS /GOOGLE ANALYTICS CONFIGURATION

######################## DJANGO-STORAGES CONFIGURATION
#DEFAULT_FILE_STORAGE = 'settings.s3utils.MediaRootS3BotoStorage'
#STATICFILES_STORAGE = 'settings.s3utils.StaticRootS3BotoStorage'

######################## END DJANGO-STORAGES CONFIGURATION

######################## REDIS CLOUD back-end cache CONFIGURATION ON HEROKU; COMMENT OUT FOR LOCAL DEV

redis_url = urlparse.urlparse(get_env_variable('REDISCLOUD_URL'))
CACHES = {
		'default': {
			'BACKEND': 'redis_cache.RedisCache',
			'LOCATION': '%s:%s' % (redis_url.hostname, redis_url.port),
			'OPTIONS': {
				'PASSWORD': redis_url.password,
				'DB': 0,
		}
	}
}

######################## END REDIS CLOUD CONFIGURATION ON HEROKU; COMMENT OUT FOR LOCAL DEV

###################### MEDIA CONFIGURATION

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
#MEDIA_ROOT = os.path.join(ROOT, 'media')
MEDIA_ROOT = '/media/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = urljoin(AWS_BUCKET_URL,'media/') #'/media/'

# URL prefix for admin static files -- CSS, JS and images; must use trailing slash;
ADMIN_MEDIA_PREFIX = '/static/admin/'
###################### END MEDIA CONFIGURATION


###################### STATIC CONFIGURATION
# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(ROOT, 'templates/static')

# URL prefix for static files.; including django admin ui deployed remotely
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/' #urljoin(AWS_BUCKET_URL,'static/') #'/static/'

# Additional locations of static files;absolute paths only
STATICFILES_DIRS = ( 
	os.path.join(ROOT, 'static'), 
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
	'django.contrib.staticfiles.finders.FileSystemFinder',
	'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#	'django.contrib.staticfiles.finders.DefaultFinder',
)
###################### END STATIC CONFIGURATION


###################### TEMPLATE CONFIGURATION
TEMPLATE_CONTEXT_PROCESSORS = (
								"django.contrib.auth.context_processors.auth",
								"django.core.context_processors.debug",
								"django.core.context_processors.i18n",
								"django.core.context_processors.media",
								"django.core.context_processors.static",
								"django.core.context_processors.tz",
								"django.contrib.messages.context_processors.messages",
								'django.core.context_processors.request',
								)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
	'django.template.loaders.filesystem.Loader',
	'django.template.loaders.app_directories.Loader',
#	 'django.template.loaders.eggs.Loader',
)

TEMPLATE_DIRS = os.path.join(ROOT, "templates"),

#TEMPLATE_DIRS = (
	# Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
	# Always use forward slashes, even on Windows.
	# Don't forget to use absolute paths, not relative paths.
	#'/Users/rise/Documents/workspace/mfb_website/development/templates/',

#)
###################### END TEMPLATE CONFIGURATION

###################### MIDDLEWARE CONFIGURATION
MIDDLEWARE_CLASSES = (
	'django.middleware.common.CommonMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	# Uncomment the next line for simple clickjacking protection:
	# 'django.middleware.clickjacking.XFrameOptionsMiddleware',
	#'debug_toolbar.middleware.DebugToolbarMiddleware',
)
###################### END MIDDLEWARE CONFIGURATION


###################### URL CONFIGURATION
ROOT_URLCONF = 'mysite.urls'
###################### END URL CONFIGURATION


###################### APP CONFIGURATION
DJANGO_APPS = (
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.sites',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	# Uncomment the next line to enable the admin:
	'django.contrib.admin',
	# Uncomment the next line to enable admin documentation:
	#'django.contrib.admindocs',

)

THIRD_PARTY_APPS = (
	'south',
	'haystack',
	'djcelery',
	'analytical',
)

LOCAL_APPS = (
	'blog',
	'core',
	'uploads',
)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS
######################## END APP CONFIGURATION


###################### LOGGING CONFIGURATION
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
	'version': 1,
	'disable_existing_loggers': False, #- initial 
	## where messages are to be passed
	'loggers': {
		'django_debug': {
			'handlers': ['console'],
			'propagate': True,
			'level': 'DEBUG',
		},
		'infolog':{
			'handlers':['console'],
			'propagate': True,
			'level':'INFO',
		},
		'django.request': {
			'handlers': ['mail_admins'],
			'level': 'ERROR',
			'propagate': True,
		},
		'django.db.backends':{
			'handlers':['console'],
			'propagate':True,
			'level':'DEBUG',
		}
	},
	## determine what happens to each message in a logger
	'handlers': {
		'null': {
			'level': 'DEBUG',
			'class':'django.utils.log.NullHandler',
		},
		'console':{
			'level':'DEBUG',
			'class':'logging.StreamHandler',
			'formatter':'debug',
		},
		'mail_admins': {
			'level': 'ERROR',
			'filters': ['require_debug_false'],
			'class': 'django.utils.log.AdminEmailHandler'
		}
	},
	'filters': {
		'require_debug_false': {
			'()': 'django.utils.log.RequireDebugFalse'
		}
	},
	## format the output text
	'formatters': {
		'verbose':{
			'format': ('%(asctime)s [%(process)d] [%(levelname)s] ' +
                       'pathname=%(pathname)s lineno=%(lineno)s ' +
                       'funcname=%(funcName)s %(message)s'),
            'datefmt': '%Y-%m-%d %H:%M:%S'
		},
		'debug':{
			'format':'%(levelname)s %(asctime)s %(message)s'
		},
		'simple':{
			'format':'%(levelname)s %(message)s'
		},
	}
}
###################### END LOGGING CONFIGURATION

###################### WSGI CONFIGURATION
# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'mysite.wsgi.application'
###################### END WSGI CONFIGURATION

# heroku 
# Parse database configuration from $DATABASE_URL
import dj_database_url

if os.getcwd() == "/app":
	DATABASES = {'default': dj_database_url.config(default=get_env_variable('DATABASE_URL'))} #use HEROKU_POSTGRESQL_COLORNAME_URL
else:
	DATABASES['default'] =  dj_database_url.config(default='postgres://localhost/mfb_db')

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']
