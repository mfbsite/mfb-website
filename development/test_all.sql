--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT user_id_refs_id_4dc23c39;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT user_id_refs_id_40c41112;
ALTER TABLE ONLY public.uploads_submission DROP CONSTRAINT uploads_submission_user_id_fkey;
ALTER TABLE ONLY public.uploads_submission DROP CONSTRAINT uploads_submission_filetype_id_fkey;
ALTER TABLE ONLY public.uploads_revision DROP CONSTRAINT uploads_revision_user_id_fkey;
ALTER TABLE ONLY public.uploads_revision DROP CONSTRAINT uploads_revision_submission_id_fkey;
ALTER TABLE ONLY public.easy_thumbnails_thumbnail DROP CONSTRAINT source_id_refs_id_38c628a45bffe8f5;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT group_id_refs_id_f4b32aac;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_fkey;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_fkey;
ALTER TABLE ONLY public.core_userprofile DROP CONSTRAINT core_userprofile_user_id_fkey;
ALTER TABLE ONLY public.core_userprofile DROP CONSTRAINT core_userprofile_position_id_fkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT content_type_id_refs_id_d043b34a;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_permission_id_fkey;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_fkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_permission_id_fkey;
ALTER TABLE ONLY public.core_userprofile DROP CONSTRAINT area_interest_3_id_refs_id_f28b7727;
ALTER TABLE ONLY public.core_userprofile DROP CONSTRAINT area_interest_2_id_refs_id_f28b7727;
ALTER TABLE ONLY public.core_userprofile DROP CONSTRAINT area_interest_1_id_refs_id_f28b7727;
DROP INDEX public.uploads_submission_user_id;
DROP INDEX public.uploads_submission_filetype_id;
DROP INDEX public.uploads_revision_user_id;
DROP INDEX public.uploads_revision_submission_id;
DROP INDEX public.easy_thumbnails_thumbnail_storage_hash;
DROP INDEX public.easy_thumbnails_thumbnail_source_id;
DROP INDEX public.easy_thumbnails_thumbnail_name;
DROP INDEX public.easy_thumbnails_source_storage_hash;
DROP INDEX public.easy_thumbnails_source_name;
DROP INDEX public.django_session_session_key_like;
DROP INDEX public.django_session_expire_date;
DROP INDEX public.django_admin_log_user_id;
DROP INDEX public.django_admin_log_content_type_id;
DROP INDEX public.core_userprofile_position_id;
DROP INDEX public.core_userprofile_area_interest_3_id;
DROP INDEX public.core_userprofile_area_interest_2_id;
DROP INDEX public.core_userprofile_area_interest_1_id;
DROP INDEX public.auth_user_username_like;
DROP INDEX public.auth_user_user_permissions_user_id;
DROP INDEX public.auth_user_user_permissions_permission_id;
DROP INDEX public.auth_user_groups_user_id;
DROP INDEX public.auth_user_groups_group_id;
DROP INDEX public.auth_permission_content_type_id;
DROP INDEX public.auth_group_permissions_permission_id;
DROP INDEX public.auth_group_permissions_group_id;
DROP INDEX public.auth_group_name_like;
ALTER TABLE ONLY public.uploads_submission DROP CONSTRAINT uploads_submission_pkey;
ALTER TABLE ONLY public.uploads_revision DROP CONSTRAINT uploads_revision_pkey;
ALTER TABLE ONLY public.uploads_ontology DROP CONSTRAINT uploads_ontology_pkey;
ALTER TABLE ONLY public.uploads_filetype DROP CONSTRAINT uploads_filetype_pkey;
ALTER TABLE ONLY public.south_migrationhistory DROP CONSTRAINT south_migrationhistory_pkey;
ALTER TABLE ONLY public.easy_thumbnails_thumbnail DROP CONSTRAINT easy_thumbnails_thumbnail_source_id_1f50d53db8191480_uniq;
ALTER TABLE ONLY public.easy_thumbnails_thumbnail DROP CONSTRAINT easy_thumbnails_thumbnail_pkey;
ALTER TABLE ONLY public.easy_thumbnails_source DROP CONSTRAINT easy_thumbnails_source_pkey;
ALTER TABLE ONLY public.easy_thumbnails_source DROP CONSTRAINT easy_thumbnails_source_name_7549c98cc6dd6969_uniq;
ALTER TABLE ONLY public.django_site DROP CONSTRAINT django_site_pkey;
ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_model_key;
ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
ALTER TABLE ONLY public.core_userprofile DROP CONSTRAINT core_userprofile_user_id_key;
ALTER TABLE ONLY public.core_userprofile DROP CONSTRAINT core_userprofile_pkey;
ALTER TABLE ONLY public.core_position DROP CONSTRAINT core_position_pkey;
ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_key;
ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_key;
ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_key;
ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_key;
ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
ALTER TABLE public.uploads_submission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.uploads_revision ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.uploads_ontology ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.uploads_filetype ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.south_migrationhistory ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.easy_thumbnails_thumbnail ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.easy_thumbnails_source ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_site ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_content_type ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.django_admin_log ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.core_userprofile ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.core_position ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user_groups ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_permission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.auth_group ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.uploads_submission_id_seq;
DROP TABLE public.uploads_submission;
DROP SEQUENCE public.uploads_revision_id_seq;
DROP TABLE public.uploads_revision;
DROP SEQUENCE public.uploads_ontology_id_seq;
DROP TABLE public.uploads_ontology;
DROP SEQUENCE public.uploads_filetype_id_seq;
DROP TABLE public.uploads_filetype;
DROP SEQUENCE public.south_migrationhistory_id_seq;
DROP TABLE public.south_migrationhistory;
DROP SEQUENCE public.easy_thumbnails_thumbnail_id_seq;
DROP TABLE public.easy_thumbnails_thumbnail;
DROP SEQUENCE public.easy_thumbnails_source_id_seq;
DROP TABLE public.easy_thumbnails_source;
DROP SEQUENCE public.django_site_id_seq;
DROP TABLE public.django_site;
DROP TABLE public.django_session;
DROP SEQUENCE public.django_content_type_id_seq;
DROP TABLE public.django_content_type;
DROP SEQUENCE public.django_admin_log_id_seq;
DROP TABLE public.django_admin_log;
DROP SEQUENCE public.core_userprofile_id_seq;
DROP TABLE public.core_userprofile;
DROP SEQUENCE public.core_position_id_seq;
DROP TABLE public.core_position;
DROP SEQUENCE public.auth_user_user_permissions_id_seq;
DROP TABLE public.auth_user_user_permissions;
DROP SEQUENCE public.auth_user_id_seq;
DROP SEQUENCE public.auth_user_groups_id_seq;
DROP TABLE public.auth_user_groups;
DROP TABLE public.auth_user;
DROP SEQUENCE public.auth_permission_id_seq;
DROP TABLE public.auth_permission;
DROP SEQUENCE public.auth_group_permissions_id_seq;
DROP TABLE public.auth_group_permissions;
DROP SEQUENCE public.auth_group_id_seq;
DROP TABLE public.auth_group;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: rise
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO rise;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: rise
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO rise;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO rise;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO rise;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO rise;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO rise;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO rise;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO rise;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO rise;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO rise;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO rise;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO rise;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO rise;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: core_position; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE core_position (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    "position" character varying(30) NOT NULL
);


ALTER TABLE public.core_position OWNER TO rise;

--
-- Name: core_position_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE core_position_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.core_position_id_seq OWNER TO rise;

--
-- Name: core_position_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE core_position_id_seq OWNED BY core_position.id;


--
-- Name: core_userprofile; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE core_userprofile (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    userpic character varying(250),
    activation_key character varying(40) NOT NULL,
    key_expires timestamp with time zone,
    address1 character varying(200) NOT NULL,
    address2 character varying(200) NOT NULL,
    city character varying(100) NOT NULL,
    state character varying(2) NOT NULL,
    zipcode character varying(40) NOT NULL,
    website character varying(200),
    position_id integer NOT NULL,
    area_interest_1_id integer,
    area_interest_2_id integer,
    area_interest_3_id integer
);


ALTER TABLE public.core_userprofile OWNER TO rise;

--
-- Name: core_userprofile_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE core_userprofile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.core_userprofile_id_seq OWNER TO rise;

--
-- Name: core_userprofile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE core_userprofile_id_seq OWNED BY core_userprofile.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    content_type_id integer,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO rise;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO rise;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO rise;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO rise;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO rise;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO rise;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO rise;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;


--
-- Name: easy_thumbnails_source; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE easy_thumbnails_source (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    modified timestamp with time zone NOT NULL,
    storage_hash character varying(40) NOT NULL
);


ALTER TABLE public.easy_thumbnails_source OWNER TO rise;

--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE easy_thumbnails_source_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.easy_thumbnails_source_id_seq OWNER TO rise;

--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE easy_thumbnails_source_id_seq OWNED BY easy_thumbnails_source.id;


--
-- Name: easy_thumbnails_thumbnail; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE easy_thumbnails_thumbnail (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    modified timestamp with time zone NOT NULL,
    source_id integer NOT NULL,
    storage_hash character varying(40) NOT NULL
);


ALTER TABLE public.easy_thumbnails_thumbnail OWNER TO rise;

--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE easy_thumbnails_thumbnail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.easy_thumbnails_thumbnail_id_seq OWNER TO rise;

--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE easy_thumbnails_thumbnail_id_seq OWNED BY easy_thumbnails_thumbnail.id;


--
-- Name: south_migrationhistory; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE south_migrationhistory (
    id integer NOT NULL,
    app_name character varying(255) NOT NULL,
    migration character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.south_migrationhistory OWNER TO rise;

--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE south_migrationhistory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.south_migrationhistory_id_seq OWNER TO rise;

--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE south_migrationhistory_id_seq OWNED BY south_migrationhistory.id;


--
-- Name: uploads_filetype; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE uploads_filetype (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    filetype character varying(50) NOT NULL
);


ALTER TABLE public.uploads_filetype OWNER TO rise;

--
-- Name: uploads_filetype_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE uploads_filetype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uploads_filetype_id_seq OWNER TO rise;

--
-- Name: uploads_filetype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE uploads_filetype_id_seq OWNED BY uploads_filetype.id;


--
-- Name: uploads_ontology; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE uploads_ontology (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    ontology character varying(30) NOT NULL
);


ALTER TABLE public.uploads_ontology OWNER TO rise;

--
-- Name: uploads_ontology_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE uploads_ontology_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uploads_ontology_id_seq OWNER TO rise;

--
-- Name: uploads_ontology_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE uploads_ontology_id_seq OWNED BY uploads_ontology.id;


--
-- Name: uploads_revision; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE uploads_revision (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    submission_id integer,
    user_id integer,
    sourcefile character varying(250) NOT NULL,
    vidanimation character varying(250),
    vidpic character varying(250),
    comments character varying(200) NOT NULL,
    _order integer NOT NULL
);


ALTER TABLE public.uploads_revision OWNER TO rise;

--
-- Name: uploads_revision_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE uploads_revision_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uploads_revision_id_seq OWNER TO rise;

--
-- Name: uploads_revision_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE uploads_revision_id_seq OWNED BY uploads_revision.id;


--
-- Name: uploads_submission; Type: TABLE; Schema: public; Owner: rise; Tablespace: 
--

CREATE TABLE uploads_submission (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    title character varying(250) NOT NULL,
    filetype_id integer,
    pdb character varying(100) NOT NULL,
    description text NOT NULL,
    ancestors character varying(100) NOT NULL,
    vidfeature boolean NOT NULL,
    vidstaffpick boolean NOT NULL,
    authors character varying(250),
    tags character varying(250)
);


ALTER TABLE public.uploads_submission OWNER TO rise;

--
-- Name: uploads_submission_id_seq; Type: SEQUENCE; Schema: public; Owner: rise
--

CREATE SEQUENCE uploads_submission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.uploads_submission_id_seq OWNER TO rise;

--
-- Name: uploads_submission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rise
--

ALTER SEQUENCE uploads_submission_id_seq OWNED BY uploads_submission.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY core_position ALTER COLUMN id SET DEFAULT nextval('core_position_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY core_userprofile ALTER COLUMN id SET DEFAULT nextval('core_userprofile_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY easy_thumbnails_source ALTER COLUMN id SET DEFAULT nextval('easy_thumbnails_source_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY easy_thumbnails_thumbnail ALTER COLUMN id SET DEFAULT nextval('easy_thumbnails_thumbnail_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY south_migrationhistory ALTER COLUMN id SET DEFAULT nextval('south_migrationhistory_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY uploads_filetype ALTER COLUMN id SET DEFAULT nextval('uploads_filetype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY uploads_ontology ALTER COLUMN id SET DEFAULT nextval('uploads_ontology_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY uploads_revision ALTER COLUMN id SET DEFAULT nextval('uploads_revision_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rise
--

ALTER TABLE ONLY uploads_submission ALTER COLUMN id SET DEFAULT nextval('uploads_submission_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add permission	1	add_permission
2	Can change permission	1	change_permission
3	Can delete permission	1	delete_permission
4	Can add group	2	add_group
5	Can change group	2	change_group
6	Can delete group	2	delete_group
7	Can add user	3	add_user
8	Can change user	3	change_user
9	Can delete user	3	delete_user
10	Can add content type	4	add_contenttype
11	Can change content type	4	change_contenttype
12	Can delete content type	4	delete_contenttype
13	Can add session	5	add_session
14	Can change session	5	change_session
15	Can delete session	5	delete_session
16	Can add site	6	add_site
17	Can change site	6	change_site
18	Can delete site	6	delete_site
19	Can add log entry	7	add_logentry
20	Can change log entry	7	change_logentry
21	Can delete log entry	7	delete_logentry
22	Can add migration history	8	add_migrationhistory
23	Can change migration history	8	change_migrationhistory
24	Can delete migration history	8	delete_migrationhistory
25	Can add position	9	add_position
26	Can change position	9	change_position
27	Can delete position	9	delete_position
28	Can add user profile	10	add_userprofile
29	Can change user profile	10	change_userprofile
30	Can delete user profile	10	delete_userprofile
31	Can add file type	11	add_filetype
32	Can change file type	11	change_filetype
33	Can delete file type	11	delete_filetype
34	Can add ontology	12	add_ontology
35	Can change ontology	12	change_ontology
36	Can delete ontology	12	delete_ontology
37	Can add submission	13	add_submission
38	Can change submission	13	change_submission
39	Can delete submission	13	delete_submission
40	Can add revision	14	add_revision
41	Can change revision	14	change_revision
42	Can delete revision	14	delete_revision
43	Can add source	15	add_source
44	Can change source	15	change_source
45	Can delete source	15	delete_source
46	Can add thumbnail	16	add_thumbnail
47	Can change thumbnail	16	change_thumbnail
48	Can delete thumbnail	16	delete_thumbnail
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('auth_permission_id_seq', 48, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$10000$F6ySCznk1SoZ$LSemNhTvZCCW9e5F5TbCrOz+p1pqDe/04yVQGU11Ro4=	2013-07-02 13:10:00.284162-07	t	rise			riseriyo@gmail.com	t	t	2013-04-23 14:12:51.053981-07
2	pbkdf2_sha256$10000$r57P65Hn8x35$4zpBAqA8PFH//xwCV9dMJ/+IdufEcQ3v3/fDSFjN3CU=	2013-07-09 11:23:35.576696-07	f	nugget			nugget.doe@gmail.com	f	t	2013-04-23 16:29:33.205257-07
4	pbkdf2_sha256$10000$rUXTKlAwYHGp$1TnNzvDAlax+xFny4T2QvC4nb0tW3859NGOXmxPvr6U=	2013-07-09 11:43:49.755077-07	f	josh			nugget.doe+3@gmail.com	f	t	2013-04-26 13:22:46.765558-07
3	pbkdf2_sha256$10000$IjILStDZiiPs$AUA2DtPtbbX2bhQyYPY0KFOm5lftgohrcGOStCifYGw=	2013-07-09 13:47:35.90092-07	f	mike			nugget.doe+2@gmail.com	f	t	2013-04-25 11:38:50.976856-07
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('auth_user_id_seq', 11, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: core_position; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY core_position (id, created, modified, "position") FROM stdin;
10	2013-05-30 15:51:37.198879-07	2013-05-30 15:51:37.199289-07	Full Professor
20	2013-05-30 15:51:37.203486-07	2013-05-30 15:51:37.2035-07	Associate Professor
30	2013-05-30 15:51:37.205544-07	2013-05-30 15:51:37.205557-07	Assistant Professor
40	2013-05-30 15:51:37.207709-07	2013-05-30 15:51:37.207721-07	Post-doctoral Student
50	2013-05-30 15:51:37.209772-07	2013-05-30 15:51:37.209785-07	Graduate Student
60	2013-05-30 15:51:37.212013-07	2013-05-30 15:51:37.212027-07	Undergrad
70	2013-05-30 15:51:37.214178-07	2013-05-30 15:51:37.214192-07	Lab Technician
80	2013-05-30 15:51:37.21658-07	2013-05-30 15:51:37.216594-07	Highschool Student
90	2013-05-30 15:51:37.218559-07	2013-05-30 15:51:37.218573-07	Animator
100	2013-05-30 15:51:37.220547-07	2013-05-30 15:51:37.220561-07	Other
\.


--
-- Name: core_position_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('core_position_id_seq', 100, true);


--
-- Data for Name: core_userprofile; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY core_userprofile (id, created, modified, user_id, userpic, activation_key, key_expires, address1, address2, city, state, zipcode, website, position_id, area_interest_1_id, area_interest_2_id, area_interest_3_id) FROM stdin;
1	2013-04-23 16:29:33.427436-07	2013-04-23 16:29:33.427831-07	2		88977ec1ea65e64809e085f7f596eed5d324e91f	2013-04-25 16:29:33.427359-07						\N	90	\N	\N	\N
2	2013-04-25 11:38:51.692054-07	2013-04-25 11:38:51.69258-07	3		09af1203faafdde53b43079593b32efd95310f73	2013-04-27 11:38:51.691952-07						\N	90	\N	\N	\N
3	2013-04-26 13:22:46.907708-07	2013-04-26 13:22:46.908091-07	4		5124db1d1182bad7e65ff46b4c0c2117b14f647a	2013-04-28 13:22:46.907638-07						\N	10	\N	\N	\N
\.


--
-- Name: core_userprofile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('core_userprofile_id_seq', 10, true);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY django_admin_log (id, action_time, user_id, content_type_id, object_id, object_repr, action_flag, change_message) FROM stdin;
1	2013-04-23 23:42:54.626696-07	1	13	1	test1 nug Crystal structure of the nucleosome core particle at 2.8 A resolution.	2	Changed vidfeature.
2	2013-04-24 15:42:09.056383-07	1	13	1	test1 nug Crystal structure of the nucleosome core particle at 2.8 A resolution.	2	Changed vidstaffpick.
3	2013-04-24 16:26:05.814798-07	1	13	3	test3 nug Structural changes linked to proton translocation by subunit c of the ATP synthase.	2	Changed vidstaffpick.
4	2013-04-24 16:38:25.402173-07	1	13	4	test4 nug Crystal structure of NAD(+)-dependent DNA ligase: modular architecture and functional implications.	2	Changed vidstaffpick.
5	2013-04-25 13:52:18.533231-07	1	13	7	test1 mike Atomic structure of the actin:DNase I complex.	2	Changed vidstaffpick.
6	2013-05-02 11:41:54.9855-07	1	13	10	test nug Crystal structures of truncated alphaA and alphaB crystallins reveal structural mechanisms of polydispersity important for eye lens function. 	3	
7	2013-05-06 12:54:50.470028-07	1	3	5	zoey	3	
8	2013-05-06 13:10:52.722806-07	1	3	6	zoey	3	
9	2013-05-06 13:45:10.164518-07	1	3	7	zoey	3	
10	2013-05-06 14:00:59.047809-07	1	3	8	zoey	3	
11	2013-05-06 16:42:25.543632-07	1	3	9	sam	3	
12	2013-05-06 16:57:19.7405-07	1	3	10	sam	3	
13	2013-06-18 15:55:06.131152-07	1	13	13	testing title	3	
14	2013-06-18 16:18:53.224019-07	1	14	18	Revision object	3	
15	2013-06-19 13:47:02.598774-07	1	13	14	1aoi_test.ma	3	
16	2013-06-19 13:47:21.650628-07	1	14	21	Revision object	3	
17	2013-06-19 14:58:49.450704-07	1	14	22	Revision object	3	
18	2013-06-19 14:59:06.61699-07	1	13	15	1aoi_test.mb	3	
19	2013-06-20 09:47:06.005372-07	1	13	16	1aoi_test.ma	3	
20	2013-06-20 15:12:26.012633-07	1	13	18	keynote_test.key	3	
21	2013-06-20 15:12:39.614141-07	1	14	25	Revision object	3	
22	2013-06-20 15:23:26.831283-07	1	13	17	1aoi_test.mb	2	Changed vidstaffpick.
23	2013-06-20 15:23:34.386739-07	1	13	20	testeps.eps	2	Changed vidstaffpick.
24	2013-06-20 16:05:33.423216-07	1	13	11	test7 nug rev4 Crystal structures of truncated alphaA and alphaB crystallins reveal structural mechanisms of polydispersity important for eye lens function.	3	
25	2013-06-20 16:05:33.438246-07	1	13	6	test6 nug Crystal structures of truncated alphaA and alphaB crystallins reveal structural mechanisms of polydispersity important for eye lens function.	3	
26	2013-06-20 16:05:33.444676-07	1	13	5	test5 nug rev3	3	
27	2013-06-20 16:05:33.451141-07	1	13	4	test4 nug Crystal structure of NAD(+)-dependent DNA ligase: modular architecture and functional implications.	3	
28	2013-06-20 16:05:33.457371-07	1	13	3	test3 nug Structural changes linked to proton translocation by subunit c of the ATP synthase.	3	
29	2013-06-20 16:05:33.464054-07	1	13	2	test2 nug Structure of an antibody-antigen complex: crystal structure of the HyHEL-10 Fab-lysozyme complex.	3	
30	2013-06-20 16:05:33.470305-07	1	13	1	test1 nug Crystal structure of the nucleosome core particle at 2.8 A resolution.	3	
31	2013-06-20 16:06:07.654112-07	1	13	12	1aoi_test.ma	3	
32	2013-06-20 16:06:07.664443-07	1	13	9	test3 mike Water structure of a hydrophobic protein at atomic resolution: Pentagon rings of water molecules in crystals of crambin.	3	
33	2013-06-20 16:06:07.670706-07	1	13	8	test2 mike Structure of the C-terminal region of p21(WAF1/CIP1) complexed with human PCNA.	3	
34	2013-06-20 16:06:07.676842-07	1	13	7	test1 mike Atomic structure of the actin:DNase I complex.	3	
35	2013-06-20 16:09:10.723321-07	1	13	21	suzanne_animation.blend	2	Changed vidfeature.
36	2013-06-20 16:10:49.45555-07	1	10	10	sam	3	
37	2013-06-20 16:11:30.074728-07	1	3	11	sam	3	
38	2013-06-20 16:31:34.166006-07	1	13	21	suzanne_animation.blend	3	
39	2013-06-20 16:31:34.176057-07	1	13	20	testeps.eps	3	
40	2013-06-20 16:31:34.183507-07	1	13	19	keynote_test.key	3	
41	2013-06-20 16:31:34.190253-07	1	13	17	1aoi_test.mb	3	
42	2013-06-20 19:50:22.844624-07	1	13	23	1aoi_test.ma	3	
43	2013-06-20 19:59:58.43711-07	1	13	22	Crystal structure of the nucleosome core particle at 2.8 A resolution.	2	Changed vidfeature and vidstaffpick.
44	2013-06-20 20:00:03.858773-07	1	13	25	testeps.eps	2	Changed vidstaffpick.
45	2013-06-21 10:20:13.781397-07	1	13	29	Atomic structure of the actin:DNase I complex.	2	Changed vidstaffpick.
46	2013-06-21 10:25:40.230309-07	1	13	31	Evolution of shape complementarity and catalytic efficiency from a primordial antibody template.	2	Changed vidstaffpick.
47	2013-06-26 15:37:26.998991-07	1	13	26	suzanne_animation.blend	2	Changed description.
48	2013-06-26 16:44:09.232613-07	1	13	26	suzanne_animation.blend	2	Changed description.
49	2013-06-26 16:48:47.41485-07	1	14	41	Revision object	2	Changed comments.
50	2013-06-26 22:33:59.487126-07	1	13	32	revision 2 testing blender source file upload	2	Changed vidstaffpick.
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 50, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY django_content_type (id, name, app_label, model) FROM stdin;
1	permission	auth	permission
2	group	auth	group
3	user	auth	user
4	content type	contenttypes	contenttype
5	session	sessions	session
6	site	sites	site
7	log entry	admin	logentry
8	migration history	south	migrationhistory
9	position	core	position
10	user profile	core	userprofile
11	file type	uploads	filetype
12	ontology	uploads	ontology
13	submission	uploads	submission
14	revision	uploads	revision
15	source	easy_thumbnails	source
16	thumbnail	easy_thumbnails	thumbnail
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('django_content_type_id_seq', 16, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
bp01ju4j63as50f0q8nun3p4z06yjmbg	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-05 21:51:49.552959-07
nd7xcoxuq0fpo2o7dtxubksp5fyyzf6o	Y2YyYWIzYTExZDU5OTg3NTBkYjVmNThkZjk4Y2Y3MzUxNGM3MWY5ZTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAXUu	2013-05-07 15:13:32.664169-07
mx7m6j9r6ka74eddgswkk3esk9y2nl6x	YTkyMDI1YjBkNTA0YjhlZjlkN2UwZGU2ZWE3OWYxMzRjODIwODY5YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLA3Uu	2013-05-10 15:05:30.474297-07
9k4ge87x8ggvg2v38uqotgsldih9eu00	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-06 08:36:42.202194-07
zfr4u3hsy9akjprc43euzz1z0yixfx6g	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-05-16 13:20:50.3706-07
2gvd4f4v9yc1oldpiv4poa5c7zl57uh1	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-05-07 16:42:36.048033-07
nqy6znmmxa9dbyv41uwdy0dju3j01lgj	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-05-10 16:26:04.929767-07
eix9hgfg6wo4tf2zm3vj1jxfg8zc3a8y	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-05-16 16:43:36.456671-07
a8fuppp6vsaq0xzjurmn85pzy62gt8p2	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-08 17:03:32.386975-07
fepjaj9zb22l5wm7me2kojiufmg3o3te	ZjE2NWVhNTY3MmVjMDE3YmRkMzNlOWRhMWU1NTgzNWUxY2JjY2Q1ZDqAAn1xAVgKAAAAdGVzdGNvb2tpZXECWAYAAAB3b3JrZWRxA3Mu	2013-05-12 06:40:27.889087-07
l7idumgiux83isy32wqto48mv1gj8i8c	Y2YyYWIzYTExZDU5OTg3NTBkYjVmNThkZjk4Y2Y3MzUxNGM3MWY5ZTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAXUu	2013-05-17 16:52:57.418706-07
5c6soryajxu77muufioh82g3gnjzgrhu	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-20 12:46:10.59265-07
p85ww2j8jxyspvuwcda9ietahn4vh4x2	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-09 12:01:42.792809-07
qzs1dmtdif3e7aet0jncn05nm639hqew	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-12 07:48:20.636195-07
u239zv4dbk3gldm0s3ogvoy5kox55nic	Y2YyYWIzYTExZDU5OTg3NTBkYjVmNThkZjk4Y2Y3MzUxNGM3MWY5ZTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAXUu	2013-05-20 12:48:54.987824-07
98o7y8yhd0gwbg931fo6wa7h98cud0t5	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-09 12:04:48.552941-07
7zh938ohnlnmarbkbnj2a3lo1ybb06tr	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-12 16:11:46.24753-07
91cylaosteztlpxbhm59t2if9tl8q6e7	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-09 12:05:36.970412-07
raw5oa6rr5bs8yx79czam02bha3py97r	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-12 16:42:12.061667-07
i0tgsjvjwyufw4ka36f5tujlznwz47vb	Y2YyYWIzYTExZDU5OTg3NTBkYjVmNThkZjk4Y2Y3MzUxNGM3MWY5ZTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAXUu	2013-05-09 12:06:25.1345-07
b0yvnivr8895uqsf5pckvazjvkphj00g	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-12 16:44:00.276886-07
wrwpjc1x7x9jvb9jzz3md5iaow7gb2q9	YTkyMDI1YjBkNTA0YjhlZjlkN2UwZGU2ZWE3OWYxMzRjODIwODY5YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLA3Uu	2013-05-09 13:47:36.655477-07
s7i0g7mf2m75kgehpjxpefnvfpe1y2ck	Y2YyYWIzYTExZDU5OTg3NTBkYjVmNThkZjk4Y2Y3MzUxNGM3MWY5ZTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAXUu	2013-05-09 13:52:10.191361-07
0cchu1c95f43ginb25kn03ppqaazjxtq	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-12 16:50:27.45381-07
rxi5l2r8b5hrd73q7eg8kbtedej27a1a	YjVlMDNhZTBkYzNjMDY5ZjAwZGUwMzU5NDc3ZjExYjRhYzNkMDZjYTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLC3Uu	2013-05-20 16:58:38.26438-07
3819aozbuhn74zvcf5iqh4we0vet5mrd	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-09 15:48:33.474141-07
0lhnyojrz9xzfdm08c8eoorwfhraoxf6	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-05-28 11:27:50.451125-07
pnulrp8711munb9ty8axm5v00zsxx5eh	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-12 16:53:13.31048-07
fwj0rj3lhnnqzi35z82je8no2iljr44r	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-10 11:16:10.192941-07
8m2eqmdjz3f5h9oc7l06onr0ldwpxul2	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-13 11:35:14.135826-07
7yypwyy6exn88h6ahrmbutol0azmamq9	YTkyMDI1YjBkNTA0YjhlZjlkN2UwZGU2ZWE3OWYxMzRjODIwODY5YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLA3Uu	2013-05-28 14:22:34.283894-07
l6jkwa750byfvmts5fg2t8ddxk4dqr0g	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-13 12:12:13.288971-07
gstnov6cjm0afd1b70x36accjvukmgwp	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-29 11:06:53.149775-07
xgartw8cxfu395mc2e3w5l4ept8a26or	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-13 13:34:59.193938-07
qqp2oz81eeruddi3n7kgrpxj6mffedu8	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-05-29 11:07:17.529866-07
dn3pv6vldk5mwzo0d0bz1wpuvavk3bt3	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-13 13:37:11.47209-07
535usuhvqa9hxo5piu7rh9qi1evgp4yi	YTkyMDI1YjBkNTA0YjhlZjlkN2UwZGU2ZWE3OWYxMzRjODIwODY5YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLA3Uu	2013-05-10 14:30:23.385491-07
zfsllggo2pd85fez6ljffqdpmajj1rqc	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-13 13:39:16.718014-07
pvcx5b8iitfdybvjts3vpcgfgdv90b2v	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-13 14:43:12.384886-07
s3j58tk7je8gg2q69650ocmszdd0i4rv	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-05-13 14:50:33.74292-07
x76eg6fp280495csfpppyy44efg1wdxo	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-07 15:11:39.253085-07
qqp453hqkk11e2j6mycfvdwsw0imnw8w	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-10 19:59:07.260336-07
izda0ywryyzhtysed8ck8fs39srwleh3	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-05-30 17:11:23.77948-07
7p3vwtpucoc9n7vc44tji36cnemjjwb0	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-11 11:20:40.483527-07
5icrss2idnn2oszj6n6f1c23jdp2t2lh	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-07-08 22:57:15.812227-07
lt4au4qc6kg1mqkjj8b020dp5ftmtnwc	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-12 14:54:53.285997-07
v2ykavy73oayjb5wyxg0zdjdfer5905t	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-05-31 09:10:58.29328-07
1q8kmmox79x0m1kajv60hqh10lydpshp	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-01 15:01:33.703956-07
9j8fkgl19xctr8g6jsc4dynq8p4ntnqk	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-12 16:05:12.88026-07
oifvz76mrz7xq09y4sdtl6dw67gpxu9i	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-13 01:44:23.774376-07
ieugm96oh41igkpd7rzqi86uhujberf7	Y2YyYWIzYTExZDU5OTg3NTBkYjVmNThkZjk4Y2Y3MzUxNGM3MWY5ZTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAXUu	2013-07-04 15:07:31.321628-07
89dubg3ekj55650pcbvne93nu2wbs2ed	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-13 01:46:30.252139-07
ooqqizqbjkk7632zagox8svpbi7jb0x8	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-05 10:36:16.311281-07
aidsf6fm45h8pzlvs8h78uslo9ym6sr4	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-14 13:21:02.422414-07
ehyhb9by7q8l7bc74e09ths2df7waq1z	YTkyMDI1YjBkNTA0YjhlZjlkN2UwZGU2ZWE3OWYxMzRjODIwODY5YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLA3Uu	2013-06-14 13:21:12.170044-07
5mjgjxa6cke2ihssaycdisnt2ufumwxh	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-14 01:13:33.006666-07
080x3vdd4x34dx0beoeguhnfr1bgddf1	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-05 16:47:33.505841-07
ebpxjig3tigpji3ogn53rdcz1dyygq8b	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-14 13:31:36.282006-07
9t1fnazvvlvag8tudt6mwgjyqvklca05	NjZiYTFjZjE5ZGEyZGE2NjMwMTJmZjY2OGE4NzM0OThkM2NjZTdkMDqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLBHUu	2013-06-14 13:45:58.034061-07
fpjk4wz6fvd2ynb81l5pfcv10wvoba8b	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-14 15:46:33.597954-07
mtskm20lhyjw64f4da11d45sq83p81qq	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-14 20:16:09.264946-07
qylzp02rb03d8kbm7gppfrhykdmk0ehy	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-17 11:08:53.652982-07
al88zdak6p0b79893z70vhlhf77dcsgs	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-14 05:45:19.39276-07
dmg1j74r0rfxbqsjijzy82gnegh13mhz	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-14 05:49:02.651536-07
amvvtb8iwpr0antlq27g48fnbvlc2h42	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-18 10:26:37.956609-07
7inlenz5xigdxunuxgkp3fjgoju7dhz8	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-14 06:06:38.941017-07
hab3euezxmjge4dji4sx222vzyotqf7f	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-14 06:07:07.891997-07
p65383vl9d5xkblt12ronpno7hlcov4p	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-14 06:08:31.661735-07
9xe31lqkp3oc40zgfu8ze0w3cz1he36e	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-14 06:21:20.829648-07
hs67t49wfmjgsnvikt0ixl2aaqftpz6l	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-18 13:44:32.405136-07
fcszzazfrr3ux0ls9v67tpngzqzswj1l	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-18 13:45:36.813803-07
4pj3qhvc8psor7srhwlxotlcb2w7vl3a	YTkyMDI1YjBkNTA0YjhlZjlkN2UwZGU2ZWE3OWYxMzRjODIwODY5YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLA3Uu	2013-06-18 13:46:36.940221-07
dc88q3xy4urt4ie280gv4q70uedqmmrf	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-14 06:34:26.965625-07
jatkoxgesjnnh1wblwjgi9ur1s997lzu	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-14 06:39:49.507811-07
9emyxd2hnwcxxkmq196wl662lg50f0oc	NjZiYTFjZjE5ZGEyZGE2NjMwMTJmZjY2OGE4NzM0OThkM2NjZTdkMDqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLBHUu	2013-06-18 14:52:29.920728-07
8kpbm09u62mjy9fvqwgsvxxqh5y9z3mc	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-18 15:50:33.53333-07
rh834dcsnqpz885zojczq9t5k6a55m44	NjZiYTFjZjE5ZGEyZGE2NjMwMTJmZjY2OGE4NzM0OThkM2NjZTdkMDqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLBHUu	2013-06-18 16:31:29.430132-07
x0wvsqcjeifx894bzpm0xrqnnurd914p	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-18 16:32:32.995431-07
2o3ihi8ipnym9tjam1srnqlv9i60m6cn	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-18 16:36:02.253052-07
akwdglrpc657j6hby4f52igfv253w3fi	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-07-22 18:02:48.5645-07
5o3m53i2xhmy6xauskfg0i4yhrwf9h22	YTkyMDI1YjBkNTA0YjhlZjlkN2UwZGU2ZWE3OWYxMzRjODIwODY5YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLA3Uu	2013-06-20 16:42:21.077232-07
1u6kkui9frrjedw8xuzadg6frxtgiv85	YTkyMDI1YjBkNTA0YjhlZjlkN2UwZGU2ZWE3OWYxMzRjODIwODY5YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLA3Uu	2013-06-21 04:42:27.281074-07
io3tr0zrcvkzv8sl8q9fddo9rjuudl3w	YTkyMDI1YjBkNTA0YjhlZjlkN2UwZGU2ZWE3OWYxMzRjODIwODY5YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLA3Uu	2013-06-21 04:44:37.854576-07
ltaqhb3mtnhb4md7mrvi2dvuddqfras8	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-20 14:10:24.656519-07
gppou3flureg95jsfu302qbxyww65jch	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-07-23 14:47:56.4232-07
f3wcd20esairuccz9pc2z9gbu8enko68	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-21 04:56:46.1726-07
hmr49z612qrfji7jegtgkl7gg9p32fme	Y2YyYWIzYTExZDU5OTg3NTBkYjVmNThkZjk4Y2Y3MzUxNGM3MWY5ZTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAXUu	2013-07-10 16:48:17.201141-07
8niy7rvvqguqxyt3qq1165rz6tdtnlek	YTkyMDI1YjBkNTA0YjhlZjlkN2UwZGU2ZWE3OWYxMzRjODIwODY5YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLA3Uu	2013-06-21 05:17:13.932475-07
1ubo6feuwy47yepw5q11pcw55h0jddjg	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-06-21 10:10:11.976001-07
44faqstmju4ofprnigysl6abrp188kp1	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-06-21 10:55:42.0911-07
hycu4gen0gnv8hhv1f3eszvrujw8dsmg	YTkyMDI1YjBkNTA0YjhlZjlkN2UwZGU2ZWE3OWYxMzRjODIwODY5YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLA3Uu	2013-06-21 21:41:38.450213-07
hq89l6d9lcrc3tbwc690d2mmzcjg7lgu	YTkyMDI1YjBkNTA0YjhlZjlkN2UwZGU2ZWE3OWYxMzRjODIwODY5YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLA3Uu	2013-06-21 22:04:16.427407-07
u97cneonxpxa54ov0csvvo67k05lak6r	ZjE2NWVhNTY3MmVjMDE3YmRkMzNlOWRhMWU1NTgzNWUxY2JjY2Q1ZDqAAn1xAVgKAAAAdGVzdGNvb2tpZXECWAYAAAB3b3JrZWRxA3Mu	2013-07-02 15:57:15.207154-07
cdr864df426yvzgvrio3gvfauscsf0ub	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-07-02 15:58:50.270934-07
r3b6zdma2933bdak0oav5f97xe2u9jwm	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-07-04 09:59:42.026386-07
ldhllxq9776zqemy6kk0sajfyfu8t7l1	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-07-10 23:29:57.233343-07
s6v505hzugaz3aj97buce0ptfyef4m16	YTkyMDI1YjBkNTA0YjhlZjlkN2UwZGU2ZWE3OWYxMzRjODIwODY5YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLA3Uu	2013-07-04 16:09:38.502231-07
ugbc1r3sw2vjmnd4abdfd0q1hmpyfnah	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-07-04 19:29:56.825416-07
5lnoorluv9ishq23b1vqo2bvuzaeb90k	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-07-11 12:52:03.696931-07
mn83xnd2rngggub96y83g059eiu07470	YTkyMDI1YjBkNTA0YjhlZjlkN2UwZGU2ZWE3OWYxMzRjODIwODY5YTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLA3Uu	2013-07-05 09:54:47.920404-07
43roxcp5uqgvjdnhittm7mhzl911ufy4	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-07-11 12:52:30.623342-07
notjp7qvhxmnjtic5darpfz1nz5o63y0	Y2YyYWIzYTExZDU5OTg3NTBkYjVmNThkZjk4Y2Y3MzUxNGM3MWY5ZTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAXUu	2013-07-05 09:56:42.570871-07
oc7jjfp0xd73ggzg6cat4cxksskoig47	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-07-05 10:04:27.615878-07
se986clij5ljncnu3rnbtryolqc5irur	MzAzMTdlMzg2ZDY2MDNhY2RhYzM5YzQyNzU0YzBkZmYxNTVmZDFhMjqAAn1xAS4=	2013-07-11 15:44:31.226918-07
e4y9msg2jbgoy2lmm7z96lhgwuvus4ux	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-07-11 15:45:49.248131-07
r0idvr6cpzze848p0njr97g9h48uwaut	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-07-12 10:05:08.64997-07
cvg5auzw3xok921mdcwhjo0xroxmd60b	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-07-13 13:55:00.647915-07
u6s4541qmsjm1hkmjajklcnitaakvawy	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-07-15 20:09:46.538933-07
4607zs9mrut1qqzrre3vdv4uih1n08xf	ZmFiMmM4MzgxN2MwMjQ3NTY0MjdjYTZhODYzNzJmNDNhODdiOGMzODqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAnUu	2013-07-16 11:45:02.646824-07
0hav4zewx1o95fvosay2q6ag8txwtejw	Y2YyYWIzYTExZDU5OTg3NTBkYjVmNThkZjk4Y2Y3MzUxNGM3MWY5ZTqAAn1xAShVEl9hdXRoX3VzZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHEDVQ1fYXV0aF91c2VyX2lkcQRLAXUu	2013-07-16 13:10:00.292028-07
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('django_site_id_seq', 1, true);


--
-- Data for Name: easy_thumbnails_source; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY easy_thumbnails_source (id, name, modified, storage_hash) FROM stdin;
17	icons/generic-icon-1024x576-key.png	2013-06-05 17:42:23-07	f9bde26a1556cd667f742bd34ec7c55e
19	icons/generic-icon-1024x576-blend.png	2013-06-05 17:00:30-07	f9bde26a1556cd667f742bd34ec7c55e
20	images/uid_2/4d2faa14-6aad-4006-84d3-738159eae6b9/2013-06-21_00:12:58.455035/4d2faa14-6aad-4006-84d3-738159eae6b9.png	2013-06-20 17:12:58-07	f9bde26a1556cd667f742bd34ec7c55e
16	icons/generic-icon-1024x576-mb.png	2013-06-20 19:43:00-07	f9bde26a1556cd667f742bd34ec7c55e
18	icons/generic-icon-1024x576-eps.png	2013-06-20 19:46:37-07	f9bde26a1556cd667f742bd34ec7c55e
21	images/uid_2/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_04:29:32.941820/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.png	2013-06-20 21:29:33-07	f9bde26a1556cd667f742bd34ec7c55e
22	icons/generic-icon-1024x576-pptx.png	2013-06-20 19:45:31-07	f9bde26a1556cd667f742bd34ec7c55e
23	icons/generic-icon-1024x576-dae.png	2013-06-20 21:47:35-07	f9bde26a1556cd667f742bd34ec7c55e
24	images/uid_3/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_05:20:38.607612/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.png	2013-06-20 22:20:38-07	f9bde26a1556cd667f742bd34ec7c55e
25	images/uid_3/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_16:58:43.719870/41873087-a607-480b-ba40-c40f8404a0e8.png	2013-06-21 09:58:43-07	f9bde26a1556cd667f742bd34ec7c55e
26	images/uid_2/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_17:25:14.475557/41873087-a607-480b-ba40-c40f8404a0e8.png	2013-06-21 10:25:14-07	f9bde26a1556cd667f742bd34ec7c55e
27	images/uid_2/8eb9b74e-e232-4db8-b97f-d15a401b42b9/2013-06-24_19:30:11.916263/8eb9b74e-e232-4db8-b97f-d15a401b42b9.png	2013-06-24 12:30:11-07	f9bde26a1556cd667f742bd34ec7c55e
28	images/uid_3/38896f09-cb6b-4435-9f72-166b277c07bc/2013-06-26_23:52:55.313180/38896f09-cb6b-4435-9f72-166b277c07bc.png	2013-06-26 16:52:55-07	f9bde26a1556cd667f742bd34ec7c55e
29	images/uid_3/623a7b8b-525a-4fb8-80b7-29df92c7ba86/2013-06-27_05:27:49.494528/623a7b8b-525a-4fb8-80b7-29df92c7ba86.png	2013-06-26 22:27:49-07	f9bde26a1556cd667f742bd34ec7c55e
30	icons/generic-icon-1024x576-pdf.png	2013-06-05 17:14:51-07	f9bde26a1556cd667f742bd34ec7c55e
31	images/uid_2/3d2cd07d-b468-416a-88fe-b93fbf10c4d0/2013-06-28_17:21:51.923496/3d2cd07d-b468-416a-88fe-b93fbf10c4d0.png	2013-06-28 10:21:52-07	f9bde26a1556cd667f742bd34ec7c55e
\.


--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('easy_thumbnails_source_id_seq', 31, true);


--
-- Data for Name: easy_thumbnails_thumbnail; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY easy_thumbnails_thumbnail (id, name, modified, source_id, storage_hash) FROM stdin;
72	images/uid_2/4d2faa14-6aad-4006-84d3-738159eae6b9/2013-06-21_00:12:58.455035/thumbnails/4d2faa14-6aad-4006-84d3-738159eae6b9.png.120x68_q85_scale.jpg	2013-06-20 17:12:58-07	20	d26becbf46ac48eda79c7a39a13a02dd
73	images/uid_2/4d2faa14-6aad-4006-84d3-738159eae6b9/2013-06-21_00:12:58.455035/thumbnails/4d2faa14-6aad-4006-84d3-738159eae6b9.png.120x90_q85_scale.jpg	2013-06-20 17:12:58-07	20	d26becbf46ac48eda79c7a39a13a02dd
74	images/uid_2/4d2faa14-6aad-4006-84d3-738159eae6b9/2013-06-21_00:12:58.455035/thumbnails/4d2faa14-6aad-4006-84d3-738159eae6b9.png.844x473_q85_scale.jpg	2013-06-20 17:12:58-07	20	d26becbf46ac48eda79c7a39a13a02dd
75	images/uid_2/4d2faa14-6aad-4006-84d3-738159eae6b9/2013-06-21_00:12:58.455035/thumbnails/4d2faa14-6aad-4006-84d3-738159eae6b9.png.185x104_q85_crop.jpg	2013-06-20 17:12:58-07	20	d26becbf46ac48eda79c7a39a13a02dd
76	images/uid_2/4d2faa14-6aad-4006-84d3-738159eae6b9/2013-06-21_00:12:58.455035/thumbnails/4d2faa14-6aad-4006-84d3-738159eae6b9.png.844x473_q85.jpg	2013-06-20 20:01:15-07	20	d26becbf46ac48eda79c7a39a13a02dd
77	images/uid_2/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_04:29:32.941820/thumbnails/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.png.120x68_q85_scale.jpg	2013-06-20 21:29:33-07	21	d26becbf46ac48eda79c7a39a13a02dd
78	images/uid_2/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_04:29:32.941820/thumbnails/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.png.120x90_q85_scale.jpg	2013-06-20 21:29:33-07	21	d26becbf46ac48eda79c7a39a13a02dd
79	images/uid_2/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_04:29:32.941820/thumbnails/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.png.844x473_q85_scale.jpg	2013-06-20 21:29:33-07	21	d26becbf46ac48eda79c7a39a13a02dd
80	images/uid_2/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_04:29:32.941820/thumbnails/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.png.185x104_q85_crop.jpg	2013-06-20 21:29:33-07	21	d26becbf46ac48eda79c7a39a13a02dd
81	icons/thumbnails/generic-icon-1024x576-pptx.png.120x90_q85_scale.jpg	2013-06-20 21:40:22-07	22	d26becbf46ac48eda79c7a39a13a02dd
82	icons/thumbnails/generic-icon-1024x576-dae.png.120x90_q85_scale.jpg	2013-06-20 21:48:12-07	23	d26becbf46ac48eda79c7a39a13a02dd
83	icons/thumbnails/generic-icon-1024x576-dae.png.120x68_q85_scale.jpg	2013-06-20 22:17:55-07	23	d26becbf46ac48eda79c7a39a13a02dd
84	images/uid_3/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_05:20:38.607612/thumbnails/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.png.120x68_q85_scale.jpg	2013-06-20 22:20:38-07	24	d26becbf46ac48eda79c7a39a13a02dd
85	images/uid_3/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_05:20:38.607612/thumbnails/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.png.120x90_q85_scale.jpg	2013-06-20 22:20:38-07	24	d26becbf46ac48eda79c7a39a13a02dd
86	images/uid_3/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_05:20:38.607612/thumbnails/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.png.844x473_q85_scale.jpg	2013-06-20 22:20:38-07	24	d26becbf46ac48eda79c7a39a13a02dd
87	images/uid_3/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_05:20:38.607612/thumbnails/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.png.185x104_q85_crop.jpg	2013-06-20 22:20:38-07	24	d26becbf46ac48eda79c7a39a13a02dd
88	icons/thumbnails/generic-icon-1024x576-pptx.png.120x68_q85_scale.jpg	2013-06-20 22:24:11-07	22	d26becbf46ac48eda79c7a39a13a02dd
89	images/uid_3/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_16:58:43.719870/thumbnails/41873087-a607-480b-ba40-c40f8404a0e8.png.120x68_q85_scale.jpg	2013-06-21 09:58:43-07	25	d26becbf46ac48eda79c7a39a13a02dd
90	images/uid_3/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_16:58:43.719870/thumbnails/41873087-a607-480b-ba40-c40f8404a0e8.png.120x90_q85_scale.jpg	2013-06-21 09:58:43-07	25	d26becbf46ac48eda79c7a39a13a02dd
91	images/uid_3/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_16:58:43.719870/thumbnails/41873087-a607-480b-ba40-c40f8404a0e8.png.844x473_q85_scale.jpg	2013-06-21 09:58:44-07	25	d26becbf46ac48eda79c7a39a13a02dd
92	images/uid_3/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_16:58:43.719870/thumbnails/41873087-a607-480b-ba40-c40f8404a0e8.png.185x104_q85_crop.jpg	2013-06-21 09:58:44-07	25	d26becbf46ac48eda79c7a39a13a02dd
93	images/uid_2/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_17:25:14.475557/thumbnails/41873087-a607-480b-ba40-c40f8404a0e8.png.120x68_q85_scale.jpg	2013-06-21 10:25:14-07	26	d26becbf46ac48eda79c7a39a13a02dd
94	images/uid_2/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_17:25:14.475557/thumbnails/41873087-a607-480b-ba40-c40f8404a0e8.png.120x90_q85_scale.jpg	2013-06-21 10:25:14-07	26	d26becbf46ac48eda79c7a39a13a02dd
95	images/uid_2/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_17:25:14.475557/thumbnails/41873087-a607-480b-ba40-c40f8404a0e8.png.844x473_q85_scale.jpg	2013-06-21 10:25:14-07	26	d26becbf46ac48eda79c7a39a13a02dd
96	images/uid_2/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_17:25:14.475557/thumbnails/41873087-a607-480b-ba40-c40f8404a0e8.png.185x104_q85_crop.jpg	2013-06-21 10:25:15-07	26	d26becbf46ac48eda79c7a39a13a02dd
97	images/uid_2/8eb9b74e-e232-4db8-b97f-d15a401b42b9/2013-06-24_19:30:11.916263/thumbnails/8eb9b74e-e232-4db8-b97f-d15a401b42b9.png.120x68_q85_scale.jpg	2013-06-24 12:30:12-07	27	d26becbf46ac48eda79c7a39a13a02dd
98	images/uid_2/8eb9b74e-e232-4db8-b97f-d15a401b42b9/2013-06-24_19:30:11.916263/thumbnails/8eb9b74e-e232-4db8-b97f-d15a401b42b9.png.120x90_q85_scale.jpg	2013-06-24 12:30:12-07	27	d26becbf46ac48eda79c7a39a13a02dd
99	images/uid_2/8eb9b74e-e232-4db8-b97f-d15a401b42b9/2013-06-24_19:30:11.916263/thumbnails/8eb9b74e-e232-4db8-b97f-d15a401b42b9.png.844x473_q85_scale.jpg	2013-06-24 12:30:12-07	27	d26becbf46ac48eda79c7a39a13a02dd
100	images/uid_2/8eb9b74e-e232-4db8-b97f-d15a401b42b9/2013-06-24_19:30:11.916263/thumbnails/8eb9b74e-e232-4db8-b97f-d15a401b42b9.png.185x104_q85_crop.jpg	2013-06-24 12:30:12-07	27	d26becbf46ac48eda79c7a39a13a02dd
101	images/uid_3/38896f09-cb6b-4435-9f72-166b277c07bc/2013-06-26_23:52:55.313180/thumbnails/38896f09-cb6b-4435-9f72-166b277c07bc.png.120x68_q85_scale.jpg	2013-06-26 16:52:55-07	28	d26becbf46ac48eda79c7a39a13a02dd
102	images/uid_3/38896f09-cb6b-4435-9f72-166b277c07bc/2013-06-26_23:52:55.313180/thumbnails/38896f09-cb6b-4435-9f72-166b277c07bc.png.120x90_q85_scale.jpg	2013-06-26 16:52:55-07	28	d26becbf46ac48eda79c7a39a13a02dd
103	images/uid_3/38896f09-cb6b-4435-9f72-166b277c07bc/2013-06-26_23:52:55.313180/thumbnails/38896f09-cb6b-4435-9f72-166b277c07bc.png.844x473_q85_scale.jpg	2013-06-26 16:52:55-07	28	d26becbf46ac48eda79c7a39a13a02dd
104	images/uid_3/38896f09-cb6b-4435-9f72-166b277c07bc/2013-06-26_23:52:55.313180/thumbnails/38896f09-cb6b-4435-9f72-166b277c07bc.png.185x104_q85_crop.jpg	2013-06-26 16:52:55-07	28	d26becbf46ac48eda79c7a39a13a02dd
105	icons/thumbnails/generic-icon-1024x576-blend.png.844x473_q85_scale.jpg	2013-06-26 22:26:55-07	19	d26becbf46ac48eda79c7a39a13a02dd
106	images/uid_3/623a7b8b-525a-4fb8-80b7-29df92c7ba86/2013-06-27_05:27:49.494528/thumbnails/623a7b8b-525a-4fb8-80b7-29df92c7ba86.png.120x68_q85_scale.jpg	2013-06-26 22:27:49-07	29	d26becbf46ac48eda79c7a39a13a02dd
107	images/uid_3/623a7b8b-525a-4fb8-80b7-29df92c7ba86/2013-06-27_05:27:49.494528/thumbnails/623a7b8b-525a-4fb8-80b7-29df92c7ba86.png.120x90_q85_scale.jpg	2013-06-26 22:27:49-07	29	d26becbf46ac48eda79c7a39a13a02dd
108	images/uid_3/623a7b8b-525a-4fb8-80b7-29df92c7ba86/2013-06-27_05:27:49.494528/thumbnails/623a7b8b-525a-4fb8-80b7-29df92c7ba86.png.844x473_q85_scale.jpg	2013-06-26 22:27:49-07	29	d26becbf46ac48eda79c7a39a13a02dd
109	images/uid_3/623a7b8b-525a-4fb8-80b7-29df92c7ba86/2013-06-27_05:27:49.494528/thumbnails/623a7b8b-525a-4fb8-80b7-29df92c7ba86.png.185x104_q85_crop.jpg	2013-06-26 22:27:49-07	29	d26becbf46ac48eda79c7a39a13a02dd
110	icons/thumbnails/generic-icon-1024x576-pdf.png.120x90_q85_scale.jpg	2013-06-27 15:11:07-07	30	d26becbf46ac48eda79c7a39a13a02dd
111	icons/thumbnails/generic-icon-1024x576-pdf.png.120x68_q85_scale.jpg	2013-06-27 15:16:43-07	30	d26becbf46ac48eda79c7a39a13a02dd
112	images/uid_2/3d2cd07d-b468-416a-88fe-b93fbf10c4d0/2013-06-28_17:21:51.923496/thumbnails/3d2cd07d-b468-416a-88fe-b93fbf10c4d0.png.120x68_q85_scale.jpg	2013-06-28 10:21:52-07	31	d26becbf46ac48eda79c7a39a13a02dd
113	images/uid_2/3d2cd07d-b468-416a-88fe-b93fbf10c4d0/2013-06-28_17:21:51.923496/thumbnails/3d2cd07d-b468-416a-88fe-b93fbf10c4d0.png.120x90_q85_scale.jpg	2013-06-28 10:21:52-07	31	d26becbf46ac48eda79c7a39a13a02dd
114	images/uid_2/3d2cd07d-b468-416a-88fe-b93fbf10c4d0/2013-06-28_17:21:51.923496/thumbnails/3d2cd07d-b468-416a-88fe-b93fbf10c4d0.png.844x473_q85_scale.jpg	2013-06-28 10:21:52-07	31	d26becbf46ac48eda79c7a39a13a02dd
115	images/uid_2/3d2cd07d-b468-416a-88fe-b93fbf10c4d0/2013-06-28_17:21:51.923496/thumbnails/3d2cd07d-b468-416a-88fe-b93fbf10c4d0.png.185x104_q85_crop.jpg	2013-06-28 10:21:53-07	31	d26becbf46ac48eda79c7a39a13a02dd
116	icons/thumbnails/generic-icon-1024x576-pdf.png.844x473_q85_scale.jpg	2013-07-02 16:22:56-07	30	d26becbf46ac48eda79c7a39a13a02dd
117	icons/thumbnails/generic-icon-1024x576-dae.png.844x473_q85_scale.jpg	2013-07-02 16:23:03-07	23	d26becbf46ac48eda79c7a39a13a02dd
64	icons/thumbnails/generic-icon-1024x576-mb.png.120x68_q85_scale.jpg	2013-06-20 19:49:14-07	16	d26becbf46ac48eda79c7a39a13a02dd
69	icons/thumbnails/generic-icon-1024x576-blend.png.120x90_q85_scale.jpg	2013-06-20 16:02:07-07	19	d26becbf46ac48eda79c7a39a13a02dd
65	icons/thumbnails/generic-icon-1024x576-key.png.120x90_q85_scale.jpg	2013-06-20 16:02:07-07	17	d26becbf46ac48eda79c7a39a13a02dd
70	icons/thumbnails/generic-icon-1024x576-blend.png.120x68_q85_scale.jpg	2013-06-20 16:04:24-07	19	d26becbf46ac48eda79c7a39a13a02dd
66	icons/thumbnails/generic-icon-1024x576-key.png.120x68_q85_scale.jpg	2013-06-20 16:04:25-07	17	d26becbf46ac48eda79c7a39a13a02dd
71	icons/thumbnails/generic-icon-1024x576-blend.png.844x473_q85.jpg	2013-06-20 16:09:19-07	19	d26becbf46ac48eda79c7a39a13a02dd
63	icons/thumbnails/generic-icon-1024x576-mb.png.120x90_q85_scale.jpg	2013-06-20 19:54:50-07	16	d26becbf46ac48eda79c7a39a13a02dd
67	icons/thumbnails/generic-icon-1024x576-eps.png.120x90_q85_scale.jpg	2013-06-20 19:58:58-07	18	d26becbf46ac48eda79c7a39a13a02dd
68	icons/thumbnails/generic-icon-1024x576-eps.png.120x68_q85_scale.jpg	2013-06-20 19:59:06-07	18	d26becbf46ac48eda79c7a39a13a02dd
\.


--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('easy_thumbnails_thumbnail_id_seq', 117, true);


--
-- Data for Name: south_migrationhistory; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY south_migrationhistory (id, app_name, migration, applied) FROM stdin;
18	easy_thumbnails	0001_initial	2013-04-23 15:06:51.007908-07
19	django_extensions	0001_empty	2013-04-23 15:08:17.832846-07
20	core	0001_initial	2013-04-23 15:09:29.043335-07
21	uploads	0001_initial	2013-04-23 15:10:45.753412-07
22	uploads	0002_auto__chg_field_filetype_filetype	2013-05-14 16:58:17.890244-07
23	uploads	0003_auto__del_field_submission_ontology_field1__del_field_submission_ontol	2013-05-30 15:32:14.118319-07
\.


--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('south_migrationhistory_id_seq', 23, true);


--
-- Data for Name: uploads_filetype; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY uploads_filetype (id, created, modified, filetype) FROM stdin;
0	2013-05-14 16:58:38.457184-07	2013-05-14 16:58:38.457198-07	Select file format of source file
10	2013-05-30 15:54:15.195335-07	2013-05-30 15:54:15.195863-07	.mfb
20	2013-05-30 15:54:15.202017-07	2013-05-30 15:54:15.202032-07	.blend
30	2013-05-30 15:54:15.204437-07	2013-05-30 15:54:15.204451-07	.ma/.mb
40	2013-05-30 15:54:15.206857-07	2013-05-30 15:54:15.20687-07	.cd4
50	2013-05-30 15:54:15.208807-07	2013-05-30 15:54:15.208821-07	.max/.3ds
60	2013-05-30 15:54:15.210987-07	2013-05-30 15:54:15.211001-07	.dae
70	2013-05-30 15:54:15.212934-07	2013-05-30 15:54:15.212948-07	.x3d
80	2013-05-30 15:54:15.214838-07	2013-05-30 15:54:15.214851-07	.pdf
90	2013-05-30 15:54:15.216845-07	2013-05-30 15:54:15.21686-07	.key
100	2013-05-30 15:54:15.219003-07	2013-05-30 15:54:15.219017-07	.ppt/.pptx
110	2013-05-30 15:54:15.220803-07	2013-05-30 15:54:15.220817-07	.ai
120	2013-05-30 15:54:15.222921-07	2013-05-30 15:54:15.222934-07	.psd/.eps
130	2013-05-30 15:54:15.225012-07	2013-05-30 15:54:15.225025-07	.xcf
140	2013-05-30 15:54:15.229067-07	2013-05-30 15:54:15.229096-07	Other file format
\.


--
-- Name: uploads_filetype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('uploads_filetype_id_seq', 140, true);


--
-- Data for Name: uploads_ontology; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY uploads_ontology (id, created, modified, ontology) FROM stdin;
10	2013-05-14 16:58:38.496326-07	2013-05-14 16:58:38.496342-07	Gene Ontology
20	2013-05-14 16:58:38.500061-07	2013-05-14 16:58:38.500075-07	Organism Ontology
30	2013-05-14 16:58:38.502343-07	2013-05-14 16:58:38.502358-07	Biological Process
40	2013-05-14 16:58:38.504576-07	2013-05-14 16:58:38.504589-07	Cell Type
50	2013-05-14 16:58:38.506735-07	2013-05-14 16:58:38.506748-07	Cell Line
60	2013-05-14 16:58:38.508842-07	2013-05-14 16:58:38.508856-07	Anatomical Entity
70	2013-05-14 16:58:38.511167-07	2013-05-14 16:58:38.511181-07	Cellular Component
80	2013-05-14 16:58:38.513404-07	2013-05-14 16:58:38.513419-07	Molecular Function
90	2013-05-14 16:58:38.515789-07	2013-05-14 16:58:38.515802-07	NCBI Organism
\.


--
-- Name: uploads_ontology_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('uploads_ontology_id_seq', 90, true);


--
-- Data for Name: uploads_revision; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY uploads_revision (id, created, modified, submission_id, user_id, sourcefile, vidanimation, vidpic, comments, _order) FROM stdin;
31	2013-06-20 19:54:50.139721-07	2013-06-20 19:54:50.212126-07	24	2	scenes/uid_3/59236845-62d7-4194-98bd-80aa800265b2/2013-06-21_02:54:50.139721/59236845-62d7-4194-98bd-80aa800265b2.ma		icons/generic-icon-1024x576-mb.png		0
32	2013-06-20 19:58:58.330741-07	2013-06-20 19:58:58.377717-07	25	1	scenes/uid_2/59236845-62d7-4194-98bd-80aa800265b2/2013-06-21_02:58:58.330741/59236845-62d7-4194-98bd-80aa800265b2.eps		icons/generic-icon-1024x576-eps.png		0
33	2013-06-20 21:29:32.94182-07	2013-06-20 21:29:33.057569-07	26	1	scenes/uid_2/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_04:29:32.941820/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.blend		images/uid_2/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_04:29:32.941820/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.png		0
34	2013-06-20 21:40:22.271632-07	2013-06-20 21:40:22.312945-07	27	1	scenes/uid_2/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_04:40:22.271632/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.pptx		icons/generic-icon-1024x576-pptx.png		0
35	2013-06-20 21:43:18.662392-07	2013-06-20 21:43:18.708701-07	28	2	scenes/uid_3/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_04:43:18.662392/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.dae	videos/uid_3/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_04:43:18.662392/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.avi	icons/generic-icon-1024x576-dae.png		0
36	2013-06-20 22:20:38.607612-07	2013-06-20 22:20:38.644736-07	29	2	scenes/uid_3/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_05:20:38.607612/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.psd		images/uid_3/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_05:20:38.607612/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.png		0
37	2013-06-21 09:58:43.71987-07	2013-06-21 09:58:43.789577-07	30	2	scenes/uid_3/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_16:58:43.719870/41873087-a607-480b-ba40-c40f8404a0e8.x3d		images/uid_3/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_16:58:43.719870/41873087-a607-480b-ba40-c40f8404a0e8.png		0
38	2013-06-21 10:25:14.475557-07	2013-06-21 10:25:14.536513-07	31	1	scenes/uid_2/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_17:25:14.475557/41873087-a607-480b-ba40-c40f8404a0e8.key		images/uid_2/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_17:25:14.475557/41873087-a607-480b-ba40-c40f8404a0e8.png		0
39	2013-06-21 10:36:24.801193-07	2013-06-21 10:36:24.920299-07	32	2	scenes/uid_3/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_17:36:24.801193/41873087-a607-480b-ba40-c40f8404a0e8.blend		icons/generic-icon-1024x576-blend.png		0
40	2013-06-24 12:30:11.916263-07	2013-06-24 12:30:11.988806-07	33	1	scenes/uid_2/8eb9b74e-e232-4db8-b97f-d15a401b42b9/2013-06-24_19:30:11.916263/8eb9b74e-e232-4db8-b97f-d15a401b42b9.xcf		images/uid_2/8eb9b74e-e232-4db8-b97f-d15a401b42b9/2013-06-24_19:30:11.916263/8eb9b74e-e232-4db8-b97f-d15a401b42b9.png		0
41	2013-06-26 15:35:36.135884-07	2013-06-26 16:48:47.407511-07	26	1	scenes/uid_2/5322257d-c600-40b7-8864-5f9905eb97c8/2013-06-26_22:35:36.135884/5322257d-c600-40b7-8864-5f9905eb97c8.blend		images/uid_2/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d/2013-06-21_04:29:32.941820/64a2f37d-fc41-4dac-ad9c-09d3d5dca51d.png	testing no png, no vid, sta-non, img forward populate	0
42	2013-06-26 16:52:55.31318-07	2013-06-26 16:52:55.713905-07	32	2	scenes/uid_3/38896f09-cb6b-4435-9f72-166b277c07bc/2013-06-26_23:52:55.313180/38896f09-cb6b-4435-9f72-166b277c07bc.blend	videos/uid_3/38896f09-cb6b-4435-9f72-166b277c07bc/2013-06-26_23:52:55.313180/38896f09-cb6b-4435-9f72-166b277c07bc.avi	images/uid_3/38896f09-cb6b-4435-9f72-166b277c07bc/2013-06-26_23:52:55.313180/38896f09-cb6b-4435-9f72-166b277c07bc.png	testing rev2 new vid, new png	0
43	2013-06-26 17:18:58.676463-07	2013-06-26 17:18:58.780628-07	30	2	scenes/uid_3/0e9eb026-b89c-48bb-adc2-deae3064802e/2013-06-27_00:18:58.676463/0e9eb026-b89c-48bb-adc2-deae3064802e.x3d		images/uid_3/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_16:58:43.719870/41873087-a607-480b-ba40-c40f8404a0e8.png	testing; initial yes png, 2nd rev no png	0
44	2013-06-26 22:24:48.252945-07	2013-06-26 22:24:48.561527-07	34	2	scenes/uid_3/623a7b8b-525a-4fb8-80b7-29df92c7ba86/2013-06-27_05:24:48.252945/623a7b8b-525a-4fb8-80b7-29df92c7ba86.blend		icons/generic-icon-1024x576-blend.png		0
45	2013-06-26 22:26:54.924381-07	2013-06-26 22:26:55.019227-07	34	2	scenes/uid_3/623a7b8b-525a-4fb8-80b7-29df92c7ba86/2013-06-27_05:26:54.924381/623a7b8b-525a-4fb8-80b7-29df92c7ba86.blend		icons/generic-icon-1024x576-blend.png	testing 2nd rev - no png, no vid	0
46	2013-06-26 22:27:49.494528-07	2013-06-26 22:27:49.864579-07	34	2	scenes/uid_3/623a7b8b-525a-4fb8-80b7-29df92c7ba86/2013-06-27_05:27:49.494528/623a7b8b-525a-4fb8-80b7-29df92c7ba86.blend		images/uid_3/623a7b8b-525a-4fb8-80b7-29df92c7ba86/2013-06-27_05:27:49.494528/623a7b8b-525a-4fb8-80b7-29df92c7ba86.png	testing 3rd rev - yes png, no vid	0
47	2013-06-26 22:29:13.053575-07	2013-06-26 22:29:13.14864-07	34	2	scenes/uid_3/623a7b8b-525a-4fb8-80b7-29df92c7ba86/2013-06-27_05:29:13.053575/623a7b8b-525a-4fb8-80b7-29df92c7ba86.blend	videos/uid_3/623a7b8b-525a-4fb8-80b7-29df92c7ba86/2013-06-27_05:29:13.053575/623a7b8b-525a-4fb8-80b7-29df92c7ba86.avi	images/uid_3/623a7b8b-525a-4fb8-80b7-29df92c7ba86/2013-06-27_05:27:49.494528/623a7b8b-525a-4fb8-80b7-29df92c7ba86.png	testing 4th rev - no png, yes vid; s/t/a - n/o/n	0
48	2013-06-26 23:14:37.238426-07	2013-06-26 23:14:37.344804-07	31	1	scenes/uid_2/f77e06cc-e5ff-4b64-bffd-f2492cee49da/2013-06-27_06:14:37.238426/f77e06cc-e5ff-4b64-bffd-f2492cee49da.key		images/uid_2/41873087-a607-480b-ba40-c40f8404a0e8/2013-06-21_17:25:14.475557/41873087-a607-480b-ba40-c40f8404a0e8.png	testing 2nd rev no png upload	0
49	2013-06-27 14:12:49.692032-07	2013-06-27 14:12:49.739459-07	35	1	scenes/uid_2/29f0ab22-b27d-4793-8bef-6c3d448378f7/2013-06-27_21:12:49.692032/29f0ab22-b27d-4793-8bef-6c3d448378f7.dae		icons/generic-icon-1024x576-dae.png		0
50	2013-06-27 15:11:07.373703-07	2013-06-27 15:11:07.499157-07	36	1	scenes/uid_2/29f0ab22-b27d-4793-8bef-6c3d448378f7/2013-06-27_22:11:07.373703/29f0ab22-b27d-4793-8bef-6c3d448378f7.pdf		icons/generic-icon-1024x576-pdf.png		0
51	2013-06-28 10:17:58.795999-07	2013-06-28 10:17:58.979754-07	37	1	scenes/uid_2/3d2cd07d-b468-416a-88fe-b93fbf10c4d0/2013-06-28_17:17:58.795999/3d2cd07d-b468-416a-88fe-b93fbf10c4d0.blend		icons/generic-icon-1024x576-blend.png		0
52	2013-06-28 10:21:51.923496-07	2013-06-28 10:21:53.309902-07	37	1	scenes/uid_2/3d2cd07d-b468-416a-88fe-b93fbf10c4d0/2013-06-28_17:21:51.923496/3d2cd07d-b468-416a-88fe-b93fbf10c4d0.mb	videos/uid_2/3d2cd07d-b468-416a-88fe-b93fbf10c4d0/2013-06-28_17:21:51.923496/3d2cd07d-b468-416a-88fe-b93fbf10c4d0.wmv	images/uid_2/3d2cd07d-b468-416a-88fe-b93fbf10c4d0/2013-06-28_17:21:51.923496/3d2cd07d-b468-416a-88fe-b93fbf10c4d0.png	test rev1 no png, no vid; rev2 yes png, yes vid	0
53	2013-06-28 10:25:12.955259-07	2013-06-28 10:25:13.245017-07	37	1	scenes/uid_2/3d2cd07d-b468-416a-88fe-b93fbf10c4d0/2013-06-28_17:25:12.955259/3d2cd07d-b468-416a-88fe-b93fbf10c4d0.blend	videos/uid_2/3d2cd07d-b468-416a-88fe-b93fbf10c4d0/2013-06-28_17:21:51.923496/3d2cd07d-b468-416a-88fe-b93fbf10c4d0.wmv	images/uid_2/3d2cd07d-b468-416a-88fe-b93fbf10c4d0/2013-06-28_17:21:51.923496/3d2cd07d-b468-416a-88fe-b93fbf10c4d0.png	rev3 no png, no vid, should forward populate	0
54	2013-07-02 11:55:18.340774-07	2013-07-02 11:55:18.464648-07	37	1	scenes/uid_2/e0113f8a-ffea-44f8-96e0-486c5cea327d/2013-07-02_18:55:18.340774/e0113f8a-ffea-44f8-96e0-486c5cea327d.blend	videos/uid_2/3d2cd07d-b468-416a-88fe-b93fbf10c4d0/2013-06-28_17:21:51.923496/3d2cd07d-b468-416a-88fe-b93fbf10c4d0.wmv	images/uid_2/3d2cd07d-b468-416a-88fe-b93fbf10c4d0/2013-06-28_17:21:51.923496/3d2cd07d-b468-416a-88fe-b93fbf10c4d0.png	testing no png, no vid uploads	0
29	2013-06-20 17:12:58.455035-07	2013-06-20 17:12:58.525933-07	22	1	scenes/uid_2/4d2faa14-6aad-4006-84d3-738159eae6b9/2013-06-21_00:12:58.455035/4d2faa14-6aad-4006-84d3-738159eae6b9.mb	videos/uid_2/4d2faa14-6aad-4006-84d3-738159eae6b9/2013-06-21_00:12:58.455035/4d2faa14-6aad-4006-84d3-738159eae6b9.mp4	images/uid_2/4d2faa14-6aad-4006-84d3-738159eae6b9/2013-06-21_00:12:58.455035/4d2faa14-6aad-4006-84d3-738159eae6b9.png		0
\.


--
-- Name: uploads_revision_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('uploads_revision_id_seq', 54, true);


--
-- Data for Name: uploads_submission; Type: TABLE DATA; Schema: public; Owner: rise
--

COPY uploads_submission (id, created, modified, user_id, title, filetype_id, pdb, description, ancestors, vidfeature, vidstaffpick, authors, tags) FROM stdin;
30	2013-06-21 09:58:43.71976-07	2013-06-26 17:18:58.790932-07	2	testing Structure of the C-terminal region of p21(WAF1/CIP1) complexed with human PCNA.	70	1axc, waf1, cip1	The crystal structure of the human DNA polymerase delta processivity factor PCNA (proliferating cell nuclear antigen) complexed with a 22 residue peptide derived from the C-terminus of the cell-cycle checkpoint protein p21(WAF1/CIP1) has been determined at 2.6 angstrom resolution. p21 binds to PCNA in a 1:1 stoichiometry with an extensive array of interactions that include the formation of a beta sheet with the interdomain connector loop of PCNA. An intact trimeric ring is maintained in the structure of the p21-PCNA complex, with a central hole available for DNA interaction. The ability of p21 to inhibit the action of PCNA is therefore likely to be due to its masking of elements on PCNA that are required for the binding of other components of the polymerase assembly. 	testing links 	f	f	mike	human pcna, c-terminal region
25	2013-06-20 19:58:58.330639-07	2013-06-20 20:00:03.851586-07	1	testeps.eps	120	2ptc	testing generic eps 	testing links	f	t	nugget	nucleosome, actin
27	2013-06-20 21:40:22.271531-07	2013-06-20 21:40:22.297673-07	1	testppt.pptx	100	1crn	testing no thumbnail upload 	testing	f	f	nugget	actin
28	2013-06-20 21:43:18.662299-07	2013-06-20 21:43:18.688045-07	2	tsetdae.dae	60	1aoi	testing dae with only video upload, no thumbnail	testing links	f	f	mike	actin, nucleosome
24	2013-06-20 19:54:50.13962-07	2013-06-20 19:54:50.197007-07	2	1aoi_test.ma	30	1aoi, 2ptc	testing no thumbnail nor video uploads, 1aoi_test.ma file	testing links	f	f	mike	1aoi. crystal structure, nucleosome
22	2013-06-20 17:12:58.45494-07	2013-06-20 19:59:58.429924-07	1	Crystal structure of the nucleosome core particle at 2.8 A resolution.	30	1aoi	The X-ray crystal structure of the nucleosome core particle of chromatin shows in atomic detail how the histone protein octamer is assembled and how 146 base pairs of DNA are organized into a superhelix around it. Both histone/histone and histone/DNA interactions depend on the histone fold domains and additional, well ordered structure elements extending from this motif. Histone amino-terminal tails pass over and between the gyres of the DNA superhelix to contact neighbouring particles. The lack of uniformity between multiple histone/DNA-binding sites causes the DNA to deviate from ideal superhelix geometry. 	testing links	t	t	nugget	nucleosome, crystal structure
29	2013-06-20 22:20:38.607509-07	2013-06-21 10:20:13.771338-07	2	Atomic structure of the actin:DNase I complex.	120	1atn	testing psd; The atomic models of the complex between rabbit skeletal muscle actin and bovine pancreatic deoxyribonuclease I both in the ATP and ADP forms have been determined by X-ray analysis at an effective resolution of 2.8 A and 3A, respectively. The two structures are very similar. The actin molecule consists of two domains which can be further subdivided into two subdomains. ADP or ATP is located in the cleft between the domains with a calcium ion bound to the beta- or beta- and gamma-phosphates, respectively. The motif of a five-stranded beta sheet consisting of a beta meander and a right handed beta alpha beta unit appears in each domain suggesting that gene duplication might have occurred. These sheets have the same topology as that found in hexokinase. 	testing links	f	t	mike	psd, actin
35	2013-06-27 14:12:49.691937-07	2013-06-27 14:12:49.719041-07	1	Identification and analysis of a bottleneck in PCB biodegradation.	60	1lgt, 1lkd	testing no png, no vid, sta-nxx; The microbial degradation of polychlorinated biphenyls (PCBs) provides the potential to destroy these widespread, toxic and persistent environmental pollutants. For example, the four-step upper bph pathway transforms some of the more than 100 different PCBs found in commercial mixtures and is being engineered for more effective PCB degradation. In the critical third step of this pathway, 2,3-dihydroxybiphenyl (DHB) 1,2-dioxygenase (DHBD; EC 1.13.11.39) catalyzes aromatic ring cleavage. Here we demonstrate that ortho-chlorinated PCB metabolites strongly inhibit DHBD, promote its suicide inactivation and interfere with the degradation of other compounds. For example, k(cat)(app) for 2',6'-diCl DHB was reduced by a factor of approximately 7,000 relative to DHB, and it bound with sufficient affinity to competitively inhibit DHB cleavage at nanomolar concentrations. Crystal structures of two complexes of DHBD with ortho-chlorinated metabolites at 1.7 A resolution reveal an explanation for these phenomena, which have important implications for bioremediation strategies. 	testing links	f	f	nugget	Binding Sites, Biodegradation, Environmental, Catalysis, Crystallography, X-Ray, Dioxygenases, Environmental Pollutants, Enzyme Inhibitors, Kinetics, Macromolecular Substances, Models, Chemical, Models, Molecular, Oxygenases
33	2013-06-24 12:30:11.916169-07	2013-06-24 12:30:11.975983-07	1	Structural changes linked to proton translocation by subunit c of the ATP synthase.	130	1c17	xcf upload; F1F0 ATP synthases use a transmembrane proton gradient to drive the synthesis of cellular ATP. The structure of the cytosolic F1 portion of the enzyme and the basic mechanism of ATP hydrolysis by F1 are now well established, but how proton translocation through the transmembrane F0 portion drives these catalytic changes is less clear. Here we describe the structural changes in the proton-translocating F0 subunit c that are induced by deprotonating the specific aspartic acid involved in proton transport. Conformational changes between the protonated and deprotonated forms of subunit c provide the structural basis for an explicit mechanism to explain coupling of proton translocation by F0 to the rotation of subunits within the core of F1. Rotation of these subunits within F1 causes the catalytic conformational changes in the active sites of F1 that result in ATP synthesis. 	testing links	f	f	nugget	proton-translocation, atp synthase
32	2013-06-21 10:36:24.800992-07	2013-06-26 22:33:59.479809-07	2	revision 2 testing blender source file upload	20	2ptc, 1crn	testing description, initial no vid, yes png; 2nd revision, yes vid, yes new png	testing links	f	t	mike	tag, 
34	2013-06-26 22:24:48.252842-07	2013-06-26 22:29:13.160064-07	2	The structure of a retinal-forming carotenoid oxygenase.	20	2biw	testing: 1st rev - no png, no vid; Enzymes that produce retinal and related apocarotenoids constitute a sequence- and thus structure-related family, a member of which was analyzed by x-ray diffraction. This member is an oxygenase and contains an Fe2+-4-His arrangement at the axis of a seven-bladed beta-propeller chain fold covered by a dome formed by six large loops. The Fe2+ is accessible through a long nonpolar tunnel that holds a carotenoid derivative in one of the crystals. On binding, three consecutive double bonds of this carotenoid changed from a straight all-trans to a cranked cis-trans-cis conformation. The remaining trans bond is located at the dioxygen-ligated Fe2+ and cleaved by oxygen. 	testing links	f	f	mike	amino acid sequence, cloning, molecular, crystallography, oxygenases
26	2013-06-20 21:29:32.941718-07	2013-06-26 16:44:09.224455-07	1	suzanne_animation.blend	20	2ptc	testing upload of thumbnail but not video; testing 2nd revision: no png, forward populate png, no vid STA-NON	testing links	f	f	nugget	blender, monkey
31	2013-06-21 10:25:14.475311-07	2013-06-26 23:14:37.355505-07	1	Evolution of shape complementarity and catalytic efficiency from a primordial antibody template.	90	1c1e	The crystal structure of an efficient Diels-Alder antibody catalyst at 1.9 angstrom resolution reveals almost perfect shape complementarity with its transition state analog. Comparison with highly related progesterone and Diels-Alderase antibodies that arose from the same primordial germ line template shows the relatively subtle mutational steps that were able to evolve both structural complementarity and catalytic efficiency. 	testing links	f	t	nugget	Diels-Alder antibody, crystal structure
36	2013-06-27 15:11:07.37361-07	2013-06-27 15:11:07.475243-07	1	Structure of an antibody-antigen complex: crystal structure of the HyHEL-10 Fab-lysozyme complex.	80	3hfm	testing rev1 sta - nxx; The crystal structure of the complex of the anti-lysozyme HyHEL-10 Fab and hen egg white lysozyme has been determined to a nominal resolution of 3.0 A. The antigenic determinant (epitope) on the lysozyme is discontinuous, consisting of residues from four different regions of the linear sequence. It consists of the exposed residues of an alpha-helix together with surrounding amino acids. The epitope crosses the active-site cleft and includes a tryptophan located within this cleft. The combining site of the antibody is mostly flat with a protuberance made up of two tyrosines that penetrate the cleft. All six complementarity-determining regions of the Fab contribute at least one residue to the binding; one residue from the framework is also in contact with the lysozyme. The contacting residues on the antibody contain a disproportionate number of aromatic side chains. The antibody-antigen contact mainly involves hydrogen bonds and van der Waals interactions; there is one ion-pair interaction but it is weak. 	testing links	f	f	nugget	Antibodies, Monoclonal, Antigen-Antibody Complex, Computer Simulation, Crystallization, Immunoglobulin Fab Fragments, Immunoglobulin G, Models, Molecular, Muramidase, Protein Conformation, X-Ray Diffraction 
37	2013-06-28 10:17:58.795793-07	2013-07-02 11:55:18.47501-07	1	Revision 3: The Geometry of the Reactive Site and of the Peptide Groups in Trypsin, Trypsinogen and its Complexes with Inhibitors	20	2ptc	testing description, blender rev3. no png, no vid uploads	testing links	f	f	nugget	Peptide Groups, Trypsin, Trypsinogen
\.


--
-- Name: uploads_submission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rise
--

SELECT pg_catalog.setval('uploads_submission_id_seq', 37, true);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: core_position_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY core_position
    ADD CONSTRAINT core_position_pkey PRIMARY KEY (id);


--
-- Name: core_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY core_userprofile
    ADD CONSTRAINT core_userprofile_pkey PRIMARY KEY (id);


--
-- Name: core_userprofile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY core_userprofile
    ADD CONSTRAINT core_userprofile_user_id_key UNIQUE (user_id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_key; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_key UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_source_name_7549c98cc6dd6969_uniq; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY easy_thumbnails_source
    ADD CONSTRAINT easy_thumbnails_source_name_7549c98cc6dd6969_uniq UNIQUE (name, storage_hash);


--
-- Name: easy_thumbnails_source_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY easy_thumbnails_source
    ADD CONSTRAINT easy_thumbnails_source_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_thumbnail_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_thumbnail_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_thumbnail_source_id_1f50d53db8191480_uniq; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_thumbnail_source_id_1f50d53db8191480_uniq UNIQUE (source_id, name, storage_hash);


--
-- Name: south_migrationhistory_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY south_migrationhistory
    ADD CONSTRAINT south_migrationhistory_pkey PRIMARY KEY (id);


--
-- Name: uploads_filetype_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY uploads_filetype
    ADD CONSTRAINT uploads_filetype_pkey PRIMARY KEY (id);


--
-- Name: uploads_ontology_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY uploads_ontology
    ADD CONSTRAINT uploads_ontology_pkey PRIMARY KEY (id);


--
-- Name: uploads_revision_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY uploads_revision
    ADD CONSTRAINT uploads_revision_pkey PRIMARY KEY (id);


--
-- Name: uploads_submission_pkey; Type: CONSTRAINT; Schema: public; Owner: rise; Tablespace: 
--

ALTER TABLE ONLY uploads_submission
    ADD CONSTRAINT uploads_submission_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_like; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX auth_group_name_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX auth_group_permissions_group_id ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX auth_group_permissions_permission_id ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX auth_permission_content_type_id ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX auth_user_groups_group_id ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX auth_user_groups_user_id ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_permission_id ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_user_id ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_like; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX auth_user_username_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: core_userprofile_area_interest_1_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX core_userprofile_area_interest_1_id ON core_userprofile USING btree (area_interest_1_id);


--
-- Name: core_userprofile_area_interest_2_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX core_userprofile_area_interest_2_id ON core_userprofile USING btree (area_interest_2_id);


--
-- Name: core_userprofile_area_interest_3_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX core_userprofile_area_interest_3_id ON core_userprofile USING btree (area_interest_3_id);


--
-- Name: core_userprofile_position_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX core_userprofile_position_id ON core_userprofile USING btree (position_id);


--
-- Name: django_admin_log_content_type_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX django_admin_log_content_type_id ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX django_admin_log_user_id ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX django_session_expire_date ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_like; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX django_session_session_key_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: easy_thumbnails_source_name; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX easy_thumbnails_source_name ON easy_thumbnails_source USING btree (name);


--
-- Name: easy_thumbnails_source_storage_hash; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX easy_thumbnails_source_storage_hash ON easy_thumbnails_source USING btree (storage_hash);


--
-- Name: easy_thumbnails_thumbnail_name; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX easy_thumbnails_thumbnail_name ON easy_thumbnails_thumbnail USING btree (name);


--
-- Name: easy_thumbnails_thumbnail_source_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX easy_thumbnails_thumbnail_source_id ON easy_thumbnails_thumbnail USING btree (source_id);


--
-- Name: easy_thumbnails_thumbnail_storage_hash; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX easy_thumbnails_thumbnail_storage_hash ON easy_thumbnails_thumbnail USING btree (storage_hash);


--
-- Name: uploads_revision_submission_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX uploads_revision_submission_id ON uploads_revision USING btree (submission_id);


--
-- Name: uploads_revision_user_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX uploads_revision_user_id ON uploads_revision USING btree (user_id);


--
-- Name: uploads_submission_filetype_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX uploads_submission_filetype_id ON uploads_submission USING btree (filetype_id);


--
-- Name: uploads_submission_user_id; Type: INDEX; Schema: public; Owner: rise; Tablespace: 
--

CREATE INDEX uploads_submission_user_id ON uploads_submission USING btree (user_id);


--
-- Name: area_interest_1_id_refs_id_f28b7727; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY core_userprofile
    ADD CONSTRAINT area_interest_1_id_refs_id_f28b7727 FOREIGN KEY (area_interest_1_id) REFERENCES uploads_ontology(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: area_interest_2_id_refs_id_f28b7727; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY core_userprofile
    ADD CONSTRAINT area_interest_2_id_refs_id_f28b7727 FOREIGN KEY (area_interest_2_id) REFERENCES uploads_ontology(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: area_interest_3_id_refs_id_f28b7727; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY core_userprofile
    ADD CONSTRAINT area_interest_3_id_refs_id_f28b7727 FOREIGN KEY (area_interest_3_id) REFERENCES uploads_ontology(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: content_type_id_refs_id_d043b34a; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT content_type_id_refs_id_d043b34a FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: core_userprofile_position_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY core_userprofile
    ADD CONSTRAINT core_userprofile_position_id_fkey FOREIGN KEY (position_id) REFERENCES core_position(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: core_userprofile_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY core_userprofile
    ADD CONSTRAINT core_userprofile_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_fkey FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: group_id_refs_id_f4b32aac; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT group_id_refs_id_f4b32aac FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: source_id_refs_id_38c628a45bffe8f5; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY easy_thumbnails_thumbnail
    ADD CONSTRAINT source_id_refs_id_38c628a45bffe8f5 FOREIGN KEY (source_id) REFERENCES easy_thumbnails_source(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: uploads_revision_submission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY uploads_revision
    ADD CONSTRAINT uploads_revision_submission_id_fkey FOREIGN KEY (submission_id) REFERENCES uploads_submission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: uploads_revision_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY uploads_revision
    ADD CONSTRAINT uploads_revision_user_id_fkey FOREIGN KEY (user_id) REFERENCES core_userprofile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: uploads_submission_filetype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY uploads_submission
    ADD CONSTRAINT uploads_submission_filetype_id_fkey FOREIGN KEY (filetype_id) REFERENCES uploads_filetype(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: uploads_submission_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY uploads_submission
    ADD CONSTRAINT uploads_submission_user_id_fkey FOREIGN KEY (user_id) REFERENCES core_userprofile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_40c41112; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT user_id_refs_id_40c41112 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_4dc23c39; Type: FK CONSTRAINT; Schema: public; Owner: rise
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT user_id_refs_id_4dc23c39 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: rise
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM rise;
GRANT ALL ON SCHEMA public TO rise;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

