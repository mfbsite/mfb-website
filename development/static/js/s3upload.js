(function() {

  window.S3Upload = (function() {

    //S3Upload.prototype.s3_object_name = 'default_name';

    S3Upload.prototype.csrftoken = 'csrftoken';

    S3Upload.prototype.s3_sign_put_url = '/signS3put';

    S3Upload.prototype.file_dom_selector = 'file_upload';

    S3Upload.prototype.onFinishS3Put = function(public_url, file) {
      var public_url = public_url;
      return console.log('base.onFinishS3Put()', public_url, file);
    };

    S3Upload.prototype.onProgress = function(percent, status, public_url, file) {
      return console.log('base.onProgress()', percent, status);
    };

    S3Upload.prototype.onError = function(status, file) {
      return console.log('base.onError()', status);
    };

    function S3Upload(options) {
      if (options === null) options = {};
      for (option in options) {
        this[option] = options[option];
      }
      this.handleFileSelect(document.getElementById(this.file_dom_selector));
      //this.onFinishS3Put();
    }

    S3Upload.prototype.handleFileSelect = function(file_element) {
      var f, files, output, _i, _len, _results;
      this.onProgress(0, 'Upload started.');
      files = file_element.files;
      output = [];
      _results = [];
      for (_i = 0, _len = files.length; _i < _len; _i++) {
        f = files[_i];
        _results.push(this.uploadFile(f));
      }
      return _results;
    };

    S3Upload.prototype.createCORSRequest = function(method, url) {
      var xhr;
      xhr = new XMLHttpRequest();
      if (xhr.withCredentials !== null) {
        xhr.open(method, url, true);
      } else if (typeof XDomainRequest !== "undefined") {
        xhr = new XDomainRequest();
        xhr.open(method, url);
      } else {
        xhr = null;
      }
      return xhr;
    };

    S3Upload.prototype.executeOnSignedUrl = function(file, callback, opts) {
      var this_s3upload, xhr, csrftoken;
      this_s3upload = this;
      csrftoken = this.csrftoken;
      xhr = new XMLHttpRequest();
      type = opts && opts.type || file.type || "application/octet-stream";
      console.log("type = " +  type);
      name = opts && opts.name || file.name;
      console.log("======== name = " + name);
      console.log("========== this.s3_sign_put_url = " + this.s3_sign_put_url);
      xhr.open('GET', this.s3_sign_put_url + '?s3_object_type=' + type + '&s3_object_name=' + encodeURIComponent(name), true);
      xhr.overrideMimeType('text/plain; charset=x-user-defined');
      //var csrftoken = document.getElementByName('csrfmiddlewaretoken')[0].value;
      xhr.setRequestHeader('X-CSRFToken', csrftoken);
      //xhr.setRequestHeader('HTTP_X_REQUESTED_WITH','XMLHttpRequest');
      xhr.onreadystatechange = function(e) {
        var result;
        if (this.readyState === 4 && this.status === 200) {
          try {
            result = JSON.parse(this.responseText);
          } catch (error) {
            this_s3upload.onError('Signing server returned some ugly/empty JSON: "' + this.responseText + '"');
            return false;
          }
          console.log("result.signed_request = " + result.signed_request + ", result.url = " + result.url);
          return callback(result.signed_request, result.url);
        } else if (this.readyState === 4 && this.status !== 200) {
          return this_s3upload.onError('Could not contact request signing server. Status = ' + this.status);
        }
      };
      return xhr.send();
    };

    S3Upload.prototype.uploadToS3 = function(file, url, public_url, opts) {
      var this_s3upload, xhr;
      this_s3upload = this;
      type = opts && opts.type || file.type || "application/octet-stream";
      console.log("[uploadToS3] type = " +  type);
      console.log("inside s3upload.js: public_url = " + public_url);
      
      xhr = this.createCORSRequest('PUT', url);
      
      //xhr = this.createCORSRequest('PUT', url);
      if (!xhr) {
        this.onError('CORS not supported');
      } else {
        xhr.onload = function() {
          if (xhr.status === 200) {
            this_s3upload.onProgress(100, 'Upload Completed.');
            return this_s3upload.onFinishS3Put(public_url);
          } else {
            return this_s3upload.onError('[Status ' + xhr.status + '; Statustext= ' + xhr.statustext + ']; Please click Reset button and try again');
          }
        };
        xhr.onerror = function() { //error caused by Access-Control-Allow-Origin
          return this_s3upload.onError('[Status ' + xhr.status +  '; Statustext = ' + xhr.statustext + ']; S3 Upload Error');
        };
        xhr.upload.onprogress = function(e) {
          var percentLoaded;
          if (e.lengthComputable) {
            percentLoaded = Math.round((e.loaded / e.total) * 100);
            return this_s3upload.onProgress(percentLoaded, percentLoaded === 100 ? 'Finalizing.' : 'Uploading.');
          }
        };
      }//end else
      xhr.setRequestHeader('Content-Type', type);
      xhr.setRequestHeader('x-amz-acl', 'public-read');
      return xhr.send(file);
    };

    S3Upload.prototype.uploadFile = function(file, opts) {
      var this_s3upload;
      this_s3upload = this;
      return this.executeOnSignedUrl(file, function(signedURL, publicURL) {
        return this_s3upload.uploadToS3(file, signedURL, publicURL, opts);
      }, opts);
    };

    return S3Upload;

  })();

}).call(this);
