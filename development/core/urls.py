# django core
from django.conf.urls import patterns, include, url

# 3rd party apps
from haystack.forms import ModelSearchForm

# mfb imports
from .formscoresearch import CoreAutoCompleteForm
from .viewscoresearch import autocompleteSearch

urlpatterns = patterns('',
	url(r'^$', 'core.viewscoresearch.autocompleteSearch', name='acsearch'),
)
