# stdlib python
import datetime

# django core
from django.utils.timezone import utc

# 3rd party plugins
from haystack import indexes

# author's code
from .models import UserProfile


class UserProfileIndex(indexes.SearchIndex, indexes.Indexable):
	'''haystack's searchindex object handles data flow into elasticsearch'''

	text 			= indexes.CharField(document=True, use_template=True)	
	user_auto 		= indexes.EdgeNgramField(model_attr="user")		# content for autocomplete field for autcomplete purposes.

	# clean data
	def index_queryset(self, using=None):
		'''Used when the entire index for model is updated.'''
		return self.get_model().objects.filter(created__lte=datetime.datetime.utcnow().replace(tzinfo=utc))
		#return self.get_model().objects.all()

	def get_model(self):
		return UserProfile

