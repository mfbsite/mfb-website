'''
Created on Nov 21, 2012

@author: rise
'''
# stdlib imports
import datetime
import random
import hashlib
import traceback
import sys
import logging
import pdb

# core django imports
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.utils.timezone import utc
from django.template import loader
from django.http import HttpResponse
from django.core.urlresolvers import reverse

# 3rd party imports

# project imports
from .formsregistration import RegistrationForm
from .models import UserProfile
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')

def reactivate(request):
	username = request.user.username
	template = loader.get_template("registration/reactivate_account.html")
	context = RequestContext(request,{'username':username})
	return HttpResponse(template.render(context))

def fail(request, username):
	template = loader.get_template("registration/fail_to_register.html")
	context = RequestContext(request,{'username':username})
	return HttpResponse(template.render(context))

def message(request, username):
	template = loader.get_template("registration/registration_complete.html")
	context = RequestContext(request,{'username':username})
	return HttpResponse(template.render(context))

def MFBRegistration(request):
	'''register a user as a MFB member'''
	if request.user.is_authenticated():
		# already registered
		return HttpResponseRedirect('dashboard')

	if request.method == 'POST':
		# submission of customized form
		form = RegistrationForm(request.POST)
		if form.is_valid():
			try:
				# all fields in form are filled out, unique username, correct email format, matching passwords
				new_user = User.objects.create_user(username=form.cleaned_data['username'], # pass value in username field in form
											email=form.cleaned_data['email'],
											password=form.cleaned_data['password'])
			except Exception as e:
				logger.info("Unable to create User: %s"%e)
				#traceback.print_exc(file=sys.stdout)
				return HttpResponseRedirect(reverse('failure',args=[form.cleaned_data['username']]))
			else:
				new_user.is_active = False
				new_user.save() # save form input into a user object for logging into account

			finally:
				# build the activation key for new user profile account; expires after 2 days
				salt = hashlib.sha1(str(random.random())).hexdigest()[:5] 
				activation_key = hashlib.sha1(salt+new_user.username).hexdigest() 
				key_expires = datetime.datetime.utcnow().replace(tzinfo=utc) + datetime.timedelta(2)

			try:
				# create manual UserProfile object
				logger.info("MFBRegistration: making new userprofile")
				new_userprofile = UserProfile(user=new_user, activation_key=activation_key, key_expires=key_expires, position=form.cleaned_data['position'])
			except Exception as e:
				logger.info("Unable to create User Profile: %s"%e)
				#traceback.print_exc(file=sys.stdout)
				# error: new user object created but not new user profile;
				# make new user inactive to avoid breaking foreign keys to users, then delete
				new_user.is_active = False
				new_user.delete()
				return HttpResponseRedirect(reverse('failure',args=[form.cleaned_data['username']]))
			else:
				print"in else"
				new_userprofile.save()

				email_subject = "REQUEST: Activate Your Molecular Flipbook Account"
				if get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.dev':
					email_body = "Almost there %s...\n\nPlease activate your account by clicking on this link within 48 hours:\nhttps://mfbdev.herokuapp.com/accounts/confirm/%s \n\nNote: This is an auto-generated e-mail message, please do not reply."%(
																							new_user.username,
																							new_userprofile.activation_key)
				elif get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.staging':
					email_body = "Almost there %s...\n\nPlease activate your account by clicking on this link within 48 hours:\nhttps://mfbstage.herokuapp.com/accounts/confirm/%s \n\nNote: This is an auto-generated e-mail message, please do not reply."%(
																							new_user.username,
																							new_userprofile.activation_key)
				elif get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.production':
					email_body = "Almost there %s...\n\nPlease activate your account by clicking on this link within 48 hours:\nhttps://www.molecularflipbook.org/accounts/confirm/%s \n\nNote: This is an auto-generated e-mail message, please do not reply."%(
																							new_user.username,
																							new_userprofile.activation_key)
				from_sender = 'molecularflipbook@gmail.com'
				to_recipient = [new_user.email]

				try:
					send_mail(email_subject, email_body, from_sender, to_recipient)
				except Exception as e:
					print e
					traceback.print_exc(file=sys.stdout)
				finally:
					# render html page stating email is sent out to user
					return HttpResponseRedirect(reverse('message', args=[new_user.username]))
		else:
			# form not valid, try again
			print("User form is bound:{0} errors:{1}").format(form.is_bound, form.errors)
			return render_to_response('registration/registration_form.html', { 'form': form }, context_instance=RequestContext(request))

	else:
		'''user is not submitting the form, show them a blank registration form'''
		form = RegistrationForm()
		# add form to context
		context = { 'form': form }
		return render_to_response('registration/registration_form.html', context, context_instance=RequestContext(request))

def confirm(request, activation_key):
	'''send activation link via email address of user'''

	# check is a user profile matches this activation key; if not, send a 'page not found'
	username = ""
	logger.info("========== CONFIRM get object activation_key = %s"%activation_key)
	try:
		#new_userprofile = get_object_or_404(UserProfile, activation_key=activation_key) # UserProfile
		new_userprofile = UserProfile.objects.get(activation_key=activation_key)
		username = new_userprofile.user
		logger.info("========== CONFIRM new_userprofile = %s and type(new_userprofile) = %s"%(new_userprofile,type(new_userprofile)))

		# check if user is already logged in;
		if new_userprofile.user.is_authenticated() and new_userprofile.user.is_active:
			logger.info("========== CONFIRM request.user is already set up = %s"%new_userprofile.user.key_expires)
			return HttpResponseRedirect(reverse('dashboard'))

		# check if key is expired
		if new_userprofile.key_expires > datetime.datetime.utcnow().replace(tzinfo=utc):
			logger.info("========== CONFIRM key is NOT expired = %s"%new_userprofile.key_expires)
			new_userprofile.user.is_active = True
			new_userprofile.activation_key = "ACTIVATED"
			new_userprofile.user.save() # save auth user
			new_userprofile.save()		# save UserProfile object
			return render_to_response('registration/activation_complete.html', {'username': new_userprofile.user,'expired': False, 'success': True}, context_instance=RequestContext(request))

		else:
			logger.info("========== CONFIRM key is expired = %s"%new_userprofile.key_expires)
			new_userprofile.user.is_active = False
			new_userprofile.user.save()	# save auth user
			return render_to_response('registration/activation_complete.html', {'username': new_userprofile.user,'expired': True, 'success': False}, context_instance=RequestContext(request))

	except Exception as e:
		logger.info("============ CONFIRM Exception - no activation key =============")
		return render_to_response('registration/no_activation_key.html', {'username': 'Hello'}, context_instance=RequestContext(request))
