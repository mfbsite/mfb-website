# stdlib imports
import re
import logging

# core django imports
from django.views.generic import TemplateView

# 3rd party imports

# project imports
from blog.models import Entry
from uploads.models import Pdb
from uploads.models import Tag
from uploads.models import Submission
from uploads.models import Revision
from uploads.models import Tutorial
from uploads.viewssubmission import getOembed, getNumberOfViews
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')


# main navigation pages
class Home(TemplateView):
	'''MFB homepage'''
	
	template_name = 'index.html'
	
	def get_context_data(self):
		
		# 0 - false; 1 - True
		feature = None
		revpk = None
		featrev = None
		subpk = None
		subUser = None
		subcreated = None
		subtitle = None
		subdescription = None
		featfiletype = None
		iframe = None
		tutiframe = None
		tutorial = None
		tuttitle = None
		tutvimeo = None
		tutpk = None
		tutcomments = None
		tutmodified = None
		submissions = None
		if Submission.objects.count():
			try:
				if Submission.objects.filter(vidfeature=True).count():
					feature = Submission.objects.get(vidfeature=True)
					revpk = Submission.objects.get(vidfeature=True).revisions.order_by('-id')[0].pk
					featrev = Revision.objects.get(pk=revpk)
					subpk = feature.pk
					subUser = feature.user;
					subcreated = feature.created
					subtitle = feature.title
					subdescription = feature.description
					featfiletype = feature.filetype
					if featrev.vimeoid:
						iframe = getOembed(featrev.vimeoid)
						if iframe:
							pattern = re.compile('width="[0-9]+" height="[0-9]+"')
							iframe = pattern.sub('width="380" height="214"', iframe)
							logger.info(iframe)

						else:
							iframe = None
			except Submission.DoesNotExist:
				logger.info("Submission object does not exist")

			submissions = Submission.objects.all().order_by('-id')

		if Tutorial.objects.filter(tutfeature=True).count():
			try:
				tutorial = Tutorial.objects.get(tutfeature=True)
				logger.info("tutorial = %s"%Tutorial.objects.get(tutfeature=True))
				tutpk = tutorial.pk
				tuttitle = tutorial.title
				tutvimeo = tutorial.vimeoid
				tutcomments = tutorial.comments
				tutmodified = tutorial.created
				if tutvimeo:
					logger.info("inside if tutvimeo = %s"%tutvimeo)
					tutiframe = getOembed(tutvimeo)
					logger.info("inside if tutiframe = %s"%tutiframe)
					if tutiframe:
						pattern = re.compile('width="[0-9]+" height="[0-9]+"')
						tutiframe = pattern.sub('width="380" height="214"', tutiframe)
						logger.info(tutiframe)

					else:
						tutiframe = None
			except Tutorial.DoesNotExist:
				logger.info("tutorial does not exist")

		return { 'title': 'Home', 'feature': feature, 'tutorial':tutorial,'tutpk':tutpk,'tutvimeo':tutvimeo,'tutmodified':tutmodified,'tutcomments':tutcomments,
				'revItem':featrev,'filetype': featfiletype, 'oembed': iframe, 'tutiframe':tutiframe,'tuttitle':tuttitle,'subtitle': subtitle, 
				'subdescription': subdescription,'submissions': submissions, 'revpk': revpk, 'subpk': subpk, 'subUser': subUser, 'subcreated': subcreated }
	
home = Home.as_view()


class About(TemplateView):
	'''About page'''
	
	template_name = 'about.html'
	
	def get_context_data(self):
		return { 'title': 'About' }
	
about = About.as_view()


class Downloads(TemplateView):
	'''MFB homepage'''
	
	template_name = 'downloads.html'
	
	def get_context_data(self):
		return { 'title': 'Downloads'}
	
downloads = Downloads.as_view()

	
class Library(TemplateView):
	'''Login page'''
	
	template_name = 'library.html'
	
	# 0 - False; 1 = True
	def get_context_data(self):
		staffpicks = None
		views = None
		if Submission.objects.count(): 
			try:
				if Submission.objects.all().filter(vidstaffpick=True).count():
					staffpicks = Submission.objects.all().filter(vidstaffpick=True).order_by('created')
			except Submission.DoesNotExist:
				staffpicks = None
			try:
				if Submission.objects.all().filter(views__gte=1):
					views = Submission.objects.all().filter(views__gte=1).order_by('-views','-created')
			except Submission.DoesNotExist:
				views = None
			submissions = Submission.objects.all().order_by('-id')
			for tag in Tag.objects.filter(count__gte=1).order_by('-count','-created'):
				if tag.submissions.all() is None:
					tag.count = 0
				else:
					tag.count = tag.submissions.all().count()
				tag.save(update_fields=['count'])
			tags = Tag.objects.filter(count__gte=1).order_by('-count','-created')
			#revisions = Submission.revisions.all().order_by('-id')
			return { 'title': 'Library', 'mostviews':False,'recent':True,'stafffaves':False,'browsetags':False,'staffpicks': staffpicks, 'views':views,'submissions': submissions, 'tagsubmissions':None,'tags': tags }
		else:
			return { 'title': 'Library', 'mostviews':False,'recent':True,'stafffaves':False,'browsetags':False,'staffpicks': None, 'views':None,'submissions': None, 'tagsubmissions':None,'tags': None }
	
library = Library.as_view()


class Tutorials(TemplateView):
	'''tutorials page'''

	template_name = 'tutorials.html'

	# 0 - False; 1 = True
	def get_context_data(self):
		if Tutorial.objects.all().count():
			tuts = Tutorial.objects.all().order_by('-id')
			return { 'tuts': tuts }
		else:
			return { 'tuts': None }

tutorial = Tutorials.as_view()


class News(TemplateView):
	'''tutorials page'''

	template_name = 'news.html'

	# 0 - False; 1 = True
	def get_context_data(self):
		if Entry.objects.all().count():
			entries = Entry.objects.all()
			return { 'entries': entries }
		else:
			return { 'entries': None }

news = News.as_view()


class Help(TemplateView):
	'''About page'''
	
	template_name = 'help.html'
	
	def get_context_data(self):
		return { 'title': 'Help' }
	
helpfaq = Help.as_view()
