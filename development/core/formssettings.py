'''
Created on April 30, 2014

@author: rise
'''
# stdlib imports
#import pdb
import os
import logging

# core django imports
from django import forms
from django.forms import ModelForm # ability to pass some model to the form
from django.contrib.auth.models import User

# 3rd party imports
import magic 

#  project's imports
from .models import UserProfile
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')

class UserForm(ModelForm):
	first_name 	= forms.CharField(label=(u'First Name: '), widget=forms.TextInput({"placeholder":"Enter your first name"}), required=False)
	last_name 	= forms.CharField(label=(u'Last Name: '), widget=forms.TextInput({"placeholder":"Enter your last name"}), required=False)
	email		= forms.EmailField(label=(u'Email: '), widget=forms.TextInput({"placeholder":"Enter your current email address"}), required=True)

	class Meta:
		model = User
		fields = ['first_name','last_name','email']

class UserProfileForm(ModelForm):
	
	class Meta:
		model = UserProfile
		fields = ['address1','address2','city', 'state', 'zipcode','website']