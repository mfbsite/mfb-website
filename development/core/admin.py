'''
Created on Oct 25, 2012

@author: rise
Display the uploads in the django admin interface.
'''
# stdlib imports

# core django imports
from django.contrib import admin

# 3rd party app imports

# project imports
from blog.models import Entry
from core.models import UserProfile
from core.models import Position
from uploads.models import Pdb 
from uploads.models import Tag
from uploads.models import Revision
from uploads.models import Submission
from uploads.models import FileType
from uploads.models import Tutorial
from uploads.models import SubmissionViewTracking


class SubmissionInline(admin.TabularInline):
	model       = Submission
	extra       = 3


class UserProfileAdmin(admin.ModelAdmin):
	fieldsets       = [
					('Activation/Registration', { 'fields': ['user', 'userpic', 'activation_key', 'key_expires']}), 
					  ('Mailing Address', { 'fields': ['address1', 'address2', 'city', 'state', 'zipcode'], 'classes': ['collapse']}), 
					  ('Website/Interests', {'fields': ['website'], 'classes': ['collapse']}),
					  ('Position', {'fields': ['position']}),                     ]
	inlines       = [SubmissionInline]
	list_display  = ('id', 'user', 'position', 'created')
	

class PdbAdmin(admin.ModelAdmin):
	fieldsets       = [
					('PDB', { 'fields': ['pdbname', 'count','submissions']}),
					]
	list_display    = ('id','pdbname', 'mostUsedPdb','displaySubmissions', )


class TagAdmin(admin.ModelAdmin):
	fieldsets       = [
					('Tag', { 'fields': ['tagname', 'count','submissions']}),
					]
	list_display    = ('id', 'tagname', 'mostUsedTag','displaySubmissions', )


class SubmissionViewTrackingAdmin(admin.ModelAdmin):
	fieldsets       = [
					('SubmissionViewTracker', { 'fields': ['count','submissions']}),
					]
	list_display    = ('id', 'count', 'created' )


class SubmissionAdmin(admin.ModelAdmin):
	fieldsets       = [
					('User/Authors', { 'fields': ['user', 'authors']}),
					('Featured Video/Staff Picks',{ 'fields': ['vidfeature','vidstaffpick']}),
					('Scene File/Animation Info', { 'fields': ['filetype', 'title', 'description', 'ancestors']}),
					]
	list_display    = ('id','title', 'user', 'filetype', 'vidfeature', 'vidstaffpick','views', 'displayPdbs', 'displayTags', )
	list_editable   = ('vidfeature','vidstaffpick',)


class RevisionAdmin(admin.ModelAdmin):
	fieldsets       = [ 
					('Submission/User', {'fields':['submission','user']}), 
					('Source File', {'fields':['sourcefile']}),
					('VimeoID', {'fields':['vimeoid']}),
					('Video File', {'fields':['vidanimation']}),
					('Thumbnail Source', {'fields':['vidpic']}),
					('Thumbnail Small',{'fields':['thumb_small']}),
					('Thumbnail Medium',{'fields':['thumb_medium']}),
					('Thumbnail Large',{'fields':['thumb_large']}),
					('Comments', {'fields':['comments']})
					]
	list_display    = ('id', 'vimeoid','submission', 'user', 'created', 'modified' )


class TutorialAdmin(admin.ModelAdmin):
	fieldsets       = [
					('Tutorial', { 'fields': ['vimeoid', 'title','iframe', 'tutfeature','tutpic', 'thumb_small','thumb_medium','thumb_large','tut_tags','comments']}),
					]
	list_display    = ('id', 'vimeoid', 'title','tutfeature','comments', )
	list_editable   = ('tutfeature',)



class EntryAdmin(admin.ModelAdmin):
	fieldsets       = [
					('Entry', {'fields': ['event_date','title','body',]}),
					]
	list_display    = ('id','event_date','title',)


admin.site.register(Entry, EntryAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Pdb,PdbAdmin)
admin.site.register(Tag,TagAdmin)
admin.site.register(Submission, SubmissionAdmin)
admin.site.register(Revision, RevisionAdmin)
admin.site.register(Position)
admin.site.register(FileType)
admin.site.register(Tutorial, TutorialAdmin)
admin.site.register(SubmissionViewTracking, SubmissionViewTrackingAdmin)
