'''
Created on April 25, 2014

@author: rise
'''
# stdlib imports
import datetime
import random
import hashlib
import logging
import pdb

# core django imports
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.template import Context
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.utils.timezone import utc
from django.template import loader
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.forms import PasswordChangeForm

# 3rd party imports

# project imports
from .formssettings import UserForm
from .formssettings import UserProfileForm
from .models import UserProfile
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')

def user_settings(request):
	context = { 'username': request.user}
	return render_to_response("registration/user_settings.html", context, context_instance=RequestContext(request))

def email_change_done(request):
	#recent_revision = Submission.objects.get(id=id).revisions.order_by(-id)[0]
	username = request.user.username
	template = loader.get_template("registration/settings_done.html")
	context = RequestContext(request,{'username':username})
	return HttpResponse(template.render(context))

def user_can_upload(user):
	return user.is_authenticated() and user.is_active

@csrf_protect
@user_passes_test(user_can_upload, login_url="/login/")
def email_change(request):
	if request.method == 'POST':
		form = UserForm(data=request.POST, instance=request.user)
		#pdb.set_trace()
		logger.info("settings = %s"%request.POST)
		if form.is_valid():
			try: 
				user = form.save(commit=False)
				user.save()
				logger.info("in settings firstname = %s, lastname = %s, and email: %s"%(user.first_name, user.last_name, user.email))

				email_subject = "Confirmation: Updated Molecular Flipbook Account"
				email_body = "Hey %s...\n\nJust sending a confirmation that you've updated your name and/or email address in your Molecular Flipbook account.\n\nIf this is incorrect, let us know: molecularflipbook@gmail.com\n\nNote: This is an auto-generated e-mail message, please do not reply."%(user.username)
				from_sender = 'molecularflipbook@gmail.com'
				to_recipient = [user.email]
				send_mail(email_subject, email_body, from_sender, to_recipient)

			except Exception as e:
				print e
				traceback.print_exc(file=sys.stdout)
			finally:
				# render html page stating email is sent out to user
				logger.info("in viewssettings: redirect to intermediate message")
				return HttpResponseRedirect(reverse('email_change_done'))

		else:
			logger.info("settings form failed")
			return render_to_response('registration/email_change_form.html', { 'form': form, 'username': request.user }, context_instance=RequestContext(request))

	else:
		form = UserForm(instance=request.user)
		context = { 'form': form , 'username': request.user, 'email': request.user.email }
		return render_to_response("registration/email_change_form.html", context, context_instance=RequestContext(request))

def address_change_done(request):
	#recent_revision = Submission.objects.get(id=id).revisions.order_by(-id)[0]
	username = request.user.username
	template = loader.get_template("registration/address_change_done.html")
	context = RequestContext(request,{'username':username})
	return HttpResponse(template.render(context))

@csrf_protect
@user_passes_test(user_can_upload, login_url="/login/")
def address_change(request):
	uprofile = UserProfile.objects.get(user=request.user)
	if request.method == 'POST':

		form = UserProfileForm(data=request.POST)
		logger.info("settings = %s"%request.POST)
		if form.is_valid():
			try: 
				profile = UserProfile.objects.filter(user=request.user).update(address1=form.cleaned_data['address1'],address2=form.cleaned_data['address2'],
												city=form.cleaned_data['city'],state=form.cleaned_data['state'],zipcode=form.cleaned_data['zipcode'],website=form.cleaned_data['website'])
				profile.save()
				logger.info("in settings address1 = %s, city = %s, and state: %s"%(profile.address1, profile.city, profile.state))

			except Exception as e:
				print e
				traceback.print_exc(file=sys.stdout)
			finally:
				# render html page stating email is sent out to user
				logger.info("in viewssettings: redirect to intermediate message")
				return HttpResponseRedirect(reverse('address_change_done'))

		else:
			logger.info("address change form failed")
			return render_to_response('registration/address_change_form.html', { 'form': form, 'username': request.user }, context_instance=RequestContext(request))

	else:
		form = UserProfileForm(instance=uprofile)
		context = { 'form': form  }
		return render_to_response("registration/address_change_form.html", context, context_instance=RequestContext(request))
