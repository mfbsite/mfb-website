# stdlib imports
import logging

# core django imports
from django import forms
from django.contrib.auth.models import User
from django.forms.forms import NON_FIELD_ERRORS
from django.contrib.auth import authenticate

# 3rd party imports

# project app imports
logger = logging.getLogger('infolog')

class LoginForm(forms.Form):
	'''login form for login page'''
	username		= forms.CharField(label=(u'Username'), widget=forms.TextInput({"placeholder": "Username", 'tabindex':'1'}))
	password		= forms.CharField(label=(u'Password'), widget=forms.PasswordInput(attrs={"placeholder": "Password", 'tabindex': '2'}, render_value=False))

	def clean(self):
		'''' method has access to remaining variables in the form'''
		#try:
		logger.info("cleaning username and password")
		cleaned_data = super(LoginForm, self).clean()
		username = cleaned_data.get('username')
		password = cleaned_data.get('password')
		if username and password:
			try:
				User.objects.get(username=username)
				user = authenticate(username=username, password=password)
				if user is None:
					logger.info("======= inside clean user is None ======= ")
					password_msg = "Wrong password; please try again."
					self.errors['password'] = self.error_class([password_msg])
					del cleaned_data['password']
				elif user.is_active == False:
					logger.info("======== inside user.is_active is false =======")
					logger.info("user account not activated")
					deactivated_msg = "Your account is not activated:"
					self.errors['__all__'] = self.error_class([deactivated_msg])
					del cleaned_data['username']
					del cleaned_data['password']

				return cleaned_data
			except User.DoesNotExist:
				logger.info("======== User does not exist ======== ")
				username_msg = "Username not in system; please try again."
				self.errors['username'] = self.error_class([username_msg])
				del cleaned_data['username']
