'''
Created on Nov 21, 2012

@author: rise
'''
# stdlib imports
import datetime
import random
import hashlib 

# core django imports
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.utils.timezone import utc
from django.template import loader
from django.http import HttpResponse
from django.core.urlresolvers import reverse

# 3rd party imports

# project imports

def mfb_app(request):
	#recent_revision = Submission.objects.get(id=id).revisions.order_by(-id)[0]
	template = loader.get_template("mfb_app.html")
	context = RequestContext(request)
	return HttpResponse(template.render(context))

def mfb_issues(request):
	#recent_revision = Submission.objects.get(id=id).revisions.order_by(-id)[0]
	username = request.user.username
	template = loader.get_template("mfb_issues.html")
	context = RequestContext(request)
	return HttpResponse(template.render(context))

def faqs(request):
	#recent_revision = Submission.objects.get(id=id).revisions.order_by(-id)[0]
	template = loader.get_template("faqs.html")
	context = RequestContext(request)
	return HttpResponse(template.render(context))

def fileuploads(request):
	#recent_revision = Submission.objects.get(id=id).revisions.order_by(-id)[0]
	username = request.user.username
	template = loader.get_template("file_uploads.html")
	context = RequestContext(request)
	return HttpResponse(template.render(context))

def tags(request):
	#recent_revision = Submission.objects.get(id=id).revisions.order_by(-id)[0]
	username = request.user.username
	template = loader.get_template("tags.html")
	context = RequestContext(request)
	return HttpResponse(template.render(context))

def guidelines(request):
	#recent_revision = Submission.objects.get(id=id).revisions.order_by(-id)[0]
	username = request.user.username
	template = loader.get_template("guidelines.html")
	context = RequestContext(request)
	return HttpResponse(template.render(context))