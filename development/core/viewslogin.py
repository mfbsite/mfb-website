'''
Created on Nov 21, 2012

@author: rise
'''
# stdlib
import pdb
import re
import logging
from urllib import urlencode
import urlparse

# core django imports
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render_to_response
from django.forms.forms import NON_FIELD_ERRORS
from django.core.urlresolvers import reverse
from django.shortcuts import redirect

# 3rd party imports

# project imports
from .formslogin import LoginForm
from uploads.models import Submission
from uploads.models import Revision
from uploads.viewssubmission import getOembed, getNumberOfViews
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')

# *********************************************
# helper function
# *********************************************
def is_empty(any_structure):
    if any_structure:
        print('Structure is not empty.')
        return False
    else:
        print('Structure is empty.')
        return True

# *********************************************
# based on code by: 
# http://hackedexistence.com/project/django/video7-userauthentication-2.html
# *********************************************
def loginView(request):
	'''check if currently logged-in user is authenticated; check if user is a MFB member and active'''
	#pdb.set_trace()
	mfb_query = {}
	if request.method == 'GET' and request.user.is_authenticated() == True:
		logger.info('request = %s'%request)
		logger.info("request.REQUEST.get('next') = %s"%request.REQUEST.get('next'))
		logger.info("request.GET = %s"%request.GET)
		#pdb.set_trace()
		if ( request.GET.get('next') == "my_bookshelf/upload" ):
			#params = request.GET.dict()
			#new_params = {key:value for key,value in params.items() if key != 'next'}
			#logger.info("new_params = %s"%new_params)
			#response = redirect(request.GET.get('next'))
			#logger.info('response["Location"] = %s'%response["Location"])
			#response['Location'] += '?' + urlencode(new_params)
			#logger.info('response["Location"] = %s'%response["Location"])
			#logger.info('response = %s'%response)
			logger.info("logged in already: going to upload.....")
			return HttpResponseRedirect(reverse('mfb_upload'))
		else:
			return HttpResponseRedirect(reverse('dashboard'))

	form = LoginForm()
	# 0 - false; 1 - True
	feature = None
	revpk = None
	featrev = None
	subpk = None
	subUser = None
	subcreated = None
	subtitle = None
	subdescription = None
	featfiletype = None
	iframe = None
	if Submission.objects.count():
		try:
			#if Submission.objects.filter(vidfeature=True).count():
			feature = Submission.objects.get(vidfeature=True)
			revpk = Submission.objects.get(vidfeature=True).revisions.order_by('-id')[0].pk
			featrev = Revision.objects.get(pk=revpk)
			subpk = feature.pk
			subUser = feature.user
			subcreated = feature.created
			subtitle = feature.title
			subdescription = feature.description
			featfiletype = feature.filetype
			if featrev.vimeoid:
				iframe = getOembed(featrev.vimeoid)
				if iframe:
					pattern = re.compile('width="[0-9]+" height="[0-9]+"')
					iframe = pattern.sub('width="380px" height="214px"', iframe)
					logger.info(iframe)
					#pattern = re.compile('src="//player.vimeo.com/video/[0-9]+"')
					#m = re.search(r'src="//player.vimeo.com/video/[0-9]+',iframe)
					#if m:
					#	logger.info('HOME: p = %s'%m.group(0))
					#	newsrc = str(m.group(0))
					#	logger.info(newsrc)
					#	iframe = pattern.sub(newsrc, iframe)
				else:
					iframe = None
		except Submission.DoesNotExist:
			feature = None
			revpk = None
			featrev = None
			subpk = None
			subUser = None
			subcreated = None
			subtitle = None
			subdescription = None
			featfiletype = None
			iframe = None
		submissions = Submission.objects.all().order_by('-id')

		if iframe:
			context = {  'form': form, 'title': 'Home', 'feature': feature, 'revItem':featrev,'filetype': featfiletype, 'oembed': iframe,
					'subtitle': subtitle, 'subdescription': subdescription,'submissions': submissions,
					'revpk': revpk, 'subpk': subpk, 'subUser': subUser, 'subuserpk':subpk,'subcreated': subcreated }
		else:
			context = { 'form': form, 'title': 'Home', 'feature': feature, 'revItem':featrev,'filetype': featfiletype, 'oembed': None,
					'subtitle': subtitle, 'subdescription': subdescription,'submissions': submissions,
					'revpk': revpk, 'subpk': subpk, 'subUser': subUser, 'subuserpk':subpk,'subcreated': subcreated }
	else:
		context = { 'form': form, 'title': 'Home', 'feature': None, 'oembed':None,'submissions': None, 'revpk': None, 'subpk': None, 'subUser': None }

	if request.method == 'POST' and (request.user.is_authenticated()) == False:
		form = LoginForm(request.POST)
		if Submission.objects.count():
			if iframe:
				context = {  'form': form, 'title': 'Home', 'feature': feature, 'revItem':featrev,'filetype': featfiletype, 'oembed': iframe,
						'subtitle': subtitle, 'subdescription': subdescription,'submissions': submissions,
						'revpk': revpk, 'subpk': subpk, 'subUser': subUser, 'subuserpk':subpk,'subcreated': subcreated }
			else:
				context = { 'form': form, 'title': 'Home', 'feature': feature, 'revItem':featrev,'filetype': featfiletype, 'oembed': None,
						'subtitle': subtitle, 'subdescription': subdescription,'submissions': submissions,
						'revpk': revpk, 'subpk': subpk, 'subUser': subUser, 'subuserpk':subpk,'subcreated': subcreated }
		else:
			context = { 'form': form, 'title': 'Home', 'feature': None, 'oembed':None,'submissions': None, 'revpk': None, 'subpk': None, 'subUser': None }
		if form.is_valid():
			logger.info("======= inside form.is_valid for viewslogin ======= ")
			# pull out info from form and assign them to variables
			user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
			if user.is_active:
				login(request, user)
				#pdb.set_trace()
				logger.info("============== VIEWSLOGIN request.GET = %s"%request.GET)
				logger.info('REQUEST = %s'%request)
				#logger.info("request.get_full_path() = %s"%request.get_full_path())
				if ( request.GET.get('next') == "my_bookshelf/upload" ):
					logger.info("request.GET.get('next') = %s"%request.GET.get('next'))
					#params = request.GET.dict()
					#new_params = {key:value for key,value in params.items() if key != 'next'}
					#logger.info("new_params = %s"%new_params)
					#response = redirect(request.GET.get('next'))
					#logger.info('response["Location"] = %s'%response["Location"])
					#response = redirect(response["Location"])
					#response['Location'] += '?' + urlencode(new_params)
					#logger.info('response["Location"] = %s'%response["Location"])
					#logger.info('response = %s'%response)
					logger.info("going to upload.....")
					#return response
					return HttpResponseRedirect(reverse('mfb_upload'))
				else:
					return HttpResponseRedirect(reverse('dashboard'))
			else:
				return HttpResponseRedirect(reverse('reactivate'))
		else:
			logger.info("======= inside else form is not valid for viewslogin ======= ")
			# if form is not valid, go to login page
			return render_to_response('registration/login.html', context, context_instance=RequestContext(request))
	else: 
		# user is not submitting the form, show the login form
		#form = LoginForm()

		return render_to_response('registration/login.html', context, context_instance=RequestContext(request))
	
def logoutView(request):
	''' display message stating user has been successfully logged out of their account '''
	logout(request)
	return render_to_response("registration/logged_out.html", context_instance=RequestContext(request))
