'''
Created on April 25, 2014

@author: rise
'''
# stdlib imports
import datetime
import random
import hashlib
import logging

# core django imports
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.template import Context
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.utils.timezone import utc
from django.template import loader
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_protect

# 3rd party imports

# project imports
from .formsresendactivationemail import ResendActivationEmailForm
from .models import UserProfile
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')

@csrf_protect
def resend_activation_email(request):
	if request.method == 'POST':
		form = ResendActivationEmailForm(request.POST)
		logger.info("resend_activation_email = %s"%request.POST)
		context = { 'form': form }
		if form.is_valid():
			email = form.cleaned_data["email"]
			logger.info("in resend_activation_email email: %s"%email)
			# check if email is in system and if user has not been activated yet
			users = User.objects.filter(email=email, is_active=0)
			logger.info("in resend_activation_email view: users = %s and %s"%(users.count(),users))

			if not users.count():
				email_msg = "Your email address is not registered in our system; Please try again."
				form.errors["email"] = form.error_class([email_msg])
				return render_to_response('registration/reactivate_account.html', context, context_instance=RequestContext(request))
			else:
				for user in users:
					for profile in UserProfile.objects.filter(user=user):
						logger.info("for each profile, make hash if key is expired: profile = %s"%profile)
						if profile.key_expires < datetime.datetime.utcnow().replace(tzinfo=utc):
							logger.info("MAKING THE HASH")
							salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
							profile.activation_key = hashlib.sha1(salt+user.email).hexdigest()
							profile.key_expires = datetime.datetime.utcnow().replace(tzinfo=utc) + datetime.timedelta(2)
							user.date_joined = datetime.datetime.now()
							user.save()
							profile.save()

							email_subject = "REQUEST: Reactivate Your Molecular Flipbook Account"
							if get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.dev':
								email_body = "Almost there %s...\n\nPlease reactivate your account by clicking on this link within 48 hours:\nhttps://mfbdev.herokuapp.com/accounts/confirm/%s \n\nNote: This is an auto-generated e-mail message, please do not reply."%(
																										user.username,
																										profile.activation_key)
							elif get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.staging':
								email_body = "Almost there %s...\n\nPlease reactivate your account by clicking on this link within 48 hours:\nhttps://mfbstage.herokuapp.com/accounts/confirm/%s \n\nNote: This is an auto-generated e-mail message, please do not reply."%(
																										user.username,
																										profile.activation_key)
							elif get_env_variable('SETTINGS_ENVIRONMENT') == 'settings.production':
								email_body = "Almost there %s...\n\nPlease reactivate your account by clicking on this link within 48 hours:\nhttps://wwww.molecularflipbook.org/accounts/confirm/%s \n\nNote: This is an auto-generated e-mail message, please do not reply."%(
																										user.username,
																										profile.activation_key)
							from_sender = 'molecularflipbook@gmail.com'
							to_recipient = [user.email]

							try:
								send_mail(email_subject, email_body, from_sender, to_recipient)
							except Exception as e:
								print e
								traceback.print_exc(file=sys.stdout)
							finally:
								# render html page stating email is sent out to user
								logger.info("resend_activation_email in view: redirect to intermediate message")
								return HttpResponseRedirect(reverse('message', args=[user.username]))
						else:
							logger.info("key is not expired; case scenario: user deleted email w/activation link; resend new activation link...")
							email_msg = "."
							form.errors["email"] = form.error_class([email_msg])

		else:
			return render_to_response('registration/reactivate_account.html', context, context_instance=RequestContext(request))

	else:
		form = ResendActivationEmailForm()
		context = { 'form': form }
		return render_to_response("registration/reactivate_account.html", context, context_instance=RequestContext(request))
