# stdlib imports
import logging

# core django imports
from django import forms
from django.contrib.auth.models import User
from django.forms.forms import NON_FIELD_ERRORS
from django.contrib.auth import authenticate

# 3rd party imports

# project app imports
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')

class ResendActivationEmailForm(forms.Form):
	email		= forms.EmailField(label=(u'Email Address'), widget=forms.TextInput({"placeholder": "Email Address","id":"email"})) 

	class Meta:
		model = User
		fields = ['email']

	def clean_email(self):
		'''' method has access to remaining variables in the form'''
		logger.info("ResendActivationEmailForm: cleaning email")
		cleaned_data = super(ResendActivationEmailForm, self).clean()
		email = cleaned_data.get('email')
		if email:
			try:
				User.objects.filter(email=email)
			except User.DoesNotExist:
				email_msg = "Your email address is not in our system. Please try another."
				self.errors['email'] = self.error_class([email_msg])
				del cleaned_data['email']
			finally:
				logger.info("ResendActivationEmailForm: return cleaned_data")
				return cleaned_data['email']
