import datetime

from django.test import TestCase
#from django.test.client import Client
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.template import Template, Context
from django.utils.timezone import utc
from datetime import date
from django_webtest import WebTest
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

# Create your tests here.
from .models import Position
from .models import UserProfile
from .formssettings import UserForm
from .formssettings import UserProfileForm
from uploads.models import Submission
from uploads.formssubmission import SubmissionForm


class MfbProjectTest(TestCase):

	def setUp(self):
		self.user = get_user_model().objects.create(username="Mayari")

	def test_about(self):
		response = self.client.get('/about/')
		self.assertEqual(response.status_code, 200)

	def test_download(self):
		response = self.client.get('/downloads/')
		self.assertEqual(response.status_code, 200)

	def test_library(self):
		response = self.client.get('/library/')
		self.assertEqual(response.status_code, 200)

	def test_tutorial(self):
		response = self.client.get('/tutorial/')
		self.assertEqual(response.status_code, 200)

	def test_help(self):
		response = self.client.get('/help/')
		self.assertEqual(response.status_code, 200)


class HomePageTest(TestCase):

	def setUp(self):
		self.user = get_user_model().objects.create(username="Mayari")

	def test_homepage(self):
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)


class PositionModelTest(TestCase):

	def test_unicode(self):
		job = Position(position="Other")
		self.assertEqual(unicode(job), job.position)

	#def test_get_user_profiles(self):
	#	job = Position.objects.create(position="Other")
	#	profilequery = Position.objects.filter(position=job.position)
	#	self.assertEqual(job.get_user_profiles().positon, profilequery)


class LoginViewTest(TestCase):
	def setUp(self):
		self.user = get_user_model().objects.create(username="Mayari")

	def test_loginview(self):
		response = self.client.get('/login/')
		self.assertEqual(response.status_code, 200)

	#def test_login_with_feature_animation(self):
	#	self.submission = Submission.objects.create()
	#	response = self.client.get('/login/')
