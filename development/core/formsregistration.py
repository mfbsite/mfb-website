# stdlib imports
import logging

# core django imports
from django.contrib.auth.models import User
from django import forms
from django.forms import ModelForm # ability to pass some model to the form

# 3rd party imports

# project imports
from .models import UserProfile
from .models import Position
from settings.base import get_env_variable

logger = logging.getLogger('infolog')

POSITIONS = [
				('10', 'Full Professor'),
				('20', 'Associate Professor'),
				('30', 'Assistant Professor'),
				('40', 'Post-doctoral Student'),
				('50', 'Graduate Student'),
				('60', 'Undergrad'),
				('70', 'Lab Technician'),
				('80', 'Highschool Student'), 
				('90', 'Animator'),
				('100','Other')
				]

#****************************************************
# Create custom form for creating new user profiles
# make password input invisible
#*****************************************************
class RegistrationForm(ModelForm):
	username	= forms.CharField(label=(u'Username'), widget=forms.TextInput({"placeholder": "Username"}))
	email		= forms.EmailField(label=(u'Email Address'), widget=forms.TextInput({"placeholder": "Email Address"})) 
	position	= forms.ChoiceField(label=(u'Position'), widget=forms.Select(), choices=POSITIONS)
	password	= forms.CharField(label=(u'Password'), widget=forms.PasswordInput(attrs={"placeholder": "Password"}, render_value=False) )
	cpassword	= forms.CharField(label=(u'Confirm Password'), widget=forms.PasswordInput(attrs={"placeholder":"Confirm Password"}, render_value=False))
	
	class Meta:
		''' automatically takes the model, User, and creates a form out of the fields of the model'''
		#model = UserProfile
		model = User
		# exclude the user field since we already dealt with user field
		#exclude = ('user','activation_key','key_expires','first_name','last_name','address1','address2',
		#		'city','zipcode','state','industry','occupation','url')
		fields = ['username','email','password']
		
	def clean_username(self):
		''' used with form.is_valid() and will take the field value of username of the model, RegistrationForm'''
		username = self.cleaned_data['username']
		try:
			User.objects.get(username=username) # find any Users with the same username
		except User.DoesNotExist:
			logger.info("Creating new User")
			# if new user does not exist in Users account, then ok name to use
			return username
		raise forms.ValidationError("That username is already taken, please use another one.")
	
	def clean_email(self):
		'''used with with form.is_valid() and will take the field value of email of the model, RegistrationForm'''
		email = self.cleaned_data['email']
		try:
			User.objects.get(email=email)
		except User.DoesNotExist:
			logger.info("Valid email")
			return email
		raise forms.ValidationError("The email address has already been used, please select another.")

	def clean_position(self):
		position_pk = self.cleaned_data['position']
		print "position=%s"%position_pk
		try:
			return Position.objects.get(pk=position_pk)
		except Position.DoesNotExist:
			raise forms.ValidationError("Please select a position.")

	def clean_cpassword(self):
		'''' method has access to remaining variables in the form'''
		print("clean method")
		cleaned_data = super(RegistrationForm, self).clean()
		password = self.cleaned_data.get('password', None)
		cpassword = self.cleaned_data.get('cpassword', None)
		if password and cpassword and (password == cpassword):
			return self.cleaned_data['cpassword']
		else:
			logger.info("mismatched passwords")
			raise forms.ValidationError("The passwords didn't match. Please try again.")
		