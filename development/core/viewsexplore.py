'''
Created on Nov 21, 2012

@author: rise
'''
# stdlib imports
import logging

# core django imports
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.template import loader

# 3rd party imports

# project imports
from uploads.models import Submission
from uploads.models import Tag
from .models import UserProfile
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')

def browseViewed(request):
	'''query all submissions and return sorted list by most viewed sourcefiles'''
	try:
		if Submission.objects.all():
			submissions = Submission.objects.all().order_by('-id')
			staffpicks = Submission.objects.all().filter(vidstaffpick=True).order_by('created')
			views = Submission.objects.all().filter(views__gte=1).order_by('-views','-created')
			tags = Tag.objects.filter(count__gte=1).order_by('-count','-created')
			context={ 'title': 'Library', 'mostviews':True, 'recent': False, 'stafffaves':False,'browsetags':False, 'views':views, 'staffpicks': staffpicks, 'submissions': submissions, 'tagsubmissions':None,'tags': tags }
			return render_to_response('library.html', context, context_instance=RequestContext(request))
		else:
			context={ 'title': 'Library', 'mostviews':True, 'recent': False, 'stafffaves':False, 'browsetags':False, 'views':None, 'staffpicks': None, 'submissions': None, 'tagsubmissions':None,'tags': None }
			return render_to_response('library.html', context, context_instance=RequestContext(request))

	except Exception as e:
		print e
		import traceback, sys
		traceback.print_exc(file=sys.stdout)


def browseRecent(request):	
	'''query all submissions and return sorted list by most recent date'''
	try:
		if Submission.objects.all():
			submissions = Submission.objects.all().order_by('-id')
			staffpicks = Submission.objects.all().filter(vidstaffpick=True).order_by('created')
			views = Submission.objects.all().filter(views__gte=1).order_by('-views','-created')
			tags = Tag.objects.filter(count__gte=1).order_by('-count','-created')
			context={ 'title': 'Library', 'mostviews':False, 'recent': True, 'stafffaves':False,'browsetags':False, 'views':views, 'staffpicks': staffpicks, 'submissions': submissions, 'tagsubmissions':None,'tags': tags }
			return render_to_response('library.html', context, context_instance=RequestContext(request))
		else:
			context={ 'title': 'Library', 'mostviews':False, 'recent': True, 'stafffaves':False, 'browsetags':False, 'views':None, 'staffpicks': None, 'submissions': None, 'tagsubmissions':None,'tags': None }
			return render_to_response('library.html', context, context_instance=RequestContext(request))

	except Exception as e:
		print e
		import traceback, sys
		traceback.print_exc(file=sys.stdout)


def browseStaffPicks(request):
	'''query all submissions and return sorted list by staff favorites'''
	try:
		if Submission.objects.all():
			submissions = Submission.objects.all().order_by('-id')
			staffpicks = Submission.objects.all().filter(vidstaffpick=True).order_by('-created')
			views = Submission.objects.all().filter(views__gte=1).order_by('-views','-created')
			tags = Tag.objects.filter(count__gte=1).order_by('-count','-created')
			context={ 'title': 'Library', 'mostviews':False, 'recent':False,'stafffaves':True,'usersubs':False,'browsetags':False, 'views':views, 'staffpicks': staffpicks, 
				'usersubmissions': None,'submissions': submissions, 'tagsubmissions':None,'tags': tags }
			return render_to_response('library.html', context, context_instance=RequestContext(request))
		else:
			context={ 'title': 'Library', 'mostviews':False, 'recent':False,'stafffaves':True,'usersubs':False,'browsetags':False, 'views':None, 'staffpicks': None, 
				'usersubmissions':None, 'submissions': None, 'tagsubmissions':None,'tags': None }
			return render_to_response('library.html', context, context_instance=RequestContext(request))

	except Exception as e:
		print e
		import traceback, sys
		traceback.print_exc(file=sys.stdout)

def browseUserSubmissions(request, pk):
	'''query all submissions and return sorted list by user'''

	template = loader.get_template("library.html")
	if Submission.objects.all():
		submissions = Submission.objects.all().order_by('-id')
		staffpicks = Submission.objects.all().filter(vidstaffpick=True).order_by('-created')
		views = Submission.objects.all().filter(views__gte=1).order_by('-views','-created')
		up = UserProfile.objects.get(pk=pk)
		up.user
		logger.info('up = %s and up.user = %s'%(up,up.user))
		usersubmissions = Submission.objects.all().filter(user=up)
		tags = Tag.objects.filter(count__gte=1).order_by('-count','-created')
		context=RequestContext(request,{ 'title': 'Library', 'mostviews':False, 'recent':False,'stafffaves':False,'usersubs':True,'browsetags':False, 'views':views, 'staffpicks': staffpicks,
				'usersubmissions':usersubmissions,'submissions': submissions, 'tagsubmissions':None,'tags': tags })

		return HttpResponse(template.render(context))
	else:
		context={ 'title': 'Library', 'mostviews':False, 'recent':False,'stafffaves':False,'usersubs':True,'browsetags':False, 'views':None, 'staffpicks': None,
				'usersubmissions':None,'submissions': None, 'tagsubmissions':None,'tags': None }
		return HttpResponse(template.render(context))

def browseSubmissionsOfTag(request, pk):
	'''query all submissions and return sorted list by tag name'''
	template = loader.get_template("library.html")
	if Submission.objects.all():
		submissions = Submission.objects.all().order_by('-id')
		staffpicks = Submission.objects.all().filter(vidstaffpick=True).order_by('-created')
		tags = Tag.objects.filter(count__gte=1).order_by('-count','-created')
		#selectedtag = Tag.objects.get(pk=pk)
		tagsubmissions = Tag.objects.get(pk=pk).submissions.order_by('-created')
		logger.info(tagsubmissions)
		context=RequestContext(request,{ 'title': 'Library', 'recent':False,'stafffaves':False,'browsetags':True,'staffpicks': staffpicks,
			'submissions': submissions, 'tagsubmissions':tagsubmissions,'tags': tags })
		return HttpResponse(template.render(context))
	else:
		context={ 'title': 'Library', 'recent':False,'stafffaves':False,'browsetags':True,'staffpicks': None, 'submissions': None, 'tagsubmissions':None,'tags': None }
		return HttpResponse(template.render(context))
