# django core
from django import forms

# 3rd party apps
from haystack.forms import SearchForm
from haystack.inputs import AutoQuery
from haystack.query import EmptySearchQuerySet, SearchQuerySet

# mfb apps
from uploads.models import Submission


class CoreAutoCompleteForm(SearchForm):
	"""slightly customized search form that allows filtering on SearchQuerySet"""

	def search(self):
		print"inside search"
		if not self.is_valid():
			print"inside first-if-not"
			return self.no_query_found()

		query = self.cleaned_data.get('q','').lower()
		print "query %s" %query
		sqs = self.searchqueryset
		print "sqs %s" %sqs

		if not query:
			print"inside 2nd if-not"
			return self.no_query_found()

		if query:
			print"inside CoreAutoCompleteForm - 3rd if"
			# or - if all lists are empty, do a general search; otherwise, do autocompletion 
			if not (sqs.using('autocomplete').autocomplete(user_auto=query) or sqs.using('autocomplete').autocomplete(pdbname_auto=query) or 
				sqs.using('autocomplete').autocomplete(tagname_auto=query)):
			#if not sqs:
				print"inside if - default"
				sqs = sqs.using('default').filter(content=AutoQuery(query)).models(Submission).order_by('modified','user_auto', 'pdbname_auto','tagname_auto').highlight()	# highlight the query in SearchResult objects

				#sqs = sqs.using('default').filter(content=AutoQuery(query)).order_by('modified','user_auto', 'pdbname_auto','tagname_auto', 'title', 'description').highlight()
				#sqs = sqs.using('default').filter(content=AutoQuery(query)).highlight()
				print sqs
			else:
				# user must provide exact word for any search result
				print"inside if - autocomplete"
				sqs1 = EmptySearchQuerySet()
				sqs2 = EmptySearchQuerySet()
				sqs3 = EmptySearchQuerySet()

				if sqs.using('autocomplete').autocomplete(user_auto__exact=query):
					sqs1 = sqs.using('autocomplete').autocomplete(user_auto__exact=query)
					print 'inside user-auto - sqs1: %s' %sqs1

				if sqs.using('autocomplete').autocomplete(pdbname_auto__exact=query):
					sqs2 = sqs.using('autocomplete').autocomplete(pdbname_auto__exact=query)
					print 'inside pdbname-auto - sqs2: %s' %sqs2

				if sqs.using('autocomplete').autocomplete(tagname_auto__exact=query):
					sqs3 = sqs.using('autocomplete').autocomplete(tagname_auto__exact=query)
					print 'inside tagname-auto - sqs3: %s' %sqs3

				temp = EmptySearchQuerySet()
				for sobj in [sqs1,sqs2,sqs3]:
					print "sobj: %s" %sobj
					if sobj:
						temp = sobj | temp
						print "temp %s" %temp
				sqs = temp
				

		if self.load_all:
			print"inside last if"
			sqs = sqs.load_all()

		print "in formscoresearch:  %s" %sqs
		return sqs


class CoreHaystackForm(SearchForm):
	"""selected term from search widget apply auto query"""

	def search(self):
		if not self.is_valid():
			return self.no_query_found()

		query = self.cleaned_data.get('q','')
		sqs = self.searchqueryset

		if not query:
			return self.no_query_found()

		if query:
			print"inside CoreHaystackForm - inside 3rd if"

			sqs = sqs.using('default').filter(content=AutoQuery(query)).models(Submission).order_by('modified').highlight()	# highlight the query in SearchResult objects
			#sqs = sqs.using('default').filter(content=AutoQuery(query)).order_by('-modified','user_auto', 'pdbname_auto','tagname_auto', 'title', 'description').highlight()	# highlight the query in SearchResult objects
			#sqs = sqs.using('default').filter(content=AutoQuery(query)).highlight()	# highlight the query in SearchResult objects

		if self.load_all:
			print"inside last if"
			sqs = sqs.load_all()

		print "uploadshaystackform: sqs %s" %sqs
		return sqs
