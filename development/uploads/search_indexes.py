# stdlib python
import datetime

# django core
from django.utils import timezone
from django.utils.timezone import utc

# 3rd party plugins
from haystack import indexes

# mfb apps
from .models import FileType
from .models import Pdb
from .models import Tag
from .models import Submission
from .models import Revision

"""
class FileTypeIndex(indexes.SearchIndex, indexes.Indexable):
	'''haystack's searchindex object handles data flow into elasticsearch'''

	text 			= indexes.CharField(document=True, use_template=True)
	filetype		= indexes.CharField(model_attr='filetype')

	def index_queryset(self, using=None):
		'''Used when the entire index for model is updated.'''
		#return self.get_model().objects.filter(created__lte=datetime.datetime.utcnow().replace(tzinfo=utc))
		return self.get_model().objects.all()

	def get_model(self):
		return FileType
"""

class PdbIndex(indexes.SearchIndex, indexes.Indexable):
	'''index pdbs '''
	text 				= indexes.CharField(document=True, use_template=True)
	pdbname_auto	 	= indexes.EdgeNgramField(model_attr="pdbname", null=True, boost=1.125) # or indexes.EdgeNgramField()???
	submissions 		= indexes.MultiValueField()

	# error TypeError: 'ManyRelatedManager' object is not iterable - don't add model_attr to MultiValueField()!!!
	# ERROR:root:Error updating notes using default  
	def prepare_pdbs(self,obj):
		return [submissions.title for submissions in obj.submissions.all()]
		
	def index_queryset(self, using=None):
		"""Used when the entire index for model is updated."""
		return self.get_model().objects.filter(created__lte=datetime.datetime.utcnow().replace(tzinfo=utc))

	def get_model(self):
		return Pdb


class TagIndex(indexes.SearchIndex, indexes.Indexable):
	'''index tags '''
	text 				= indexes.CharField(document=True, use_template=True)
	tagname_auto	 	= indexes.EdgeNgramField(model_attr="tagname", null=True, boost=1.125) # or indexes.EdgeNgramField()???
	submissions 		= indexes.MultiValueField()

	# error TypeError: 'ManyRelatedManager' object is not iterable - don't add model_attr to MultiValueField()!!!
	# ERROR:root:Error updating notes using default  
	def prepare_tags(self,obj):
		return [submission.title for submission in obj.submissions.all()]
		
	def index_queryset(self, using=None):
		"""Used when the entire index for model is updated."""
		return self.get_model().objects.filter(created__lte=datetime.datetime.utcnow().replace(tzinfo=utc))

	def get_model(self):
		return Tag


class SubmissionIndex(indexes.SearchIndex, indexes.Indexable):
	'''haystacks searchindex object handles data flow into elasticsearch'''

	text 				= indexes.CharField(document=True, use_template=True)
	user 				= indexes.CharField(model_attr='user')				# a submission belongs to which userprofile
	authors				= indexes.CharField(model_attr='authors')
	title				= indexes.CharField(model_attr='title')
	description			= indexes.CharField(model_attr='description')
	modified			= indexes.DateTimeField(model_attr='modified')

	# clean data
	def prepare_author(self, obj):
		return obj.user.name or 'SubmissionIndex: User not available'

	def prepare_authors(self,obj):
		return obj.authors or "SubmissionIndex: Authors not available"

	def prepare_title(self, obj):
		return obj.title or 'SubmissionIndex: Title not available'

	def prepare_modified(self, obj):
		return obj.modified or  "SubmissionIndex: Modified not available"

	def prepare_description(self, obj):
		return obj.description or "SubmissionIndex: Description not available"

	def index_queryset(self, using=None):
		"""Used when the entire index for model is updated."""
		return self.get_model().objects.filter(created__lte=datetime.datetime.utcnow().replace(tzinfo=utc))
		#return self.get_model().objects.all()

	def get_model(self):
		return Submission


class RevisionIndex(indexes.SearchIndex, indexes.Indexable):
	'''haystacks searchindex object handles data flow into elasticsearch'''

	# use EdgeNgramField for autocompletion
	text				= indexes.CharField(document=True, use_template=True)
	user 				= indexes.CharField(model_attr='user')
	sourcefile			= indexes.CharField(model_attr='sourcefile')
	#vidanimation 		= indexes.CharField(model_attr='vidanimation')
	vidpic				= indexes.CharField(model_attr='vidpic')
	#comments			= indexes.CharField(model_attr='comments')

	# clean data
	def prepare_author(self,obj):
		return obj.user.name or 'RevisionIndex: user not available'

	def prepare_sourcefile(self, obj):
		return obj.sourcefile or 'RevisionIndex: sourcefile not available'

	#def prepare_vidanimation(self, obj):
	#	return obj.vidanimation.url or 'RevisionIndex: vidanimation not available'

	def prepare_vidpic(self, obj):
		return obj.vidpic or 'RevisionIndex: vidpic not available'

	#def prepare_comments(self, obj):
	#	return obj.comments or "RevisionIndex: Comments not available"

	def index_queryset(self, using=None):
		"""Used when the entire index for model is updated."""
		return self.get_model().objects.filter(created__lte=datetime.datetime.utcnow().replace(tzinfo=utc))
		#return self.get_model().objects.all()

	def get_model(self):
		return Revision


