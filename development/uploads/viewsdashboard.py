'''
Created on Nov 21, 2012

@author: rise
'''
# stdlib imports
import datetime
import pdb
import logging
logger = logging.getLogger('infolog')

# core django imports
from django.shortcuts import render_to_response
from django.template import RequestContext
#from django.http import HttpResponse
from django.utils.timezone import utc
from django import template

register = template.Library()

# 3rd party imports

# project imports
from .models import Submission
from .models import SubmissionViewTracking
from core.models import UserProfile

@register.filter(name='get_views_key')
def get_views_count(dictionary,key):
	return dictionary.get(key)

def getDashboard(request):	
	'''navigate to dashboard after user authenticates into site'''
	try: 
		# get number of submissions by logged in user
		logged_in_user = UserProfile.objects.get(user=request.user)
		logged_in_user_submissions = Submission.objects.all().order_by('-id').filter(user=logged_in_user)

		# output of last 7 or 30 days of views for all submissions of a user
		timestamps = []
		viewsdict = {}
		#pdb.set_trace()
		start_date = datetime.datetime.utcnow().replace(tzinfo=utc) + datetime.timedelta(-30)
		for sub in logged_in_user_submissions:
			if sub.getViewCounts():
				for viewobj in sub.getViewCounts():
					logger.info("==============")
					logger.info(viewobj.created)
					if start_date <= viewobj.created <= datetime.datetime.utcnow().replace(tzinfo=utc):
						timestamps.append(viewobj.created)
		# timestamps.sort() {'7/9/2014': 2, '6/24/2014': 1, '7/13/2014': 2}
		for the_time in timestamps:
			the_date = the_time.date()
			if the_date in viewsdict:
				viewsdict[the_date] = viewsdict[the_date] + 1
			else:
				viewsdict[the_date] = 1
		#sorted(viewsdict, key=viewsdict.get) {'7/9/2014': 2, '7/13/2014': 2, '6/24/2014': 1}
		#logger.info(sorted(viewsdict.keys(), key=lambda k: viewsdict[k][]))
		new_list = sorted(viewsdict.items())
		logger.info("new_list={0}".format(new_list))
		#logger.info(new_list)
		#new_dict = dict(new_list)
		#logger.info("new_dict={0}".format(new_dict))
		#dates = [x[0].strf('%m/%d/%Y') for x in new_list]
		dates = []
		for t in viewsdict:
			dates.append( t.strftime("%m/%d/%Y"))
			dates.sort()
		vals = [x[1] for x in new_list]
		logger.info(vals)
		numofviewspermonth = len(timestamps)


		context = { 'vids': logged_in_user_submissions, 'viewsdictkeys':dates, 'viewsdictvals':vals, 'numofviewspermonth':numofviewspermonth }
		return render_to_response('accounts/dashboard_master.html', context, context_instance=RequestContext(request))
	except Exception as e: 
		print e
		import traceback, sys
		traceback.print_exc(file=sys.stdout)
