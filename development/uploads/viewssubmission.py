'''
Created on Nov 21, 2012

@author: rise
'''
# stdlib imports
import pdb
import os
import traceback
import sys
import time
import urllib2
import json
import requests
import base64
import hmac
import urllib
from hashlib import sha1
import uuid
import logging

# core django imports
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import user_passes_test
from django.core.files.uploadedfile import UploadedFile
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.template import loader
from django.core.urlresolvers import reverse
from django.db import transaction
from django.views.decorators.csrf import csrf_protect

# 3rd party apps
import vimeo
import vimeo.config
import vimeo.convenience
#from celery import tasks

# mfb imports
from .formssubmission import PdbForm
from .formssubmission import TagForm
from .formssubmission import SubmissionForm
from .formssubmission import FileTypeForm
from .formssubmission import DefaultRevisionForm
from .models import Pdb
from .models import Tag
from .models import Submission
from .models import Revision
from .models import FileType
from core.models import UserProfile
from .tasks import *
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')

VALIDMIMES = [
			'text/plain',
			'application/octet-stream',
			'application/xml',
			'application/pdf',
			'application/zip',
			'inode/directory',
			'application/vnd.openxmlformats-officedocument.presentationml.presentation',
			'image/x-xcf',
			'application/postscript',
			'image/vnd.adobe.photoshop',
			'video/quicktime',
			'video/mp4',
			'video/x-msvideo',
			'video/x-ms-asf',
			'video/avi',
			'video/mov',
]

## use a sleep to wait a few secs for vimeo servers to be synced.
## sometimes, going too fast
sleep_workaround = True
AWS_ACCESS_KEY_ID = get_env_variable('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = get_env_variable('AWS_SECRET_ACCESS_KEY')
AWS_BUCKET_URL = get_env_variable('AWS_BUCKET_URL')
AWS_STORAGE_BUCKET_NAME = get_env_variable('AWS_STORAGE_BUCKET_NAME')
VIMEO_API_KEY = get_env_variable('VIMEO_API_KEY')
VIMEO_API_SECRET = get_env_variable('VIMEO_API_SECRET')
VIMEO_ACCESS_TOKEN = get_env_variable('VIMEO_ACCESS_TOKEN')
VIMEO_ACCESS_SECRET = get_env_variable('VIMEO_ACCESS_SECRET')
VIMEO_OEMBED_URL = get_env_variable('VIMEO_OEMBED_URL')

def getNumberOfViews(vimeoid):
	client = vimeo.VimeoClient(key=VIMEO_API_KEY, 
							   secret=VIMEO_API_SECRET,
							   token=VIMEO_ACCESS_TOKEN,
							   token_secret=VIMEO_ACCESS_SECRET,
							   format="json"
							)

	info = client.vimeo_videos_getInfo(video_id=vimeoid)

	logger.info("numofviews = %s, %s"%(type(info), info[0]["number_of_plays"]))
	return info[0]["number_of_plays"]

def getOembed(vimeoid):
	logger.info("============== START: GETOEMBED =================")
	try:
		iframestr = ''
		r = requests.get(VIMEO_OEMBED_URL + str(vimeoid))

		try:
			iframestr = str(r.json()['html'])
			logger.info("str(r.json()['html']) = %s"%iframestr)

		except ValueError:
			return ''
		else:
			return iframestr #s['html']

	except requests.exceptions.RequestException as e:
		logger.info('HTTP Error %s occurred'%e)
		return ''

#def login_remote_mfb_user(user):
#	return user.is_authenticated() and user.is_active


def sign_s3_remote(request,un,pw,filename):
	# create login code for un, pw
	# call sign_s3 and return signature to MFB app HTTPResponse
	#sign_s3()
	pass

#@user_passes_test(login_remote_mfb_user, un, pw)
def sign_s3(request):
	# Load necessary information into the application:
	logger.info("============== START: SIGN_S3 =================")
	logger.info('===== SIGN_S3 REQUEST = %s'%request)
	logger.info('AWS_ACCESS_KEY = %s'%AWS_ACCESS_KEY_ID)
	logger.info('AWS_SECRET_ACCESS_KEY = %s'%AWS_SECRET_ACCESS_KEY)
	logger.info('AWS_STORAGE_BUCKET_NAME = %s'%AWS_STORAGE_BUCKET_NAME)

	# Collect information on the file from the GET parameters of the request:
	object_name = urllib.quote_plus(request.GET.get('s3_object_name'))
	print("SIGN_S3: request.GET.get('s3_object_name') = %s"%request.GET.get('s3_object_name'))
	mime_type = request.GET.get('s3_object_type')
	print("SIGN_S3: request.GET.get('s3_object_type') = %s"%request.GET.get('s3_object_type'))
	logger.info("SIGN_S3: request.GET') = %s"%request.GET)

	
	# Set the expiry time of the signature (in seconds) and declare the permissions of the file to be uploaded
	expires = int(time.time()+10)
	logger.info("=========== SIGN_S3: expires = %s"%expires)
	amz_headers = "x-amz-acl:public-read"

	# sanitize the filename
	randnum = str(uuid.uuid4())
	ext = object_name.split('.')[-1]
	sanitized_name = "%s.%s" %(randnum, ext)
	logger.info("sanitized_name = %s"%sanitized_name)

	if request.user.is_authenticated():
		logger.info("request.user = %s and request.user.id = %s"%(request.user, request.user.id))
	uid = UserProfile.objects.get(user=request.user.id).id
	logger.info("uid = %s"%uid)

	# place file in appropriate user directory
	if mime_type in ['application/octet-stream','text/plain','application/xml','application/pdf',
						'application/zip','inode/directory',
						'application/vnd.openxmlformats-officedocument.presentationml.presentation',
						'application/postscript','image/vnd.adobe.photoshop']:
		key = "media/scenes/uid_" + str(uid)
	elif mime_type in ['image/jpeg','image/png','image/gif']:
		key = "media/images/uid_" + str(uid)
	elif mime_type in ['video/x-ms-wmv','video/mp4','video/quicktime','video/avi']:
		key = "media/videos/uid_" + str(uid)

	print("key = %s"%key)
	print("sanitized_name = %s"%sanitized_name)

	# Generate the PUT request that JavaScript will use:
	put_request = "PUT\n\n%s\n%d\n%s\n/%s/%s/%s" % (mime_type, expires, amz_headers, AWS_STORAGE_BUCKET_NAME, key, sanitized_name)

	# Generate the signature with which the request can be signed:
	signature = base64.encodestring(hmac.new(AWS_SECRET_ACCESS_KEY, put_request, sha1).digest())
	# Remove surrounding whitespace and quote special characters:
	signature = urllib.quote_plus(signature.strip())
	logger.info("======= SIGN_S3: signature = %s"%signature)

	# Build the URL of the file in anticipation of its imminent upload:
	url = '%s/%s/%s' % (AWS_BUCKET_URL, key, sanitized_name)
	logger.info("======= SIGN_S3: url = %s"%url)

	content = json.dumps({
		'signed_request': '%s?AWSAccessKeyId=%s&Expires=%d&Signature=%s' % (url, AWS_ACCESS_KEY_ID, expires, signature),
		'url': url
	})
	
	# Return the signed request and the anticipated URL back to the browser in JSON format:
	return HttpResponse(content, content_type='application/json')

def success(request, pk):
	#recent_revision = Submission.objects.get(id=id).revisions.order_by(-id)[0]
	template = loader.get_template("accounts/successful_submission.html")
	context = RequestContext(request,{'revid':pk})
	return HttpResponse(template.render(context))

def user_can_upload(user):
	return user.is_authenticated() and user.is_active


@user_passes_test(user_can_upload, login_url="/login/")
@csrf_protect
def MFBSubmission(request, **kwargs):
	'''create new Submission and default Revision objects'''
	logger.info("===================== START: IN MFBSubmission: request.REQUEST: %s"%request.REQUEST)
	new_submission = Submission()
	default_revision = Revision()
	if request.method == 'POST': #and request.is_ajax():
		#pdb.set_trace()
		# create 3 form instances for 3 model objects, collect POST data for each object; 
		# bound data to form instances
		pdbform = PdbForm(request.POST)
		tagform = TagForm(request.POST)
		subform = SubmissionForm(request.POST, instance=new_submission) 
		ftform = FileTypeForm(request.POST)
		revform = DefaultRevisionForm(request.POST)
		logger.info("POST: %s" %(request.POST))
		logged_in_user = UserProfile.objects.get(user=request.user)
		logger.info("logged_in_user = %s"%logged_in_user)
		
		# is_valid() checks to see if data is clean for both form instance and model instance and saves data to instances
		if pdbform.is_valid() and tagform.is_valid() and subform.is_valid() and ftform.is_valid() and revform.is_valid(): # 
			#pdb.set_trace()
			try:
				new_submission = subform.save(commit=False)
				new_submission.user = logged_in_user
				new_submission.authors = subform.cleaned_data['authors']
				new_submission.filetype= ftform.cleaned_data['filetype'] #already cleaned in formsubmission.py
				new_submission.vidfeature = False
				new_submission.vidstaffpick = False
				new_submission.save() # save instance Submission object in db

				if pdbform.cleaned_data['pdbname']:
					for pdbpk in pdbform.cleaned_data['pdbname']:
						pdb = Pdb.objects.get(pk=pdbpk)
						pdb.submissions.add(new_submission)
						pdb.count = pdb.mostUsedPdb()
						pdb.save()

				if tagform.cleaned_data['tagname']:
					for tagpk in tagform.cleaned_data['tagname']:
						tag = Tag.objects.get(pk=tagpk)
						tag.submissions.add(new_submission)
						tag.count = tag.mostUsedTag()
						tag.save()

				# submission object always has FK in Revision, related to Submission instance and UserProfile instance
				default_revision.submission = new_submission
				default_revision.user = logged_in_user
				default_revision.comments = ''
				logger.info("inside view: revform.cleaned_data['sourcefile'] = %s"%revform.cleaned_data['sourcefile'])
				default_revision.sourcefile = revform.cleaned_data['sourcefile']
				default_revision.save()

				# user does not upload video nor thumbnail file; for thumbnail use generic filetype
				if revform.cleaned_data['vidanimation']:
					default_revision.vidanimation= revform.cleaned_data['vidanimation']
				else:
					default_revision.vidanimation = ""

				default_revision.vimeoid = ""
				extension = ""
				try:
					logger.info("Filetype = %s"%FileType.objects.get(pk=request.POST.get('filetype')).filetype.split('.')[2])
					extension = FileType.objects.get(pk=request.POST.get('filetype')).filetype.split('.')[2]
				except:
					logger.info("Filetype = %s"%FileType.objects.get(pk=request.POST.get('filetype')).filetype.split('.')[1])
					extension = FileType.objects.get(pk=request.POST.get('filetype')).filetype.split('.')[1]

				if revform.cleaned_data['vidpic']:
					logger.info("inside view: revform.cleaned_data['vidpic'] = %s"%revform.cleaned_data['vidpic'])
					default_revision.vidpic = revform.cleaned_data['vidpic']
				else:
					default_revision.vidpic = os.path.join(AWS_BUCKET_URL, 'media','icons','generic-icon-1024x576-' + extension + '.png')
					logger.info('full url path thumb_small = %s'%os.path.join(AWS_BUCKET_URL, 'media','icons', extension + '_small.jpg'))

				# use placeholder images until we can process the thumbnails, push to S3, and save to DB in Celery task
				default_revision.thumb_small = os.path.join(AWS_BUCKET_URL, 'media','icons', extension + '_small.jpg')
				default_revision.thumb_medium = os.path.join(AWS_BUCKET_URL, 'media','icons', extension + '_medium.jpg')
				default_revision.thumb_large = os.path.join(AWS_BUCKET_URL, 'media','icons', extension + '_large.jpg')
				default_revision.save() # save instance Revision object in db

				logger.info("inside view: revform.cleaned_data['vidanimation'] = %s"%revform.cleaned_data['vidanimation'])
				# start celery task workers
				if default_revision.vidanimation:
					result = send_to_vimeo.delay( str(new_submission.id), str(default_revision.id) )

				if 'generic' not in default_revision.vidpic:
					result = make_thumbnails.apply_async(args=[str(new_submission.id), str(default_revision.id), extension],  countdown=10)

			except Exception as e:
				print e
				traceback.print_exc(file=sys.stdout)
			finally:
				return HttpResponseRedirect(reverse('success', args=[default_revision.pk]))

		else:
			# form not valid, try again
			#context = { 'pdbform': pdbform, 'tagform': tagform,'subform': subform, 'ftform': ftform, 'revform': revform, 'user': request.user,'userid': request.user.id  }
			context = { 'pdbform': pdbform, 'tagform': tagform,'subform': subform, 'ftform': ftform, 'revform': revform, 'user': request.user, 
				'userid': request.user.id,  #'title': title,'pdbs': pdbs,
				}
			return render_to_response('accounts/submission_form.html', context, context_instance=RequestContext(request))
		
	else:
		'''user is not submitting the form, show them a blank submission form'''
		# unbound form, it cannot do validation, renders a blank form as HTML
		pdbform = PdbForm()
		tagform = TagForm()
		subform = SubmissionForm(instance=new_submission)
		ftform  = FileTypeForm()
		revform = DefaultRevisionForm(instance=default_revision)

		logger.info('====== VIEWSSUBMISSION request.GET = %s'%request.GET)
		#if ( request.GET ):
		#	title = request.GET.get('title')
		#	pdbname = request.GET.get('pdbname')
		#	logger.info('====== VIEWSSUBMISSION pdbname = %s'%pdbname)
		#	logger.info('====== VIEWSSUBMISSION title = %s'%title)
		#	pdbname = pdbname.split('+')
		#	pdbs = ""
		#	for pdb in pdbname:
		#		if pdbs:
		#			pdbs = pdbs + ',' + pdb
		#		else:
		#			pdbs = pdb
		#	pdbs = str(pdbs)

		#else:
		#	title = ""
		#	pdbs = ""

		# add form to context
		context = { 'pdbform': pdbform, 'tagform': tagform,'subform': subform, 'ftform': ftform, 'revform': revform, 'user': request.user, 
			'userid': request.user.id, #'title': title, 'pdbs': pdbs,
			}
		return render_to_response('accounts/submission_form.html', context, context_instance=RequestContext(request))
