# stdlib imports
from __future__ import absolute_import
import pdb
import os
from os.path import join, abspath, dirname
import traceback
import sys
import logging
import time
import uuid
import urllib
import urllib2
import json
import requests
import StringIO
import string
import hashlib
from os import path, access, R_OK # W_OK for write permission
import tempfile
from PIL import Image, ImageOps
import PIL


# core django imports
from django.core.files import File
from django.core.files.base import ContentFile
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction

# 3rd party apps
import vimeo
import vimeo.config
import vimeo.convenience
from celery import shared_task
from boto.s3.connection import S3Connection
from boto.s3.key import Key
from time import sleep
#from celery import task, current_task
from celery.result import AsyncResult
#from celery.registry import tasks
from celery.utils.log import get_task_logger

# mfb imports
from .models import Revision
from .models import Submission
from .models import SubmissionViewTracking
from .models import Tutorial
from settings.base import get_env_variable

logger = get_task_logger('infolog')

AWS_ACCESS_KEY_ID = get_env_variable('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = get_env_variable('AWS_SECRET_ACCESS_KEY')
AWS_BUCKET_URL = get_env_variable('AWS_BUCKET_URL')
AWS_STORAGE_BUCKET_NAME = get_env_variable('AWS_STORAGE_BUCKET_NAME')
VIMEO_API_KEY = get_env_variable('VIMEO_API_KEY')
VIMEO_API_SECRET = get_env_variable('VIMEO_API_SECRET')
VIMEO_ACCESS_TOKEN = get_env_variable('VIMEO_ACCESS_TOKEN')
VIMEO_ACCESS_SECRET = get_env_variable('VIMEO_ACCESS_SECRET')
VIMEO_OEMBED_URL = get_env_variable('VIMEO_OEMBED_URL')


@shared_task
def getOembed(vimeoid):
	import ast

	try:
		iframestr = ''
		r = requests.get(VIMEO_OEMBED_URL + str(vimeoid))

		try:
			iframestr = str(r.json()['html'])
			logging.info("str(r.json()['html']) = %s"%iframestr)

		except ValueError:
			return ''
		else:
			return iframestr #s['html']

	except requests.exceptions.RequestException as e:
		logging.info('HTTP Error %s occurred'%e)
		return ''

# using the @shared_task decorateor lets you create tasks without having
# any concrete Celery instance; using bind makes fn a bound method so we
# can access attributes and methods on the task type instance
# retry: 2 minutes
@shared_task(default_retry_delays=2 * 60, max_retries=3)
def uploadvid( subpk, revpk ):
	#cache.set(uploadvid.request.id, operation_results)
	try:
		sleep_workaround = True
		sub = Submission.objects.get(pk=int(subpk))
		rev = Revision.objects.get(pk=int(revpk))

		logging.info("============= START: uploadvid============")
		client = vimeo.VimeoClient(key=VIMEO_API_KEY, 
								   secret=VIMEO_API_SECRET,
								   token=VIMEO_ACCESS_TOKEN,
								   token_secret=VIMEO_ACCESS_SECRET,
								   format="json"
								)

		quota = client.vimeo_videos_upload_getQuota()
		logging.info("Your current quota = %s MiB" %str(int(quota['upload_space']['free'])/(1024*1024)))
		t = client.vimeo_videos_upload_getTicket()
		logging.info("Ticket = %s" %t['id'])
		vup = vimeo.convenience.VimeoUploader(client, t, quota=quota)
		#if client.vimeo_videos_upload_checkTicket(oauth_token=VIMEO_ACCESS_SECRET,ticket_id=t['id']):
		#ticket_chunks = vup.upload(ROOT + rev.vidanimation.url)

		# get video from S3
		keypath = (rev.vidanimation).split('com/')[-1]
		filename = keypath.split('/')[-1]
		conn = S3Connection(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
		bucket = conn.get_bucket(AWS_STORAGE_BUCKET_NAME, validate=False)
		k = Key(bucket)
		k.key = keypath
		dirpath = tempfile.mkdtemp()
		FILEPATH = os.path.join(dirpath,filename)
		logging.info("======= UPLOADVID: FILEPATH filename = %s" %FILEPATH)
		k.get_contents_to_filename(FILEPATH)

		if path.isfile(FILEPATH) and access(FILEPATH, R_OK):
			md5 = hashlib.md5()
			with open(FILEPATH, 'rb') as f:
				for chunk in iter(lambda: f.read(256*128), b''):
					md5.update(chunk)

		# check hash; upload file to vimeo
		if '"%s"'%md5.hexdigest() == k.etag:
			logging.info("======= UPLOADVID: md5.hexdigest() == k.etag")
			ticket_chunks = vup.upload(FILEPATH)
			#ticket_chunks = vup.upload(cleanedvideo)
			logging.info("ticket_chunks = %s"%ticket_chunks)
			vid = vup.complete()['video_id']
			logging.info("======= UPLOADVID: vid = %s"%vid)
			rev.vimeoid = vid 
			rev.save(update_fields=['vimeoid'])
			logging.info("======= UPLOADVID: SAVED REVISION")

			if sleep_workaround and (sub.title or sub.description or sub.displayTags()):
				time.sleep(5)

			if sub.title:
				client.vimeo_videos_setTitle(video_id=vid, title=sub.title)

			if sub.description:
				client.vimeo_videos_setDescription(video_id=vid, description=sub.description)

			if sub.displayTags():
				client.vimeo_videos_addTags(video_id=vid, tags=sub.displayTags())

		logging.info("============= DONE: uploadvid============")
	except Exception as exc:
		raise uploadvid.retry(exc=exc)

# retry in 5 minutes
@shared_task(default_retry_delays=5 * 60, max_retries=6)
def getVimeoThumbnailUrl(revpk):
	rev = Revision.objects.get(pk=int(revpk))
	userid = rev.user.id
	randomnumber = rev.vidanimation.split('/')[-1].split('.')[0].split('_')[0]

	try:
		logging.info("============= START: getVimeoThumbnailUrl============")
		client = vimeo.VimeoClient(key=VIMEO_API_KEY, 
								   secret=VIMEO_API_SECRET,
								   token=VIMEO_ACCESS_TOKEN,
								   token_secret=VIMEO_ACCESS_SECRET,
								   format="json"
								)

		thumbnailsurls = client.vimeo_videos_getThumbnailUrls(video_id=rev.vimeoid)
		logging.info("getVimeoThumbnailUrl: thumbnailurls = %s"%thumbnailsurls)

		if thumbnailsurls["thumbnail"]:
			thumbnailurl_640 = thumbnailsurls["thumbnail"][2]["_content"]
			logging.info("type(thumbnailurl_640); thumbnailurl_640 = %s, %s"%(type(thumbnailurl_640),thumbnailurl_640))
			filename = thumbnailurl_640.split('/')[-1] # keep this filename 3.g. 9digits_640.jpg
			logging.info("filename = %s"%filename)
			ext = filename.split('.')[-1]

			# get vimeo thumbnail
			theimage = randomnumber + '.jpeg'
			dirpath = tempfile.mkdtemp()
			FILEPATH = os.path.join(dirpath,theimage)
			logging.info("FILEPATH theimage = %s" %FILEPATH)
			(FILEPATH, headers) = urllib.urlretrieve(thumbnailurl_640, filename)

			img = PIL.Image.open(FILEPATH)

			# push new resized image to S3; keep same filename

			newsourceimg = img.resize((640,360), PIL.Image.ANTIALIAS)
			newsourceimg.save(FILEPATH, format='jpeg')
			conn = S3Connection(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
			bucket = conn.get_bucket(AWS_STORAGE_BUCKET_NAME, validate=False)
			k = Key(bucket)
			k.key = 'media/images/uid_' + str(userid) + '/'+ theimage
			k.set_contents_from_filename(FILEPATH)

			# save new resized file's full url into URLfield
			full_url = AWS_BUCKET_URL + k.key
			logging.info("Full_URL = %s" %full_url )
			rev.vidpic = full_url

			# create thumbnails and push to S3
			small = 120,68
			medium = 185,104
			large = 841,473
			smallname = randomnumber + "_small.jpeg"
			medname = randomnumber + "_medium.jpeg"
			largename = randomnumber + "_large.jpeg"

			imgsmall = img.resize((640,360),PIL.Image.ANTIALIAS)
			imgsmall.thumbnail(small,Image.ANTIALIAS)
			imgsmall.save(os.path.join(dirpath,smallname),format='jpeg')
			k = Key(bucket)
			k.key = 'media/images/uid_' + str(userid) + '/' + smallname
			k.set_contents_from_filename(os.path.join(dirpath,smallname))
			rev.thumb_small = os.path.join(AWS_BUCKET_URL, k.key)

			imgmed = img.resize((640,360),PIL.Image.ANTIALIAS)
			imgmed.thumbnail(medium,Image.ANTIALIAS)
			imgmed.save(os.path.join(dirpath,medname), format='jpeg')
			k = Key(bucket)
			k.key = 'media/images/uid_' + str(userid) + '/' + medname
			k.set_contents_from_filename(os.path.join(dirpath,medname))
			rev.thumb_medium = os.path.join(AWS_BUCKET_URL, k.key)

			imglarge = img.resize((841,473),PIL.Image.ANTIALIAS)
			imglarge.save(os.path.join(dirpath,largename), format='jpeg')
			k = Key(bucket)
			k.key = "media/images/uid_" + str(userid) + '/' + largename
			k.set_contents_from_filename(os.path.join(dirpath,largename))
			rev.thumb_large = os.path.join(AWS_BUCKET_URL, k.key)
			rev.save(update_fields=['thumb_small','thumb_medium','thumb_large'])

		logging.info("============= DONE: getVimeoThumbnailUrl============")
	except Exception as exc:
		raise getVimeoThumbnailUrl.retry(exc=exc)

@shared_task
def send_to_vimeo( subpk, revpk):
	#cache.set(send_to_vimeo.request.id, operation_results)
	rev = Revision.objects.get(pk=revpk)
	logging.info("============= START: SENDTOVIMEO ============")

	# upload vid & get vimeoid; save to db -> wait 10 minutes and then get thumbnail for vimeoid; save to db
	if 'generic' in rev.vidpic:
		chain = uploadvid.s( subpk, revpk) | getVimeoThumbnailUrl.si(revpk).set(countdown=600)

	# push vid to vimeo and keep user uploaded thumbnail
	else:
		chain = uploadvid.s( subpk, revpk )

	chain()

	logging.info("============= DONE: SENDTOVIMEO ============")


@shared_task(default_retry_delays=5 * 60, max_retries=3)
def make_thumbnails( subpk, revpk, extension ):
	''' create thumbnails (small, medium, large) for user-chosen image or generic file image '''
	rev = Revision.objects.get(pk=revpk)
	logging.info("============= START: MAKE_THUMBNAILS ============")
	userid = rev.user.id

	try:
		with transaction.atomic():
			keypath = (rev.vidpic).split('com/')[-1]
			logging.info('keypath = %s'%keypath)
			filename = keypath.split('/')[-1]
			sanitized_name = filename.split('.')[0]

			# grab thumbnail source from S3
			conn = S3Connection(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
			bucket = conn.get_bucket(AWS_STORAGE_BUCKET_NAME, validate=False)
			k = Key(bucket)
			k.key = keypath
			dirpath = tempfile.mkdtemp()
			FILEPATH = os.path.join(dirpath,filename)
			logging.info("FILEPATH/filename = %s" %FILEPATH)
			k.get_contents_to_filename(FILEPATH)

			if path.isfile(FILEPATH) and access(FILEPATH, R_OK):
				md5 = hashlib.md5()
				with open(FILEPATH, 'rb') as f:
					for chunk in iter(lambda: f.read(256*128), b''):
						md5.update(chunk)

			# create thumbnails and push to S3
			if '"%s"'%md5.hexdigest() == k.etag:
				logging.info("MAKE_THUMBNAILS: md5.hexdigest() == k.etag")
				small = 120,68
				medium = 185,104
				large = 841,473
				smallname = sanitized_name + "_small.jpeg"
				medname = sanitized_name + "_medium.jpeg"
				largename = sanitized_name + "_large.jpeg"

				img = PIL.Image.open(FILEPATH)

				if img.mode != "RGB":
					img = img.convert("RGB")
				imgsmall = ImageOps.fit(img, small, PIL.Image.ANTIALIAS)
				imgsmall.save(os.path.join(dirpath, smallname),format='jpeg')
				k = Key(bucket)
				k.key = 'media/images/uid_' + str(userid) + '/' + smallname
				k.set_contents_from_filename(os.path.join(dirpath, smallname))
				thumb_small_url = os.path.join(AWS_BUCKET_URL, k.key)
				Revision.objects.filter(pk=revpk).update(thumb_small=thumb_small_url)

				imgmed = ImageOps.fit(img, medium, PIL.Image.ANTIALIAS)
				imgmed.save(os.path.join(dirpath, medname),format='jpeg')
				k = Key(bucket)
				k.key = 'media/images/uid_' + str(userid) + '/' + medname
				k.set_contents_from_filename(os.path.join(dirpath, medname))
				thumb_medium_url = os.path.join(AWS_BUCKET_URL, k.key)
				Revision.objects.filter(pk=revpk).update(thumb_medium=thumb_medium_url)

				imglarge = ImageOps.fit(img, large, PIL.Image.ANTIALIAS)
				imglarge.save(os.path.join(dirpath, largename),format='jpeg')
				k = Key(bucket)
				k.key = 'media/images/uid_' + str(userid) + '/' + largename
				k.set_contents_from_filename(os.path.join(dirpath, largename))
				thumb_large_url = os.path.join(AWS_BUCKET_URL, k.key)
				Revision.objects.filter(pk=revpk).update(thumb_large=thumb_large_url)


	except Exception as exc:
		raise make_thumbnails.retry(exc=exc)

	logging.info("============= DONE: MAKE_THUMBNAILS ============")

# retry in 5 minutes
@shared_task(default_retry_delays=5 * 60, max_retries=6)
def post_tutorial_thumbnails(adminid, tutid, vimeoid):
	tut = Tutorial.objects.get(pk=int(tutid))

	try:
		logging.info("============= START: POST_TUTORIAL_THUMBNAILS ============")
		client = vimeo.VimeoClient(key=VIMEO_API_KEY, 
								   secret=VIMEO_API_SECRET,
								   token=VIMEO_ACCESS_TOKEN,
								   token_secret=VIMEO_ACCESS_SECRET,
								   format="json"
								)

		thumbnailsurls = client.vimeo_videos_getThumbnailUrls(video_id=vimeoid)
		logging.info("post_tutorial: thumbnailurls = %s"%thumbnailsurls)

		if thumbnailsurls["thumbnail"]:
			thumbnailurl_640 = thumbnailsurls["thumbnail"][2]["_content"]
			logging.info("type(thumbnailurl_640); thumbnailurl_640 = %s, %s"%(type(thumbnailurl_640),thumbnailurl_640))
			filename = thumbnailurl_640.split('/')[-1] # keep this filename 3.g. 9digits_640.jpg
			logging.info("filename = %s"%filename)
			ext = filename.split('.')[-1]

			# get vimeo thumbnail
			theimage = vimeoid + '.jpeg'
			dirpath = tempfile.mkdtemp()
			FILEPATH = os.path.join(dirpath,theimage)
			logging.info("FILEPATH theimage = %s" %FILEPATH)
			(FILEPATH, headers) = urllib.urlretrieve(thumbnailurl_640, filename)

			img = PIL.Image.open(FILEPATH)

			# push new resized image to S3; keep same filename

			newsourceimg = img.resize((640,360), PIL.Image.ANTIALIAS)
			newsourceimg.save(FILEPATH, format='jpeg')
			conn = S3Connection(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
			bucket = conn.get_bucket(AWS_STORAGE_BUCKET_NAME, validate=False)
			k = Key(bucket)
			k.key = 'media/images/uid_' + str(adminid) + '/'+ theimage
			k.set_contents_from_filename(FILEPATH)

			# save new resized file's full url into URLfield
			full_url = AWS_BUCKET_URL + k.key
			logging.info("Full_URL = %s" %full_url )
			tut.tutpic = full_url
			tut.save(update_fields=['tutpic'])

			# create thumbnails and push to S3
			small = 120,68
			medium = 185,104
			large = 841,473
			smallname = vimeoid + "_small.jpeg"
			medname = vimeoid + "_medium.jpeg"
			largename = vimeoid + "_large.jpeg"

			imgsmall = img.resize((640,360),PIL.Image.ANTIALIAS)
			imgsmall.thumbnail(small,Image.ANTIALIAS)
			imgsmall.save(os.path.join(dirpath,smallname),format='jpeg')
			k = Key(bucket)
			k.key = 'media/images/uid_' + str(adminid) + '/' + smallname
			k.set_contents_from_filename(os.path.join(dirpath,smallname))
			tut.thumb_small = os.path.join(AWS_BUCKET_URL, k.key)
			tut.save(update_fields=['thumb_small'])

			imgmed = img.resize((640,360),PIL.Image.ANTIALIAS)
			imgmed.thumbnail(medium,Image.ANTIALIAS)
			imgmed.save(os.path.join(dirpath,medname), format='jpeg')
			k = Key(bucket)
			k.key = 'media/images/uid_' + str(adminid) + '/' + medname
			k.set_contents_from_filename(os.path.join(dirpath,medname))
			tut.thumb_medium = os.path.join(AWS_BUCKET_URL, k.key)
			tut.save(update_fields=['thumb_medium'])

			imglarge = img.resize((841,473),PIL.Image.ANTIALIAS)
			imglarge.save(os.path.join(dirpath,largename), format='jpeg')
			k = Key(bucket)
			k.key = "media/images/uid_" + str(adminid) + '/' + largename
			k.set_contents_from_filename(os.path.join(dirpath,largename))
			tut.thumb_large = os.path.join(AWS_BUCKET_URL, k.key)
			tut.save(update_fields=['thumb_large'])

		logging.info("============= DONE: POST_TUTORIAL_THUMBNAILS ============")
	except Exception as exc:
		raise post_tutorial_thumbnails.retry(exc=exc)

# retry in 5 minutes
@shared_task(default_retry_delays=5 * 60, max_retries=6)
def get_vimeo_info(tutid, vimeoid):
	tut = Tutorial.objects.get(pk=int(tutid))

	try:
		logging.info("============= START: GET_VIMEO_INFO ============")
		client = vimeo.VimeoClient(key=VIMEO_API_KEY,
								   secret=VIMEO_API_SECRET,
								   token=VIMEO_ACCESS_TOKEN,
								   token_secret=VIMEO_ACCESS_SECRET,
								   format="json"
								)

		tutinfo = client.vimeo_videos_getInfo(video_id=vimeoid)
		logging.info("==================get_vimeo_info: tutinfo = %s"%tutinfo)
		logging.info("==================get_vimeo_info: type(tutinfo) = %s"%type(tutinfo))

		if tutinfo[0]["title"]:
			logging.info("tutinfo[0]['title'] = %s"%tutinfo[0]["title"])
			logging.info("type(tutinfo[0]['title']) = %s"%type(tutinfo[0]["title"]))
			tut.title = tutinfo[0]["title"]
			tut.save(update_fields=['title'])

		if tutinfo[0]["description"]:
			logging.info("tutinfo[0]['description'] = %s"%tutinfo[0]["description"])
			if not tut.comments:
				tut.comments = tutinfo[0]["description"]
				tut.save(update_fields=['comments'])

		#if tutinfo[0]["tags"]:
		#	logging.info("tutinfo[0]['tags'] = %s"%tutinfo[0]["tags"])
		#	if not tut.tut_tags:
		#		tut.tut_tags = tutinfo[0]["tags"]
		#		tut.save(update_fields=['tut_tags'])

		logging.info("============= DONE: GET_VIMEO_INFO ============")
	except Exception as exc:
		raise get_vimeo_info.retry(exc=exc)

# retry in 1 minutes
@shared_task(default_retry_delays=1 * 60, max_retries=6)
def log_submission_view(subid):
	sub = Submission.objects.get(pk=int(subid))

	try:
		logging.info("============= START: LOG_SUBMISSION_VIEWS ============")
		# update views field in Submission model
		sub.trackViews()

		# associate the view with a timestamp
		subview = SubmissionViewTracking()
		subview.save()
		subview.count = subview.count + 1
		subview.submissions.add(sub)
		subview.save()
		logging.info("================== subview.count: = %s"%subview.count)

		logging.info("============= DONE: LOG_SUBMISSION_VIEWS ============")
	except Exception as exc:
		raise log_submission_view.retry(exc=exc)
