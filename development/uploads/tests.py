import datetime

from django.test import TestCase
#from django.test.client import Client
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.template import Template, Context
from django.utils.timezone import utc
from datetime import date
from django_webtest import WebTest

# Create your tests here.
from .models import Submission
from .models import Revision
from .models import Pdb
from .models import Tag
from .models import Tutorial
from .models import FileType
from .models import SubmissionViewTracking
from .formsrevision import PdbForm
from .formsrevision import TagForm
from .formsrevision import SubmissionForm
from .formsrevision import RevisionForm
from .formsrevision import FileTypeForm
