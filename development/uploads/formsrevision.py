'''
Created on Jan 3, 2013

@author: rise
'''
# stdlib imports
import os
import logging


# core django imports
from django import forms
from django.forms import ModelForm # ability to pass some model to the form

# 3rd party imports
import magic

# project app imports
from .models import Pdb
from .models import Tag
from .models import Submission
from .models import Revision
from .models import FileType
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')

FILETYPES = [
				('0', 'Select file format of source file'),
				('10', '.flipbook (Molecular FlipBook)'),
				('20', '.blend (Blender)'),
				('30', '.ma/.mb (Maya)'),
				('40', '.c4d (Cinema 4D)'),
				('50', '.max/.3ds (3DS Max)'),
				('60', '.dae (Collada)'),
				('70', '.x3d (Web 3D)'),
				('80', '.pdf (Acrobat)'), 
				('90', '.key (Keynote)'),
				('100','.ppt/.pptx (Powerpoint)'),
				('110', '.ai (Illustrator)'), 
				('120', '.psd/.eps (Photoshop)'),
				('130','.xcf (GIMP)'),
				('140', 'Other file format')
				]

VALIDMIMES = [
			'text/plain',
			'application/octet-stream',
			'application/xml',
			'application/pdf',
			'application/zip',
			'inode/directory',
			'application/vnd.openxmlformats-officedocument.presentationml.presentation',
			'text/plain',
			'image/x-xcf',
			'application/postscript',
			'image/vnd.adobe.photoshop',
			'video/quicktime',
			'video/mp4',
			'video/x-msvideo',
			'video/x-ms-asf',
			'video/avi',
			'video/mov',
]


class PdbForm(forms.Form): # (ModelForm):
	'''create a PDB form to list pdb items for scene'''
	pdbname 		= forms.CharField(label=(u'Pdbs'), max_length=60, required=False)
	
	def clean_pdbname(self):
		delimiter = ','
		pdblist = []
		pdbpklist = []

		# test if pdbname has an empty string/zero pdbs
		if not self.cleaned_data['pdbname']:
			pass

		# test if pdbname has one or more pdbs, accounts for ending comma
		else:

			pdblist = [ pdb.strip().lower() for pdb in self.cleaned_data['pdbname'].split(delimiter) ]
			logger.info("pdblist = %s"%pdblist)
			for pdbitem in pdblist:
				if pdbitem:
					logger.info("pdbitem = %s"%pdbitem)
					try:
						pdbpklist.append(Pdb.objects.get(pdbname__iexact=pdbitem).pk)
					except Pdb.DoesNotExist:
						pdbpklist.append(Pdb.objects.create(pdbname=pdbitem).pk)

		logger.info("pdbpklist = %s"%pdbpklist)
		self.cleaned_data['pdbname'] = pdbpklist
		return self.cleaned_data['pdbname']


class TagForm(forms.Form): #(ModelForm):
	'''create a filetype form to describe file format'''
	tagname			= forms.CharField(label=(u'Tags'), max_length=2000, required=False)

	def clean_tagname(self):
		delimiter = ','
		taglist = []
		tagpklist = []

		# do nothing if tagname has empty string/zero tags
		if not self.cleaned_data['tagname']:
			pass

		# test if tagname has one or more tags, accounts for ending comma
		else:
			logger.info("cleaned_data['tagname'] = %s"%self.cleaned_data['tagname'])
			taglist = [ tag.strip().lower() for tag in self.cleaned_data['tagname'].split(delimiter) ]
			logger.info("taglist = %s"%taglist)
			for tagitem in taglist:
				if tagitem:
					logger.info("tagitem = %s"%tagitem)
					try:
						tagpklist.append(Tag.objects.get(tagname__iexact=tagitem).pk)
					except Tag.DoesNotExist:
						tagpklist.append(Tag.objects.create(tagname=tagitem).pk)

		logger.info("tagpklist = %s"%tagpklist)
		self.cleaned_data['tagname'] = tagpklist
		return self.cleaned_data['tagname']


class SubmissionForm(ModelForm):
	'''create a Submission object and modify with revision form object'''
	title			= forms.CharField(label=(u'Title'), widget=forms.TextInput({"class":"error", 'id':'title'}))
	authors			= forms.CharField(label=(u'Author(s)'), widget=forms.TextInput({"class":"error", 'id':'authors'}))
	description	 	= forms.CharField(label=(u'Description'), widget=forms.Textarea({"placeholder":"Tell us about your animation","class":"error",'id':'description'}))
	ancestors		= forms.CharField(label=(u'Ancestors'), widget=forms.TextInput({"placeholder":"Links to references or related content", "class":"ignore",'id':'ancestors'}), required=False)

	class Meta:
		model = Submission
		fields = ('title','authors','description','ancestors')

	def clean_title(self):
		title = self.cleaned_data['title']
		if title is None:
			raise forms.ValidationError("Please enter a title.")
		return self.cleaned_data['title']
	
	def clean_authors(self):
		authors = self.cleaned_data['authors']
		if authors is None:
			raise forms.ValidationError("Please enter author(s).")
		return self.cleaned_data['authors']

	def clean_description(self):
		description = self.cleaned_data['description']
		if description is None:
			raise forms.ValidationError("Please tell us about your animation.")
		return self.cleaned_data['description']

	def clean_ancestors(self):
		ancestors = self.cleaned_data['ancestors']
		if ancestors is None:
			raise forms.ValidationError("Please cite the reference file.")
		return self.cleaned_data['ancestors']


class FileTypeForm(ModelForm):
	'''create a filetype form to describe file format'''
	filetype 		= forms.ChoiceField(label=(u'Filetype'), widget=forms.Select({"id":'filetype-select'}), choices=FILETYPES, initial="Select file format of source file")
	
	class Meta:
		model = FileType
		
	def clean_filetype(self):
		filetype_pk = self.cleaned_data['filetype']
		try:
			return FileType.objects.get(pk=filetype_pk)
		except FileType.DoesNotExist:
			raise forms.ValidationError("Please choose a filetype.")
		
		
class RevisionForm(ModelForm):
	'''create a Revision object with form, add when Submission object is revised'''
	sourcefile		= forms.URLField(widget=forms.HiddenInput({'id':'sourceurl'}))
	vidpic			= forms.URLField(widget=forms.HiddenInput({'id':'imgurl'}), required=False)
	vidanimation	= forms.URLField(widget=forms.HiddenInput({'id':'vidurl'}), required=False)
	comments		= forms.CharField(label=(u'Ancestors'), widget=forms.TextInput({"placeholder":"Please enter comments", "class":"input-block-level", 'id':'comments'}), required=False)

	class Meta:
		model = Revision
		fields = ('sourcefile','vidanimation','vidpic', 'comments')

	def clean_sourcefile(self):
		upload = self.cleaned_data.get('sourcefile')
		if upload:
			upload = upload.strip()
		self.cleaned_data['sourcefile'] = upload
		logger.info("clean_sourcefile = %s"%self.cleaned_data['sourcefile'])
		return self.cleaned_data['sourcefile']

	def clean_vidpic(self):
		upload = self.cleaned_data.get('vidpic')
		if upload:
			upload = upload.strip()
		else:
			upload = ""
		self.cleaned_data['vidpic'] = upload
		logger.info("clean_vidpic = %s"%self.cleaned_data['vidpic'])
		return self.cleaned_data['vidpic']

	def clean_vidanimation(self):
		upload = self.cleaned_data.get('vidanimation')
		if upload:
			upload = upload.strip()
		else:
			upload = ""
		self.cleaned_data['vidanimation'] = upload
		logger.info("clean_vidanimation = %s"%self.cleaned_data['vidanimation'])
		return self.cleaned_data['vidanimation']

	def clean_comments(self):
		comments = self.cleaned_data.get('comments')
		return self.cleaned_data['comments']
	