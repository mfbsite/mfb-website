'''
Created on Jan 3, 2013

@author: rise
'''
# stdlib imports
import os
import logging

# core django imports
from django import forms
from django.forms import ModelForm # ability to pass some model to the form

# 3rd party imports
import magic

# project app imports
from .models import Tutorial
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')
		
class TutorialForm(ModelForm):
	'''create a Tutorial object with form to post to Tutorial'''
	vimeoid			= forms.CharField(label=(u'VimeoID'),widget=forms.TextInput({"placeholder":"Enter valid Vimeo ID"}))
	tutfeature		= forms.BooleanField(label=(u'Feature Tutorial'),widget=forms.CheckboxInput(),required=False)
	comments	 	= forms.CharField(widget=forms.Textarea({"placeholder":"Describe your tutorial"}), required=False)

	class Meta:
		model = Tutorial
		fields = ('vimeoid','tutfeature','comments')


class FeatureTutorialForm(ModelForm):
	tutfeature 		= forms.BooleanField(widget=forms.CheckboxInput(),required=False)

	class Meta:
		model = Tutorial
		fields = ('tutfeature',)
