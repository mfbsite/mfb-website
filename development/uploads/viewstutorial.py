'''
Created on May 19, 2014

@author: rise
'''
# stdlib imports
import pdb
import os
import traceback
import sys
import time
import urllib2
import json
import requests
import base64
import hmac
import urllib
from hashlib import sha1
import uuid
import re
import logging

# core django imports
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import user_passes_test
from django.core.files.uploadedfile import UploadedFile
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.template import loader
from django.core.urlresolvers import reverse
from django.db import transaction
from django.views.decorators.csrf import csrf_protect

# 3rd party apps
import vimeo
import vimeo.config
import vimeo.convenience
#from celery import tasks

# mfb imports
from .formstutorial import TutorialForm
from .formstutorial import FeatureTutorialForm
from .models import Tutorial
from uploads.viewssubmission import getNumberOfViews, getOembed
from core.models import UserProfile
from .tasks import *
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')

## use a sleep to wait a few secs for vimeo servers to be synced.
## sometimes, going too fast
sleep_workaround = True
AWS_ACCESS_KEY_ID = get_env_variable('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = get_env_variable('AWS_SECRET_ACCESS_KEY')
AWS_BUCKET_URL = get_env_variable('AWS_BUCKET_URL')
AWS_STORAGE_BUCKET_NAME = get_env_variable('AWS_STORAGE_BUCKET_NAME')
VIMEO_API_KEY = get_env_variable('VIMEO_API_KEY')
VIMEO_API_SECRET = get_env_variable('VIMEO_API_SECRET')
VIMEO_ACCESS_TOKEN = get_env_variable('VIMEO_ACCESS_TOKEN')
VIMEO_ACCESS_SECRET = get_env_variable('VIMEO_ACCESS_SECRET')
VIMEO_OEMBED_URL = get_env_variable('VIMEO_OEMBED_URL')


def getTutInfo(request, tutPk):
	'''display detailed editable information for given revision object'''
	selected_tut = Tutorial.objects.get(pk=tutPk)

	logger.info("============ getTutInfo: vimeoid = %s"%selected_tut.vimeoid)
	iframe = getOembed(selected_tut.vimeoid)
	# if original size is not 640x360, change dimensions in iframe
	logger.info("========== viewsrevision: getTutInfo: iframe = %s, type(iframe) = %s"%(iframe,type(iframe)))
	numofviews = getNumberOfViews(selected_tut.vimeoid)

	if iframe:
		pattern = re.compile('width="[0-9]+" height="[0-9]+"')
		#iframe = p.sub('width="640" height="360"', iframe)
		#iframe = pattern.sub('width="620" height="360"', iframe)
		iframe = pattern.sub('width="940" height="705"', iframe)
		logger.info(iframe)
		#pattern = re.compile('src="//player.vimeo.com/video/[0-9]+"')
		#m = re.search(r'src="//player.vimeo.com/video/[0-9]+',iframe)
		#if m:
		#	logger.info('getTutInfo: p = %s'%m.group(0))
		#	newsrc = str(m.group(0))
		#	logger.info(newsrc)
		#	iframe = pattern.sub(newsrc, iframe)

		context = { 'tut': selected_tut, 'vimeoid':selected_tut.vimeoid,'tutid':tutPk,'title': selected_tut.title, 'thumb_large':selected_tut.thumb_large,'tags':selected_tut.tut_tags,'comments': selected_tut.comments, 
					'oembed': iframe,'timestamp': selected_tut.created
				}

	return render_to_response('accounts/tutorial_info.html', context, context_instance=RequestContext(request))

def admin_settings(request):
	context = { 'username': request.user}
	return render_to_response("accounts/admin_settings.html", context, context_instance=RequestContext(request))

def post_tutorial_done(request, vimeoid):
	tutpk = Tutorial.objects.get(vimeoid=vimeoid).id
	template = loader.get_template("accounts/post_tutorial_done.html")
	context = RequestContext(request,{'tutpk':tutpk,'vimeoid':vimeoid})
	return HttpResponse(template.render(context))

def user_is_staff_admin(user):
	return user.is_authenticated() and user.is_staff
				
@user_passes_test(user_is_staff_admin, login_url="/login/")
@csrf_protect
def post_tutorial(request):	
	'''create and post new Tutorial object to Tutorial Library '''
	if request.method == 'POST': #and request.is_ajax():
		logger.info("================= START POST_TUTORIAL request.POST: %s" %(request.POST))
		tutform = TutorialForm(request.POST)
		logger.info("logged_in_super_user = %s"%request.user)
		
		if tutform.is_valid(): # 
			try:
				new_tutorial = tutform.save()

				logger.info("inside post_tutorial: tutform.cleaned_data['vimeoid'] = %s"%tutform.cleaned_data['vimeoid'])
				logger.info("inside post_tutorial: new_tutorial.id = %s"%new_tutorial.id)
				# start celery task workers
				if new_tutorial.vimeoid:
					result1 = post_tutorial_thumbnails.delay( str(request.user.id), str(new_tutorial.id), str(new_tutorial.vimeoid) )
					result2 = get_vimeo_info.delay( str(new_tutorial.id), str(new_tutorial.vimeoid) )

			except Exception as e:
				print e
				traceback.print_exc(file=sys.stdout)
			finally:
				logger.info("================= END POST_TUTORIAL ")
				return HttpResponseRedirect(reverse('post_tutorial_done', args=[new_tutorial.vimeoid]))

		else:
			# form not valid, try again
			context = { 'tutform': tutform, 'user': request.user  }
			return render_to_response('accounts/post_tutorial_form.html', context, context_instance=RequestContext(request))
		
	else:
		'''user is not submitting the form, show them a blank registration form'''
		# unbound form, it cannot do validation, renders a blank form as HTML
		tutform = TutorialForm()
		tutblocked = 'true'
		try:
			if Tutorial.objects.get(tutfeature=True):
				tutblocked = 'true'
		except Tutorial.DoesNotExist:
			tutblocked = 'false'

		# add form to context
		context = { 'tutform': tutform, 'tutblocked':tutblocked,'user': request.user, }
		return render_to_response('accounts/post_tutorial_form.html', context, context_instance=RequestContext(request))

def feature_tutorial_change_done(request,vimeoid):
	username = request.user.username
	tut = Tutorial.objects.get(vimeoid=vimeoid)
	tutpk = tut.id
	title = tut.title
	template = loader.get_template("accounts/feature_tutorial_change_done.html")
	context = RequestContext(request,{'tutpk':tutpk,'title':title,'username':username})
	return HttpResponse(template.render(context))

@user_passes_test(user_is_staff_admin, login_url="/login/")
@csrf_protect
def feature_animation_change(request):
	context = {}
	return render_to_response('admin/uploads/submission/change_list.html', context, context_instance=RequestContext(request))


@user_passes_test(user_is_staff_admin, login_url="/login/")
@csrf_protect
def feature_tutorial_change(request):	
	'''create and post new Tutorial object to Tutorial Library '''
	if request.method == 'POST': #and request.is_ajax():
		tutform = TutorialForm(request.POST)
		logger.info("POST: %s" %(request.POST))
		logged_in_user = UserProfile.objects.get(user=request.user)
		logger.info("logged_in_super_user = %s"%logged_in_user)
		
		if tutform.is_valid(): # 
			try:
				feature_tutorial = tutform.save(commit=False)
				feature_tutorial.save()

				logger.info("inside view: revform.cleaned_data['vidanimation'] = %s"%revform.cleaned_data['vidanimation'])
				# start celery task workers
				if feature_tutorail.vimeoid:
					result = post_tutorial.delay( str(feature_tutorial.vimeoid) )

				if 'generic' not in default_revision.vidpic:
					result = make_thumbnails.apply_async(args=[str(new_submission.id), str(default_revision.id), extension],  countdown=10)

			except Exception as e:
				print e
				traceback.print_exc(file=sys.stdout)
			finally:
				return HttpResponseRedirect(reverse('feature_tutorial_change_done', args=[new_tutorial.vimeoid]))

		else:
			# form not valid, try again
			context = { 'tutform': tutform, 'user': request.user  } #'ontforms': ontforms, 
			#return render_to_response('accounts/feature_tutorial_change_form.html', context, context_instance=RequestContext(request))
			render_to_response('admin/uploads/tutorial/change_list.html',context,context_instance=RequestContext(request))

	else:
		'''user is not submitting the form, show them a blank registration form'''
		# unbound form, it cannot do validation, renders a blank form as HTML
		tuts = Tutorial.objects.all()
		tutform = TutorialForm()
		if tuts:
			#feature_tutorial = Tutorial.objects.filter(tutfeature=True)
			#if feature_tutorial:
				#tutform = TutorialForm(instance=feature_tutorial[0])
				# add form to context
				#context = { 'tuts': tuts, 'tutfeatform':tutfeatureform,'user': request.user, }
			#else:
			context = { 'tuts': tuts,'user': request.user, }
		else:
			context = { 'tuts': None,'user': request.user }

		#return render_to_response('accounts/feature_tutorial_change_form.html', context, context_instance=RequestContext(request))
		render_to_response('admin/uploads/tutorial/change_list.html',context,context_instance=RequestContext(request))
