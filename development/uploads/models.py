#*******************************************************************
# models.py: 
# Database layout - A model class represents a database table, and 
# an instance of that class represents a particular record in the 
# database table.
#*******************************************************************
# stdlib imports
import os
import uuid

# core django imports
from django.db import models
from django.core.files import File

# 3rd party app imports
from model_utils.models import TimeStampedModel

# mfb imports
from core.models import UserProfile


class FileType(TimeStampedModel):
	'''describe the file format of an upload'''
	filetype 			= models.CharField(max_length=50)

	def __unicode__(self):
		return "%s" %(self.filetype)


class Pdb(TimeStampedModel):
	'''M2M pdb can be in many Submissions and a Submission can have many pdbs'''
	pdbname			= models.CharField(max_length=10, blank=True, unique=True) 								# pdb can have many submissions
	count			= models.IntegerField(default=1, null=True)												# number of submissions using this pdb
	submissions		= models.ManyToManyField('Submission', related_name='pdbs', null=True, blank=True) 		# submission may have 0 or 1 or more tags

	class Meta:
		ordering = ["-count","-created"]

	def __unicode__(self):
		return "%s" %(self.pdbname)

	def displaySubmissions(self):
		return ', '.join([submission.title for submission in self.submissions.all()])

	def mostUsedPdb(self):
		return self.submissions.all().count()


class Tag(TimeStampedModel):
	'''M2M tag can be in many Submission and a Submission can have many tags'''
	tagname				= models.CharField(max_length=50, blank=True, unique=True) 								# tag can have many submissions
	count				= models.IntegerField(default=0,null=True)												# number of submissions using this tag
	submissions			= models.ManyToManyField('Submission', related_name='tags', null=True, blank=True) 		# submission may have 0 or 1 or more tags

	class Meta:
		ordering = ["-count","-created"]

	def __unicode__(self):
		return "%s" %(self.tagname)

	def displaySubmissions(self):
		return '; '.join([submission.title for submission in self.submissions.all()])

	def getSubmissions(self):
		return self.submissions.all()

	def mostUsedTag(self):
		if self.submissions.all().count() is None:
			self.count = 0
		else:
			self.count = self.submissions.all().count()
		self.save(update_fields=['count'])
		return self.submissions.all().count()

class SubmissionViewTracking(TimeStampedModel):
	count				= models.PositiveIntegerField(default=0, blank=True)
	submissions 		= models.ManyToManyField('Submission',related_name='viewcounts',null=True, blank=True)		# Submission may have 0 or more views


class Submission(TimeStampedModel):
	'''submit objects of scene files, animations; submission can have multiple revision objects'''
	user 				= models.ForeignKey('core.UserProfile', related_name='submissions') # a submission remembers who submitted it
	authors				= models.CharField(max_length=500, null=True)
	title 				= models.CharField(max_length=250)
	filetype 			= models.ForeignKey(FileType, null=True, blank=True)
	description 		= models.TextField()
	ancestors			= models.CharField(max_length=100, blank=True)
	vidfeature			= models.BooleanField()
	vidstaffpick		= models.BooleanField()
	views				= models.PositiveIntegerField(default=0)

	# used in admin interface, shell, etc.; self.id value autocreated by django
	def __unicode__(self):
		return "%s" %(self.title)

	def displayUser(self):
		return "%s" %(self.user)

	def displayPdbs(self):
		return ', '.join([pdb.pdbname for pdb in self.pdbs.all()])

	def displayTags(self):
		return ', '.join([tag.tagname for tag in self.tags.all()])

	def displayViews(self):
		return str(views)

	def getTags(self):
		return self.tags.all()

	def getPdbs(self):
		return self.pdbs.all()

	def trackViews(self):
		self.views = self.views + 1
		self.save(update_fields=['views'])

	def getViewCounts(self):
		return self.viewcounts.all()

	def displayRevisions(self):
		return '; '.join([revision.sourcefile.url for revision in self.revisions.all()])
	
	def delete(self, *args, **kwargs):
		self.file.delete(False)
		super(Submission, self).delete(*args, **kwargs)

def upload_source_to(instance, filename):
	sourceExt = filename.split('.')[-1]
	sanitized_filename = "%s.%s" %(instance.randnum, sourceExt)
	return os.path.join("scenes","uid_" + str(instance.user.user_id), sanitized_filename)

def upload_movie_to(instance, filename):
	ext = filename.split('.')[-1]
	sanitized_filename = "%s.%s" %(instance.randnum, ext)
	return os.path.join("videos","uid_" + str(instance.user.user_id), sanitized_filename)

def upload_thumbnail_to(instance, filename):
	ext = filename.split('.')[-1]
	sanitized_filename = "%s.%s" %(instance.randnum, ext)
	return os.path.join("images","uid_" + str(instance.user.user_id), sanitized_filename)
	

class Revision(TimeStampedModel):
	'''upload newly modified animation scenes, thumbnails, changing of text input'''
	randnum = str(uuid.uuid4())
	submission 			= models.ForeignKey('Submission', related_name='revisions', null=True, blank=True) # a revision remembers which submission it belongs to
	user 				= models.ForeignKey('core.UserProfile', related_name='member', null=True, blank=True)
	# original uploaded file (latest revision) for users to download; placed in MEDIA_ROOT
	sourcefile			= models.URLField(max_length=2000)
	#sourcefile			= models.FileField('Revision',max_length=250, upload_to=upload_source_to)
	# video of source file; placed in MEDIA_ROOT and MEDIA_URL
	vidanimation		= models.URLField(max_length=2000, blank=True)
	#vidanimation		= models.FileField('Revision', max_length=250, null=True, blank=True, upload_to=upload_movie_to)
	vimeoid 			= models.CharField(max_length=50, blank=True)
	# thumbnail preview/image; parsed out of filetype or submitted by user; placed in MEDIA_ROOT

	vidpic				= models.URLField(max_length=2000, blank=True)
	thumb_small			= models.URLField(max_length=2000, blank=True)
	thumb_medium		= models.URLField(max_length=2000, blank=True)
	thumb_large			= models.URLField(max_length=2000, blank=True)
	#vidpic				= models.ImageField('Revision', max_length=250, null=True, blank=True, upload_to=upload_thumbnail_to)
	comments			= models.CharField(max_length=200, blank=True)

	class Meta:
		order_with_respect_to = 'submission'
		ordering = "-created"

	def displayMember(self):
		return "%s" %(self.user)


class Tutorial(TimeStampedModel):
	'''get tutorials from Vimeo'''
	vimeoid 			= models.CharField(max_length=50, verbose_name="VimeoID")
	title 				= models.CharField(max_length=500, blank=True)
	iframe				= models.CharField(max_length=1000,blank=True)
	tutfeature			= models.BooleanField()
	tutpic				= models.URLField(max_length=2000, blank=True)
	thumb_small			= models.URLField(max_length=2000, blank=True)
	thumb_medium		= models.URLField(max_length=2000, blank=True)
	thumb_large			= models.URLField(max_length=2000, blank=True)
	tut_tags			= models.CharField(max_length=200, blank=True)
	comments			= models.CharField(max_length=200, blank=True)

#	class Meta:
#		ordering = "-created"
