'''
Created on Jan 3, 2013

@author: rise
'''
# stdlib imports
import pdb
import os
import traceback
import sys
import time
import urllib2
import json
import requests
import base64
import hmac
import urllib
from hashlib import sha1
import uuid
import re
import logging

# core django imports
from django.contrib import messages
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import user_passes_test
from django.core.files.uploadedfile import UploadedFile
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.template import loader
from django.core.urlresolvers import reverse
from django.db import transaction
from django.views.decorators.csrf import csrf_protect

# 3rd party imports

# project imports
from core.models import UserProfile
from .formsrevision import PdbForm
from .formsrevision import TagForm
from .formsrevision import SubmissionForm
from .formsrevision import RevisionForm
from .formsrevision import FileTypeForm
from .models import Pdb
from .models import Tag
from .models import Submission
from .models import Revision
from .models import FileType
from .tasks import *
from .viewssubmission import getOembed
from .viewssubmission import getNumberOfViews
from .tasks import *
from settings.base import get_env_variable

# log all errors for debugging
logger = logging.getLogger('infolog')

FILETYPES = [
				('10', '.flipbook (Molecular FlipBook)'),
				('20', '.blend (Blender)'),
				('30', '.ma/.mb (Maya)'),
				('40', '.c4d (Cinema 4D)'),
				('50', '.max/.3ds (3DS Max)'),
				('60', '.dae (Collada)'),
				('70', '.x3d (Web 3D)'),
				('80', '.pdf (Acrobat)'), 
				('90', '.key (Keynote)'),
				('100','.ppt/.pptx (Powerpoint)'),
				('110', '.ai (Illustrator)'), 
				('120', '.psd/.eps (Photoshop)'),
				('130','.xcf (GIMP)'),
				('140', 'Other file format')
				]

AWS_ACCESS_KEY_ID = get_env_variable('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = get_env_variable('AWS_SECRET_ACCESS_KEY')
AWS_BUCKET_URL = get_env_variable('AWS_BUCKET_URL')
AWS_STORAGE_BUCKET_NAME = get_env_variable('AWS_STORAGE_BUCKET_NAME')
VIMEO_API_KEY = get_env_variable('VIMEO_API_KEY')
VIMEO_API_SECRET = get_env_variable('VIMEO_API_SECRET')
VIMEO_ACCESS_TOKEN = get_env_variable('VIMEO_ACCESS_TOKEN')
VIMEO_ACCESS_SECRET = get_env_variable('VIMEO_ACCESS_SECRET')
VIMEO_OEMBED_URL = get_env_variable('VIMEO_OEMBED_URL')

def sub_tag_info_success(request, subPk, revPk):
	#recent_revision = Submission.objects.get(id=id).revisions.order_by(-id)[0]
	template = loader.get_template("accounts/successful_subtagging.html")
	submission = Submission.objects.get(pk=subPk)
	context = RequestContext(request,{'title':submission.title,'subid':subPk, 'revid':revPk})
	return HttpResponse(template.render(context))

def rev_tag_info_success(request, revPk):
	#recent_revision = Submission.objects.get(id=id).revisions.order_by(-id)[0]
	template = loader.get_template("accounts/successful_revtagging.html")
	selected_revision = Revision.objects.get(pk=revPk)
	logger.info("selected_revision.pk = %s"%selected_revision.pk)
	submissionPk = selected_revision.submission.pk

	submission = Submission.objects.get(pk=submissionPk)
	context = RequestContext(request,{'title':submission.title, 'revPk':revPk})
	return HttpResponse(template.render(context))

def getSubInfo(request, subPk, revPk):
	'''display detailed editable information for given revision object'''
	selected_revision = Revision.objects.get(pk=revPk)
	selected_submission = Submission.objects.get(pk=subPk)
	logger.info("=============== CALLING log_submission_view IN GETSUBINFO ============== ")
	result = log_submission_view.delay( str(selected_submission.id) ) # track view count
	tagform = TagForm()
	submissions = Submission.objects.filter(user=selected_submission.user)
	
	revExt = str(selected_submission.filetype)
	vidCount = Submission.objects.filter(user=selected_submission.user).count()

	if request.method == "POST" and request.is_ajax():
		logger.info("inside VIEWSREVISION POST: getSubInfo")
		tagform = TagForm(request.POST)
		if tagform.is_valid():
			if tagform.cleaned_data['tagname']:
				for tagpk in tagform.cleaned_data['tagname']:
					tag = Tag.objects.get(pk=tagpk)
					tag.submissions.add(selected_submission)
					tag.count = tag.mostUsedTag()
					tag.save()

			message = 'true'

			return HttpResponse(json.dumps({'message': message}))

	if selected_revision.vimeoid:
		logger.info("getSubInfo: vimeoid = %s"%selected_revision.vimeoid)
		iframe = getOembed(selected_revision.vimeoid)
		# if original size is not 640x360, change dimensions in iframe
		logger.info("========== viewsrevision: getSubInfo: iframe = %s, type(iframe) = %s"%(iframe,type(iframe)))
		numofviews = getNumberOfViews(selected_revision.vimeoid)

		if iframe:
			pattern = re.compile('width="[0-9]+" height="[0-9]+"')
			#iframe = p.sub('width="640" height="360"', iframe)
			iframe = pattern.sub('width="620" height="360"', iframe)
			logger.info(iframe)
			#pattern = re.compile('src="//player.vimeo.com/video/[0-9]+"')
			#m = re.search(r'src="//player.vimeo.com/video/[0-9]+',iframe)
			#if m:
			#	logger.info('getSubInfo: p = %s'%m.group(0))
			#	newsrc = str(m.group(0))
			#	logger.info(newsrc)
			#	iframe = pattern.sub(newsrc, iframe)

			if request.user.is_authenticated():
				context = { 'vidCount': vidCount, 'submissions': submissions, 'selected_submission': selected_submission, 'username': selected_submission.user, 'userpk':selected_submission.user.pk,
						'title': selected_submission.title, 'authors':selected_submission.authors,'pdbs': selected_submission.displayPdbs, 'tagform':tagform,'tags': selected_submission.getTags,
						'tagsinput':selected_submission.displayTags,'filetype': selected_submission.filetype,'description': selected_submission.description, 'ancestors': selected_submission.ancestors,
						'revmodified': selected_revision.modified, 'sourcefile': selected_revision.sourcefile, 'subpk':subPk,'revpk':revPk,
						'selected_revision': selected_revision, 'oembed': iframe, 'numofviews': selected_submission.views,
						'comments': selected_revision.comments, 'timestamp': selected_revision.modified 
						}
			else:
				context = { 'vidCount': vidCount, 'submissions': submissions, 'selected_submission': selected_submission, 'username': selected_submission.user, 'userpk':selected_submission.user.pk,
						'title': selected_submission.title, 'authors':selected_submission.authors,'pdbs': selected_submission.displayPdbs, 'tags': selected_submission.getTags, 
						'tagsinput':selected_submission.displayTags,'filetype': selected_submission.filetype,'description': selected_submission.description, 'ancestors': selected_submission.ancestors,
						'revmodified': selected_revision.modified, 'sourcefile': selected_revision.sourcefile, 'subpk':subPk,'revpk':revPk,
						'selected_revision': selected_revision, 'oembed': iframe, 'numofviews': selected_submission.views,
						'comments': selected_revision.comments, 'timestamp': selected_revision.modified 
						}

			return render_to_response('accounts/submission_info.html', context, context_instance=RequestContext(request))

	if request.user.is_authenticated():
		#else: 
		context = { 'vidCount': vidCount, 'submissions': submissions, 'selected_submission': selected_submission, 'username': selected_submission.user, 'userpk':selected_submission.user.pk,
				'title': selected_submission.title, 'authors':selected_submission.authors, 'pdbs': selected_submission.displayPdbs, 'tagform':tagform,'tags': selected_submission.getTags, 
				'tagsinput':selected_submission.displayTags,'filetype': selected_submission.filetype,'description': selected_submission.description, 'ancestors': selected_submission.ancestors,
				'revmodified': selected_revision.modified, 'sourcefile': selected_revision.sourcefile, 'subpk':subPk,'revpk':revPk,
				'selected_revision': selected_revision, 'oembed': None, 'numofviews':selected_submission.views,
				'comments': selected_revision.comments, 'timestamp': selected_revision.modified 
				}
	else:
		logger.info("========== viewsrevision: getSubInfo: no vimeoid")
		context = { 'vidCount': vidCount, 'submissions': submissions, 'selected_submission': selected_submission, 'username': selected_submission.user, 'userpk':selected_submission.user.pk,
				'title': selected_submission.title, 'authors':selected_submission.authors, 'pdbs': selected_submission.displayPdbs, 'tags': selected_submission.getTags, 
				'tagsinput':selected_submission.displayTags,'filetype': selected_submission.filetype,'description': selected_submission.description, 'ancestors': selected_submission.ancestors,
				'revmodified': selected_revision.modified, 'sourcefile': selected_revision.sourcefile, 'subpk':subPk,'revpk':revPk,
				'selected_revision': selected_revision, 'oembed': None, 'numofviews': selected_submission.views,
				'comments': selected_revision.comments, 'timestamp': selected_revision.modified
				}

	return render_to_response('accounts/submission_info.html', context, context_instance=RequestContext(request))

def getRevInfo(request, revPk):
	'''display detailed editable information for given revision object'''
	#pdb.set_trace()
	selected_revision = Revision.objects.get(pk=revPk)
	logger.info("selected_revision.pk = %s"%selected_revision.pk)
	logger.info("selected_revision.vimeoid = %s"%selected_revision.vimeoid)
	submissionPk = selected_revision.submission.pk
	selected_submission = Submission.objects.get(pk=submissionPk)
	logger.info("selected_submission = %s"%selected_submission)
	logger.info("selected_submission.displayTags = %s"%selected_submission.displayTags())
	tagform = TagForm()

	revExt = str(selected_submission.filetype)
	vidCount = Submission.objects.filter(user=selected_submission.user).count()

	if request.method == "POST" and request.is_ajax():
		logger.info("inside VIEWSREVISION POST: getRevInfo")
		tagform = TagForm(request.POST)
		if tagform.is_valid():
			if tagform.cleaned_data['tagname']:
				try:
					# remove deleted tags
					for oldtag in selected_submission.getTags():
						oldpk = Tag.objects.get(tagname=oldtag).pk
						if ( oldpk not in tagform.cleaned_data['tagname'] ):
							oldtag.submissions.remove(selected_submission)
							oldtag.count = oldtag.mostUsedTag()
							oldtag.save()

					for tagpk in tagform.cleaned_data['tagname']:
						tag = Tag.objects.get(pk=tagpk)
						tag.submissions.add(selected_submission)
						tag.count = tag.mostUsedTag()
						tag.save()

					message = 'true'
					return HttpResponse(json.dumps({'message': message}))

				except Exception as e:
					logger.info(e)
					data = json.dumps(e)
					return HttpResponse(data, mimetype='application/json')

	if selected_revision.vimeoid:
		iframe = getOembed(selected_revision.vimeoid)
		numofviews = getNumberOfViews(selected_revision.vimeoid)
		logger.info("========== viewsrevision: getRevInfo: iframe = %s, type(iframe) = %s"%(iframe,type(iframe)))

		if iframe:
			pattern = re.compile('width="[0-9]+" height="[0-9]+"')
			iframe = pattern.sub('width="700" height="394"', iframe)
			logger.info(iframe)
			#pattern = re.compile('src="//player.vimeo.com/video/[0-9]+"')
			#m = re.search(r'src="//player.vimeo.com/video/[0-9]+',iframe)
			#if m:
			#	logger.info('getRevInfo: p = %s'%m.group(0))
			#	newsrc = str(m.group(0))
			#	logger.info(newsrc)
			#	iframe = pattern.sub(newsrc, iframe)

			if request.user.is_authenticated():
				context = { 'vidCount': vidCount, 'selected_submission': selected_submission, 'username': selected_submission.user, 'userpk':selected_submission.user.pk,
						'title': selected_submission.title, 'authors':selected_submission.authors, 'pdbs': selected_submission.displayPdbs, 'tagform':tagform,'tags': selected_submission.getTags, 
						'tagsinput':selected_submission.displayTags,'filetype': selected_submission.filetype,'description': selected_submission.description, 'ancestors': selected_submission.ancestors,
						'revmodified': selected_revision.modified, 'sourcefile': selected_revision.sourcefile, 'subpk':selected_submission.pk,'revpk':revPk,
						'selected_revision': selected_revision, 'oembed': iframe, 'numofviews': selected_submission.views,
						'comments': selected_revision.comments, 'timestamp': selected_revision.modified
						}
			else:
				context = { 'vidCount': vidCount, 'selected_submission': selected_submission, 'username': selected_submission.user, 'userpk':selected_submission.user.pk,
						'title': selected_submission.title, 'authors':selected_submission.authors, 'pdbs': selected_submission.displayPdbs, 'tags': selected_submission.getTags, 
						'tagsinput':selected_submission.displayTags,'filetype': selected_submission.filetype,'description': selected_submission.description, 'ancestors': selected_submission.ancestors,
						'revmodified': selected_revision.modified, 'sourcefile': selected_revision.sourcefile, 'subpk':selected_submission.pk,'revpk':revPk,
						'selected_revision': selected_revision, 'oembed': iframe, 'numofviews':selected_submission.views,
						'comments': selected_revision.comments, 'timestamp': selected_revision.modified 
						}

			return render_to_response('accounts/revision_info.html', context, context_instance=RequestContext(request))

	if request.user.is_authenticated():
		context = { 'vidCount': vidCount, 'selected_submission': selected_submission, 'username': selected_submission.user, 'userpk':selected_submission.user.pk,
				'title': selected_submission.title, 'authors':selected_submission.authors, 'pdbs': selected_submission.displayPdbs, 'tagform':tagform,'tags': selected_submission.getTags, 
				'tagsinput':selected_submission.displayTags,'filetype': selected_submission.filetype,'description': selected_submission.description, 'ancestors': selected_submission.ancestors,
				'revmodified': selected_revision.modified, 'sourcefile': selected_revision.sourcefile,'subpk':selected_submission.pk,'revpk':revPk,
				'selected_revision': selected_revision, 'oembed': None, 'numofviews':selected_submission.views,
				'comments': selected_revision.comments, 'timestamp': selected_revision.modified
				}
	else:
		logger.info("========== viewsrevision: getSubInfo: no vimeoid")
		context = { 'vidCount': vidCount, 'selected_submission': selected_submission, 'username': selected_submission.user, 'userpk':selected_submission.user.pk,
				'title': selected_submission.title, 'authors':selected_submission.authors, 'pdbs': selected_submission.displayPdbs, 'tags': selected_submission.getTags, 
				'tagsinput':selected_submission.displayTags,'filetype': selected_submission.filetype,'description': selected_submission.description, 'ancestors': selected_submission.ancestors,
				'revmodified': selected_revision.modified, 'sourcefile': selected_revision.sourcefile,'subpk':selected_submission.pk,'revpk':revPk,
				'selected_revision': selected_revision, 'oembed': None, 'numofviews': selected_submission.views,
				'comments': selected_revision.comments, 'timestamp': selected_revision.modified
				}

	return render_to_response('accounts/revision_info.html', context, context_instance=RequestContext(request))

def getSearchRevInfo(request, revPk):
	'''display detailed editable information for given revision object'''
	selected_revision = Revision.objects.get(pk=revPk)
	submissionPk = selected_revision.submission.pk
	selected_submission = Submission.objects.get(pk=submissionPk)
	tagform = TagForm()
	vidCount = 0

	if request.method == "POST" and request.is_ajax():
		logger.info("inside VIEWSREVISION POST: getSubInfo")
		tagform = TagForm(request.POST)
		if tagform.is_valid():
			if tagform.cleaned_data['tagname']:
				for tagpk in tagform.cleaned_data['tagname']:
					tag = Tag.objects.get(pk=tagpk)
					tag.submissions.add(selected_submission)
					tag.count = tag.mostUsedTag()
					tag.save()

			message = 'true'
			return HttpResponse(json.dumps({'message': message}))

	if selected_revision.vimeoid:
		iframe = getOembed(selected_revision.vimeoid)
		numofviews = getNumberOfViews(selected_revision.vimeoid)
		logger.info("========== viewsrevision: getRevInfo: iframe = %s, type(iframe) = %s"%(iframe,type(iframe)))

		if iframe:
			pattern = re.compile('width="[0-9]+" height="[0-9]+"')
			#iframe = p.sub('width="640" height="360"', iframe)
			iframe = pattern.sub('width="841" height="473"', iframe)
			pattern = re.compile('src="//player.vimeo.com/video/[0-9]+"')
			logger.info(iframe)
			#m = re.search(r'src="//player.vimeo.com/video/[0-9]+',iframe)
			#if m:
			#	logger.info('getSearchRevInfo: p = %s'%m.group(0))
			#	newsrc = str(m.group(0))
			#	logger.info(newsrc)
			#	iframe = pattern.sub(newsrc, iframe)

			if request.user.is_authenticated():
				context = { 'vidCount': vidCount, 'selected_submission': selected_submission, 'username': selected_submission.user, 'userpk':selected_submission.user.pk,
						'title': selected_submission.title, 'authors':selected_submission.authors, 'pdbs': selected_submission.displayPdbs, 'tagform':tagform,'tags': selected_submission.getTags, 
						'tagsinput':selected_submission.displayTags,'filetype': selected_submission.filetype,'description': selected_submission.description, 'ancestors': selected_submission.ancestors,
						'revmodified': selected_revision.modified, 'sourcefile': selected_revision.sourcefile, 'subpk':submissionPk,'revpk':revPk,
						'selected_revision': selected_revision, 'oembed': iframe, 'numofviews': numofviews,
						'comments': selected_revision.comments, 'timestamp': selected_revision.modified
						}
			else:
				context = { 'vidCount': vidCount, 'selected_submission': selected_submission, 'username': selected_submission.user, 'userpk':selected_submission.user.pk,
						'title': selected_submission.title, 'authors':selected_submission.authors, 'pdbs': selected_submission.displayPdbs, 'tags': selected_submission.getTags, 
						'tagsinput':selected_submission.displayTags,'filetype': selected_submission.filetype,'description': selected_submission.description, 'ancestors': selected_submission.ancestors,
						'revmodified': selected_revision.modified, 'sourcefile': selected_revision.sourcefile, 'subpk':submissionPk,'revpk':revPk,
						'selected_revision': selected_revision, 'oembed': iframe, 'numofviews':"N/A",
						'comments': selected_revision.comments, 'timestamp': selected_revision.modified 
						}

			return render_to_response('search/search_revinfo.html', context, context_instance=RequestContext(request))

	if request.user.is_authenticated():
		context = { 'vidCount': vidCount, 'selected_submission': selected_submission, 'username': selected_submission.user, 'userpk':selected_submission.user.pk,
				'title': selected_submission.title, 'authors':selected_submission.authors, 'pdbs': selected_submission.displayPdbs, 'tagform':tagform,'tags': selected_submission.getTags, 
				'tagsinput':selected_submission.displayTags,'filetype': selected_submission.filetype,'description': selected_submission.description, 'ancestors': selected_submission.ancestors,
				'revmodified': selected_revision.modified, 'sourcefile': selected_revision.sourcefile,'subpk':submissionPk,'revpk':revPk,
				'selected_revision': selected_revision, 'oembed': None, 'numofviews':"N/A",
				'comments': selected_revision.comments, 'timestamp': selected_revision.modified
				}
	else:
		logger.info("========== viewsrevision: getSubInfo: no vimeoid")
		context = { 'vidCount': vidCount, 'selected_submission': selected_submission, 'username': selected_submission.user, 'userpk':selected_submission.user.pk,
				'title': selected_submission.title, 'authors':selected_submission.authors, 'pdbs': selected_submission.displayPdbs, 'tags': selected_submission.getTags, 
				'tagsinput':selected_submission.displayTags,'filetype': selected_submission.filetype,'description': selected_submission.description, 'ancestors': selected_submission.ancestors,
				'revmodified': selected_revision.modified, 'sourcefile': selected_revision.sourcefile,'subpk':submissionPk,'revpk':revPk,
				'selected_revision': selected_revision, 'oembed': None, 'numofviews': "N/A",
				'comments': selected_revision.comments, 'timestamp': selected_revision.modified
				}

	return render_to_response('search/search_revinfo.html', context, context_instance=RequestContext(request))

def rev_success(request, pk):
	#recent_revision = Submission.objects.get(id=id).revisions.order_by(-id)[0]
	template = loader.get_template("accounts/successful_revision.html")
	context = RequestContext(request,{'revid':pk})
	return HttpResponse(template.render(context))

def user_can_upload(user):
	return user.is_authenticated() and user.is_active

def getLatestRev(pkVid):
	latestRev = Submission.objects.get(pk=pkVid).revisions.all()[Submission.objects.get(pk=pkVid).revisions.count()-1]
	return latestRev

@user_passes_test(user_can_upload, login_url="/login/")
@csrf_protect
def MFBRevision(request, pkVid):
	'''submit a revision for a submission object'''
	selected_submission = Submission.objects.get(pk=pkVid)
	new_revision = Revision() #create; doesn't save to db
	latestRev = getLatestRev(pkVid)

	if request.method == 'POST':
		#pdb.set_trace()
		# create form instances for each model object
		pdbform = PdbForm(request.POST)
		tagform = TagForm(request.POST)
		subform = SubmissionForm(request.POST, instance=selected_submission)
		ftform = FileTypeForm(request.POST)
		revform = RevisionForm(request.POST)
		logged_in_user = UserProfile.objects.get(user=request.user)
		logger.info('===== VIEWSREVISION: POST = %s'%request.POST)

		# check if bound data is clean for form instances and model instances
		if pdbform.is_valid() and tagform.is_valid() and subform.is_valid() and ftform.is_valid() and revform.is_valid():
			try:
				logger.info("============ VIEWSREVISION ==================")
				subform.save() # save new data to existing Submission object 

				logger.info('========= VIEWSREVISION: PDB = %s'%pdbform.cleaned_data['pdbname'])
				if pdbform.cleaned_data['pdbname']:
					for oldpdb in selected_submission.getPdbs():
						oldpk = Pdb.objects.get(pdbname=oldpdb).pk
						logger.info("======= VIEWSREVISION: oldpk = %s"%oldpk)
						if ( oldpk not in pdbform.cleaned_data['pdbname'] ):
							oldpdb.submissions.remove(selected_submission)
							oldpdb.count = oldpdb.mostUsedPdb()
							oldpdb.save()

					for pdbpk in pdbform.cleaned_data['pdbname']:
						logger.info("======= VIEWSREVISION: pdbpk = %s"%pdbpk)
						pdb = Pdb.objects.get(pk=pdbpk)
						logger.info("======= VIEWSREVISION: pdb = %s"%pdb)
						pdb.submissions.add(selected_submission)
						pdb.count = pdb.mostUsedPdb()
						pdb.save()

				logger.info('========= VIEWSREVISION: TAG = %s'%tagform.cleaned_data['tagname'])
				if tagform.cleaned_data['tagname']:
					for oldtag in selected_submission.getTags():
						oldpk = Tag.objects.get(tagname=oldtag).pk
						logger.info("======= VIEWSREVISION: oldpk = %s"%oldpk)
						if ( oldpk not in tagform.cleaned_data['tagname'] ):
							oldtag.submissions.remove(selected_submission)
							oldtag.count = oldtag.mostUsedTag()
							oldtag.save()

					for tagpk in tagform.cleaned_data['tagname']:
						logger.info("======= VIEWSREVISION: tagpk = %s"%tagpk)
						tag = Tag.objects.get(pk=tagpk)
						logger.info("======= VIEWSREVISION: tag = %s"%tag)
						tag.submissions.add(selected_submission)
						tag.count = tag.mostUsedTag()
						tag.save()

				#pdb.set_trace()
				logger.info('========= VIEWSREVISION: new_revision.save()')
				new_revision.submission = selected_submission
				new_revision.user = logged_in_user
				new_revision.sourcefile = revform.cleaned_data['sourcefile']
				new_revision.comments = revform.cleaned_data['comments']
				new_revision.save()

				rev_set = selected_submission.revisions.all()
				revCt = rev_set.count() - 1
				logger.info("======= VIEWSREVISION: revCt = %s"%revCt)
				prev_vidpic = rev_set[revCt-1].vidpic
				prev_vid = rev_set[revCt-1].vidanimation
				prev_vimeoid = rev_set[revCt-1].vimeoid
				prev_thumbsmall = rev_set[revCt-1].thumb_small
				prev_thumbmed = rev_set[revCt-1].thumb_medium
				prev_thumblarge = rev_set[revCt-1].thumb_large

				if revform.cleaned_data['vidanimation']:
					logger.info('========= VIEWSREVISION: user video uploaded')
					new_revision.vidanimation = revform.cleaned_data['vidanimation']

				else:
					logger.info("========= VIEWSREVISION: no vidanimation uploaded")
					if prev_vid:
						logger.info('========= VIEWSREVISION: use prev_vid')
						new_revision.vidanimation = prev_vid
						new_revision.vimeoid = prev_vimeoid

					else:
						logger.info('========= VIEWSREVISION: assign empty string')
						new_revision.vidanimation = ""
						new_revision.vimeoid = ""

				extension = ''
				try:
					logger.info("Filetype = %s"%FileType.objects.get(pk=request.POST.get('filetype')).filetype.split('.')[2])
					extension = FileType.objects.get(pk=request.POST.get('filetype')).filetype.split('.')[2]
				except:
					logger.info("Filetype = %s"%FileType.objects.get(pk=request.POST.get('filetype')).filetype.split('.')[1])
					extension = FileType.objects.get(pk=request.POST.get('filetype')).filetype.split('.')[1]

				logger.info("NEW FILETYPE extension = %s"%extension)
				if revform.cleaned_data['vidpic']:
					logger.info('========= VIEWSREVISION: user vidpic uploaded = %s'%revform.cleaned_data['vidpic'])
					new_revision.vidpic = revform.cleaned_data['vidpic']
					new_revision.thumb_small = os.path.join(AWS_BUCKET_URL, 'media','icons', extension + '_small.jpg')
					new_revision.thumb_medium = os.path.join(AWS_BUCKET_URL, 'media','icons', extension + '_medium.jpg')
					new_revision.thumb_large = os.path.join(AWS_BUCKET_URL, 'media','icons', extension + '_large.jpg')
				else:
					logger.info('========= VIEWSREVISION: no vidpic uploaded ')
					if 'generic' in prev_vidpic:
						logger.info("============ VIEWSREVISION: thumbnail - rev_set[revCt-1].vidpic: %s"%prev_vidpic)
						new_revision.vidpic = os.path.join(AWS_BUCKET_URL, 'media','icons','generic-icon-1024x576-' + extension + '.png')
						new_revision.thumb_small = os.path.join(AWS_BUCKET_URL, 'media','icons', extension + '_small.jpg')
						new_revision.thumb_medium = os.path.join(AWS_BUCKET_URL, 'media','icons', extension + '_medium.jpg')
						new_revision.thumb_large = os.path.join(AWS_BUCKET_URL, 'media','icons', extension + '_large.jpg')

					else:
						new_revision.vidpic = prev_vidpic # forward populate user-selected image
						new_revision.thumb_small = prev_thumbsmall
						new_revision.thumb_medium = prev_thumbmed
						new_revision.thumb_large = prev_thumblarge

				logger.info('========= VIEWSREVISION: new_revision')
				new_revision.save() # save instance Revision object in db
				logger.info('========= VIEWSREVISION: new_revision.pk = %s'%new_revision.pk)

				#selected_submission.user = logged_in_user
				#selected_submission.authors = subform.cleaned_data['authors']
				#selected_submission.filetype= ftform.cleaned_data['filetype']
				selected_submission.revisions.add(new_revision)
				selected_submission.save()

				logger.info("inside view: revform.cleaned_data['vidanimation'] = %s"%revform.cleaned_data['vidanimation'])

				# start celery task workers
				if new_revision.vidanimation:
					result = send_to_vimeo.delay( str(selected_submission.id), str(new_revision.id) )

				if 'generic' not in new_revision.vidpic:
					result = make_thumbnails.apply_async(args=[str(selected_submission.id), str(new_revision.id), extension],  countdown=10)
				
				logger.info('========= DONE VIEWSREVISION =============== ')
				#messages.add_message(request, messages.SUCCESS, (u"Upload file revised and saved."))
			except Exception as e: 
				new_revision.delete()
				logger.info('exception e = %'%e)
				traceback.print_exc(file=sys.stdout)

			finally: 
				logger.info('=========== IN FINALLY: redirect success ==============')
				return HttpResponseRedirect(reverse('rev_success', args=[new_revision.pk]))

		else:
			# form submitted and not valid, try again
			logger.info("User form is bound:%s errors:%s"%(subform.is_bound, subform.errors))
			logger.info("User form is bound:%s errors:%s"%(revform.is_bound, revform.errors))
			context = { 'subform': subform, 'ftform': ftform, 'revform': revform, 'latestRev': latestRev, 'pkVid': pkVid }
			return render_to_response('accounts/revision_form.html', context, context_instance=RequestContext(request))
		
	else:
		'''user is not submitting the form, show them a blank revision form with selected populated fields'''
		# unbound form, not used as fallback values, create form to edit existing Submission obj and create a new revision obj for this Submission object
		pdbform = PdbForm()
		tagform = TagForm()
		subform = SubmissionForm(instance=selected_submission)
		ftform  = FileTypeForm()
		revform = RevisionForm(instance=new_revision)
		revExt = str(selected_submission.filetype)
		context = { 'pdbform': pdbform, 'tagform': tagform,'subform': subform, 'pdbs': selected_submission.displayPdbs,'tags': selected_submission.displayTags,'ftform': ftform, 'revform': revform, 'title': selected_submission.title, 'latestRev': latestRev, 'latestRevExt': revExt, 'pkVid': pkVid }

		return render_to_response('accounts/revision_form.html', context, context_instance=RequestContext(request))
	